<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('getcity', 'Api\CommonController@getcity');
Route::get('getcountry', 'Api\CommonController@getcountry');
Route::get('getorganization', 'Api\CommonController@getorganization');
Route::get('getstate', 'Api\CommonController@getstate');
Route::get('getarea', 'Api\CommonController@getarea');
Route::get('getcategory', 'Api\CommonController@getcategory');
Route::get('getsubcategory', 'Api\CommonController@getsubcategory');
Route::get('gettags', 'Api\CommonController@gettags');
Route::get('getsubscriiption', 'Api\CommonController@getsubscriiption');

Route::any('statecitylist/{state_id?}', 'Api\CommonController@CountrySateCityArealist');
Route::get('categorysubcategorylist/{category_id?}', 'Api\CommonController@categorysubcategorylist');
Route::get('getreference', 'Api\CommonController@getreference');
Route::post('getOTP', 'Api\CommonController@getOTP');
Route::post('vendor_by_subcategory', 'Api\CommonController@vendorlistbysubcategory');
Route::get('vendor_details/{vendor_id?}', 'Api\CommonController@vendordetails');
Route::get('booked_list/{vendor_id?}', 'Api\CommonController@vendorBookedAvl');
Route::post('whatsappCallClick', 'Api\CommonController@whatsappCallClick');


Route::group(['prefix' => 'customer'], function()
{
	Route::post('login', 'Api\PassportController@login');
	Route::post('register', 'Api\PassportController@register');
	Route::any('eventlist/{id?}', 'Api\CommonController@EventList');
	Route::any('categorysubdetails', 'Api\CommonController@getCategoryDetails');
	Route::any('partnerlist', 'Api\CommonController@PartnerList');
	Route::any('partnercourselist', 'Api\CommonController@PartnerCourseList');
	Route::any('partnerproductlist', 'Api\CommonController@PartnerProductList');
	Route::any('partnerofferlist', 'Api\CommonController@PartnerOfferList');
	Route::any('partnermenufoods', 'Api\CommonController@PartnerMenuFoodList');
	Route::any('partnerjobs', 'Api\CommonController@PartnerJobsList');
	Route::any('partnerphotos/{vendor_id?}', 'Api\CommonController@vendorGallery');
	Route::any('pages/{page_id?}', 'Api\CommonController@CommonPages');
	Route::any('contact', 'Api\CommonController@Contactus');
	Route::post('contactus', 'Api\CommonController@VendorLeadgeneration');

	Route::any('newslist', 'Api\CommonController@NewsList');
	Route::any('bloglist', 'Api\CommonController@BlogList');
	Route::any('advertisementlist', 'Api\CommonController@Advertisement');
	Route::any('positionlist', 'Api\CommonController@PositionList');
	Route::any('testimonial', 'Api\CommonController@TestimonialList');
    Route::any('locationcity', 'Api\CommonController@Locationcity');
    Route::any('search', 'Api\CommonController@searchVendor');


	Route::any('partnermenufoodsbooking', 'Api\CustomerController@FoodBooking')->middleware('auth:api');
	Route::any('partnerreviewrating', 'Api\CustomerController@ReviewRating')->middleware('auth:api');
	Route::any('eventbooking', 'Api\CustomerController@EventBooking')->middleware('auth:api');
	Route::any('reviewlist', 'Api\CustomerController@ReviewList')->middleware('auth:api');
	Route::get('customerdetails', 'Api\CustomerController@customerdetails')->middleware('auth:api');
    Route::any('update_profile_image', 'Api\CustomerController@update_profile_image')->middleware('auth:api');
    Route::any('update_profile', 'Api\CustomerController@update_profile')->middleware('auth:api');
    Route::any('customerlogout', 'Api\CustomerController@customerlogout')->middleware('auth:api');
    Route::any('managechat', 'Api\CustomerController@manageChat')->middleware('auth:api');
    Route::any('updateChatStatus', 'Api\CustomerController@updateChatStatus')->middleware('auth:api');

});


Route::group(['prefix' => 'partner'], function()
{
	Route::post('login', 'Api\Partner\PassportController@login');
	Route::post('register', 'Api\Partner\PassportController@register');
	Route::post('forgotpassword', 'Api\Partner\PassportController@forgotpassword');

    Route::any('uploadMoreImage', 'Api\Partner\PartnerController@uploadMoreImage')->middleware('auth:api');
    Route::any('managegallery', 'Api\Partner\PartnerController@MangeGallary')->middleware('auth:api');
	Route::any('managemenufoods', 'Api\Partner\PartnerController@MangeMenuFoods')->middleware('auth:api');
	Route::any('managejobs', 'Api\Partner\PartnerController@ManageJobs')->middleware('auth:api');
	Route::any('manageadvertisement', 'Api\Partner\PartnerController@MangeAdvertisement')->middleware('auth:api');
	Route::any('manageproducts', 'Api\Partner\PartnerController@MangeProducts')->middleware('auth:api');
	Route::any('manageoffers', 'Api\Partner\PartnerController@Mangeoffers')->middleware('auth:api');
	Route::any('manageevents', 'Api\Partner\PartnerController@MangeEvents')->middleware('auth:api');
	Route::any('managebookedevents', 'Api\Partner\PartnerController@MangeEventsBooked')->middleware('auth:api');
	Route::any('managecourse', 'Api\Partner\PartnerController@MangeCourses')->middleware('auth:api');
	Route::any('contactlist', 'Api\Partner\PartnerController@MangeLeads')->middleware('auth:api');
	Route::any('managekeywords', 'Api\Partner\PartnerController@MangeKeyword')->middleware('auth:api');
	Route::any('reviews', 'Api\Partner\PartnerController@MangeReview')->middleware('auth:api');
	Route::any('managefeedback', 'Api\Partner\PartnerController@MangeFeedback')->middleware('auth:api');

	Route::get('partnerdetails', 'Api\Partner\PartnerController@partnerdetails')->middleware('auth:api');
	Route::get('dashboared', 'Api\Partner\PartnerController@partnerdashboard')->middleware('auth:api');

	//five steps
	Route::post('profileimageupdate', 'Api\Partner\PartnerController@Profileimageupdate')->middleware('auth:api');
	Route::any('partnerInfo', 'Api\Partner\PartnerController@partnerInfo')->middleware('auth:api');
	Route::any('partnerContactdetails', 'Api\Partner\PartnerController@partnerContactdetails')->middleware('auth:api');
	Route::any('bankdetails', 'Api\Partner\PartnerController@bankdetails')->middleware('auth:api');
	Route::any('subscription', 'Api\Partner\PartnerController@subscription')->middleware('auth:api');
	//five steps

	Route::any('partnerlogout', 'Api\Partner\PartnerController@partnerlogout')->middleware('auth:api');
	Route::post('changepassword', 'Api\Partner\PartnerController@changepassword')->middleware('auth:api');
    Route::post('purchase_subscription', 'Api\Partner\PartnerController@purchase_subscription')->middleware('auth:api');
    Route::post('my_subscribed_plan', 'Api\Partner\PartnerController@my_subscribed_plan')->middleware('auth:api');
    Route::any('my_wallet', 'Api\Partner\PartnerController@partnerWallet')->middleware('auth:api');
    Route::any('managebookAvailability', 'Api\Partner\PartnerController@bookAvailability')->middleware('auth:api');
    Route::any('profile_status', 'Api\Partner\PartnerController@partnerStatus')->middleware('auth:api');
    Route::any('analytics_report', 'Api\Partner\PartnerController@analytics_report')->middleware('auth:api');

});
