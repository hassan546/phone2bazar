<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::group(['prefix' => 'admin',  'middleware' => 'prevent-back-history'], function()
{
     Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');

	Route::get('forgetpasword', 'Auth\AdminForgotPasswordController@showLinkRequestFormAdmin')->name('admin.forgetpassword');
    Route::post('password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');


	Route::post('password/reset', 'Auth\AdminResetPasswordController@reset')->name('admin.password.update');
    Route::get('password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

    Route::post('/', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('dashboard', 'Admin\AdminController@index')->name('admin.home');
	Route::any('settings', 'Admin\AdminController@Settings')->name('admin.settings');
	Route::any('adminprofileupdate', 'Admin\AdminController@adminprofileupdate')->name('admin.adminprofileupdate');
	//Page Management
	Route::resource('page-management','Admin\PageController');
	Route::post('change_statuspage','Admin\PageController@ChangeStatusPage')->name('change_statuspage');

    //Banner Management
	Route::resource('promotional-banner-management','Admin\BannerController');
	Route::post('change_status2','Admin\BannerController@ChangeStatus')->name('change_statusbanner');

	//Subscription Management
	Route::resource('subscriiption-management','Admin\SubscriptionController');
	Route::post('change_status_subscription','Admin\SubscriptionController@ChangeStatus')->name('change_status_subscription');


	//Countyr Management
	Route::resource('country-management','Admin\CountryController');
	Route::post('change_statuscountr','Admin\CountryController@change_statuscountr')->name('change_statuscountr');

	//State Management
	Route::resource('state-management','Admin\StateController');
	Route::post('change_statusstate','Admin\StateController@Setstatus')->name('change_statusstate');

	//News/Reviews Management
	Route::resource('news-management','Admin\NewsController');
	Route::post('change_statusnews','Admin\NewsController@ChangeStatus')->name('change_statusnews');
	Route::resource('newsreview','Admin\NewsReviewController');
	Route::post('change_review','Admin\NewsReviewController@Setstatus')->name('setstatus');


   //Blog/Reviews Management
	Route::resource('blogs-management','Admin\BlogsController');
	Route::post('change_statusblog','Admin\BlogsController@ChangeStatus')->name('change_statusblog');
	Route::resource('blogreview','Admin\BlogsReviewController');
	Route::post('change_review_blog','Admin\BlogsReviewController@Setstatus')->name('change_review_blog');

	//Occasion/Reviews Management
	Route::resource('occasion-management','Admin\OccasionController');
	Route::post('change_statusoccasion','Admin\OccasionController@ChangeStatus')->name('change_statusoccasion');
	Route::resource('occasionreview','Admin\OccasionReviewController');
	Route::post('change_review_occasion','Admin\OccasionReviewController@Setstatus')->name('change_review_occasion');
	Route::post('get-areaadmin','Admin\OccasionController@GetArea')->name('get.areaadmin');


		//Testimonial Management
	Route::resource('testimonial-management','Admin\TestimonialController');
	Route::post('change_statustestimonial','Admin\TestimonialController@ChangeStatus')->name('change_statustestimonilal');

	//Notification Management
	Route::resource('notifcation-management','Admin\NotificaitonController');
	Route::post('change_statusnotification','Admin\NotificaitonController@ChangeStatus')->name('change_statusnotification');

	//Chat Management
	Route::resource('chat-management','Admin\ChatController');
	Route::post('change_statuschat','Admin\ChatController@ChangeStatus')->name('change_statuschat');

	//Promocode Management
	Route::resource('promocode-management','Admin\PromoCodeController');
	Route::post('change_statuspromo','Admin\PromoCodeController@ChangeStatus')->name('change_statuspromo');


	//Partner Event Management
	Route::resource('adminpartner-eventmangement','Admin\PartnerEventController');
	Route::any('manage-eventstatus', 'Admin\PartnerEventController@ChangeStatus')->name('manage-eventstatus');

	//Partner Rating Management
	Route::resource('adminpartner-ratingmangement','Admin\PartnerRatingController');
	Route::any('manage-ratingstatus', 'Admin\PartnerRatingController@ChangeStatus')->name('manage-ratingstatus');

    //Partner Event Booked Management
    Route::post('adminpartner-eventbookmangement','Admin\PartnerBookAvlController@index');
	Route::resource('adminpartner-eventbookmangement','Admin\PartnerBookAvlController',['except' => ['store']]);
    Route::any('manage-eventbookstatus', 'Admin\PartnerBookAvlController@ChangeStatus')->name('manage-eventbookstatus');



  //Booling Category Management
	Route::resource('category-management','Admin\CategoryController');
	Route::post('change_status','Admin\CategoryController@ChangeStatus')->name('change_statuscategory');

	//Sub Category Management
	Route::resource('subcategory-management','Admin\SubCategoryController');
	Route::post('change_statussubcategory','Admin\SubCategoryController@ChangeStatus')->name('change_statussubcategory');
	Route::post('change_statussubcategory2','Admin\SubCategoryController@ChangeStatus2')->name('change_statussubcategory2');

  //Sub Category Management
	Route::resource('tags-management','Admin\TagController');
	Route::post('change_statustags','Admin\TagController@ChangeStatus')->name('change_statustags');
	Route::post('change_statustags2','Admin\TagController@ChangeStatus2')->name('change_statustags2');

	//Organization Management
	Route::resource('organizaiton-management','Admin\OrganizatonController');
	Route::post('change_statusamenity','Admin\OrganizatonController@ChangeStatus')->name('change_statusamenity');

	//Booling Area Management
	Route::resource('area-management','Admin\AreaController');
	Route::post('change_statusarea','Admin\AreaController@ChangeStatus')->name('change_statusarea');
	Route::post('get-city','Admin\CityController@GetCity')->name('get.city');


	//City Room Category Management
	Route::resource('city-management','Admin\CityController');
	Route::post('setstatus-city', 'Admin\CityController@Setstatus')->name('setstatus_city');
	Route::post('setstatus-city3', 'Admin\CityController@Setstatus3')->name('setstatus_city3');
	Route::post('set_city_nav', 'Admin\CityController@set_city_nav')->name('set_city_nav');
	Route::post('get-country','Admin\CityController@GetCountry')->name('get.country');

	//Customer Management
	Route::resource('customer-management','Admin\AdminCustomerController');
	Route::post('change_statuscustomer','Admin\AdminCustomerController@ChangeStatusCustomer')->name('change_statuscustomer');
	Route::any('add-amount-customer/{id?}','Admin\AdminCustomerController@AddAmounttoCustoemr')->name('addamounttocustoemr');

	Route::resource('partner-management','Admin\PartnerController');
	Route::post('change_statuspartner','Admin\PartnerController@ChangeStatus')->name('change_statuspartner');
	Route::post('change_statuspartner2','Admin\PartnerController@ChangeStatus2')->name('change_statuspartner2');
    Route::post('add_amount','Admin\PartnerController@AddAmountPartner')->name('add_amount');
	Route::post('change_statuspartner_review','Admin\PartnerController@ChangeStatusReviws')->name('change_statuspartner_review');
	Route::post('get-location','Admin\AreaController@GetLocation')->name('get.location');
	Route::post('get-pincode','Admin\AreaController@GetPincode')->name('get.pincode');
	Route::post('get-publishedprice','Admin\HotelierController@GetPublishedprice')->name('get.publishedprice');

	Route::post("remove-image","Admin\HotelierController@removeImage")->name("remove.image");
	Route::post("thum-image","Admin\HotelierController@thumbImage")->name("thumb.image");

	Route::get('hotelier-management-registration', 'Admin\HotelierController@HotelierRegistration')->name('hotelier-management-registration');
	//Price setup
	Route::resource('room-details','Admin\RoomDetailController');
	Route::post('get-hotel-total-room','Admin\RoomDetailController@GetTotalRome')->name('get.Total.rooms');

	Route::resource('hotelier-slot','Admin\HotelierSlotController');
	Route::post('hotelier-slot-status','Admin\HotelierSlotController@Setstatus')->name('setstatus-slot');
    Route::post('get-roomdetails','Admin\HotelierSlotController@GetRoom')->name('get-roomdetails');

    // Wallet Transaction
    Route::post('wallet-transaction','Admin\WalletTransactionController@index');
    Route::resource('wallet-transaction','Admin\WalletTransactionController',['except' => ['store']]);

    // Purchased Subscription
    Route::post('purchased-subscription','Admin\PartnerSubscriptionController@index');
    Route::resource('purchased-subscription','Admin\PartnerSubscriptionController',['except' => ['store']]);

    //Advertisement Management
    Route::post('adminpartner-advertisement','Admin\PartnerAdvertisementController@index');
	Route::resource('adminpartner-advertisement','Admin\PartnerAdvertisementController',['except' => ['store']]);
    Route::any('manage-advertisementstatus', 'Admin\PartnerAdvertisementController@ChangeStatus')->name('manage-advertisementstatus');

    //Offer Management
    Route::post('offer-management','Admin\ManageOfferController@index');
	Route::resource('offer-management','Admin\ManageOfferController',['except' => ['store']]);
    Route::post('change_statusoffer','Admin\ManageOfferController@change_statusoffer')->name('change_statusoffer');


	//amdin logout
    Route::post('get-subcategory','Admin\PartnerController@Getsubcategory')->name('get-subcategory');
    Route::any('refered_employee', 'Admin\AdminController@referedEmployee')->name('admin.refered_employee');
    Route::any('getChatCount', 'Admin\AdminController@getChatCount')->name('admin.getChatCount');
    Route::get('logout', 'Auth\AdminLoginController@logout')->name('admin.logout');


});

Auth::routes();
