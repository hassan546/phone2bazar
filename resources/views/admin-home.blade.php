@extends('layouts.adminapp')
@section('titletag')
Dashboard
@stop
@section('content')
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-md-8">
                <h4>Dashboard</h4>
              </div>
            </div>
            <!-- End Page Header -->

			 <!-- Small Stats Blocks -->
           <div class="row">

              <div style="cursor:pointer;" onclick="window.location.href='{{route('partner-management.index')}}'" class="col-lg-6 col-md-2 col-sm-2 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                         <span class="stats-small__label text-uppercase">Total Vendor/Partner</span>
                        <h6 class="stats-small__value count my-3">{{  $totalpartners }}</h6>
                      </div>
                     <!--  <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
                      </div> -->
                    </div>
                    <!-- <canvas height="120" class="blog-overview-stats-small-2"></canvas> -->
                  </div>
                </div>
              </div>
			  <div style="cursor:pointer;" onclick="window.location.href='{{route('customer-management.index')}}'" class="col-lg-6 col-md-2 col-sm-2 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Total Customer</span>
                        <h6 class="stats-small__value count my-3">{{  $total_users }}</h6>
                      </div>
                     <!--  <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
                      </div> -->
                    </div>
                    <!-- <canvas height="120" class="blog-overview-stats-small-2"></canvas> -->
                  </div>
                </div>
              </div>
			  </div>


            <!-- End Small Stats Blocks -->

          </div>
@stop
@section('animated')
@stop
@section('modal')
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title">Modal Header <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@stop


