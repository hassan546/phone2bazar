<html class="no-js h-100" lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>@yield('titletag') | Phone2bazaar</title>

    <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >

    <link rel="stylesheet" id="main-stylesheet" data-version="1.0.0" href="{{ asset('public/assets/admin/styles/shards-dashboards.1.0.0.min.css')}}">

    <link rel="stylesheet" href="{{ asset('public/assets/admin/styles/extras.1.0.0.min.css')}}">

    <link rel="stylesheet" href="{{ asset('public/assets/admin/css/styles.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/admin/css/sweetalert.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />



    <script async defer src="https://buttons.github.io/buttons.js"></script>

  </head>

<body class="h-100">



    <div class="container-fluid">

      <div class="row">

        <!-- Main Sidebar -->

 @include('admin.inc.nav')

        <!-- End Main Sidebar -->

        <main class="main-content col-lg-12 col-md-12 col-sm-12" style="padding-left: 55px;">

           @include('admin.inc.nav-top')

          <!-- / .main-navbar -->

        @yield('pagecss')

		@yield('content')

		@include('admin.inc.footer')



        </main>

      </div>

    </div>

@yield('animated')

@include('admin.inc.commonscript')

@yield('pagescript')

@yield('modal')

  </body>

</html>

