<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		 <title>@yield('titletag') | Phone2bazaar</title>
		<meta name="description" content="@yield ('metadescription')">
	   <meta name="keywords" content="@yield ('metakeyword')">
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" href="{{ asset('public/assets/frontend/css/bootstrap.min.css') }}" />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
			rel="stylesheet">
		<link
			href="https://fonts.googleapis.com/css?family=Philosopher:400,700|Ubuntu:300,400,400i,500,700"
			rel="stylesheet">
		<link rel="stylesheet" type="text/css"
			href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link
			href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
			type="text/css" rel="stylesheet" media="screen,projection" />
        <link href="https://pennypixels.pennymacusa.com/css/1_4_1/pp.css">
		<link rel="stylesheet" href="{{ asset('public/assets/frontend/css/layout.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/frontend/css/responsive.css') }}">
	</head>
<body>
    @include('frontend.inc.nav-top')
	 @yield('pagecss')
	@yield('content')

    @include('frontend.inc.footer_top')
    @include('frontend.inc.footer')
    @include('frontend.inc.footer_bottom')
    @include('frontend.inc.commonscript')
	 @yield('pagescript')
</body>
</html>
