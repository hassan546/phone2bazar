@extends('layouts.adminapp')
@section('titletag')
City Management
@stop
@section('pagecss')
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
@stop
@section('content')
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">City Management</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
            @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">


					<button onclick="window.location.href='{{route('city-management.create')}}';" class="btn btn-sm btn-accent ml-auto">
                          <i class="material-icons">file_copy</i> Add</button>


                     <table class="table mb-0 table" id="example">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">Serial No</th>
						  <th scope="col" class="border-0">City Name</th>
                          <th scope="col" class="border-0">State Name</th>
						  <!-- <th scope="col" class="border-0">Show in List</th> -->
                          <th scope="col" class="border-0">Active/Inactive</th>
						  <th scope="col" class="border-0">Action</th>
						  <!--
						  <th scope="col" class="border-0">Display to Nav</th>
						  !-->

                        </tr>
                      </thead>
					@php
					$count=0;
					@endphp
                      <tbody>
					   @foreach ($cities as $cat)
                      <tr>
                      <td>{{ ++$count }}</td>
					   <td>{{ $cat->city_name }}</td>
                      <td>{{ $cat->state_name }}</td>

					 <!--<td> <input type="checkbox"  name="display" onclick="clickDisplay3({{$cat->city_id}})" {{ $cat->show_in_listing ? "checked" : ""}} /> </td>-->
					  <td> <input type="checkbox"  name="display" onclick="clickDisplay2({{$cat->city_id}})" {{ $cat->is_active ? "checked" : ""}} /> </td>

					  <!--
                      <td> <input type="checkbox" id="display{{ $cat->city_id }}" name="display" onclick="clickDisplay({{ $cat->city_id }})" {{ $cat->is_display ? "checked" : ""}}/> </td>
					  !-->

					  <td><a href="{{ route('city-management.edit',array($cat->city_id)) }}">Edit</a> | <a href="javascript:void(0);" onclick="delete_field('delete-form{{ $cat->city_id }}')">Delete</a>
						 <form action="{{route('city-management.destroy',[$cat->city_id])}}" method="POST" style="display: none;" id="delete-form{{ $cat->city_id }}">
				@method('DELETE')
				@csrf
				</form>

						 </td>

                    </tr>
						 @endforeach


                      </tbody>
                    </table>
                  </div>



                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script>
      $(document).ready(function() {
    $('#example').DataTable();
} );
function clickDisplay2(id){
  $.ajax({
   type: "POST",
   dataType: "json",
   url:  "{{route('setstatus_city')}}",
  data:({
	  id : id, _token: "{{csrf_token()}}",status:'checked'
	}),
   success: function(data)
   {
	   alert(data.status);
  }
 });

}


function clickDisplay3(id){
  $.ajax({
   type: "POST",
   dataType: "json",
   url:  "{{route('setstatus_city3')}}",
  data:({
	  id : id, _token: "{{csrf_token()}}",status:'checked'
	}),
   success: function(data)
   {
	   alert(data.status);
  }
 });

}


	function delete_field(argument)
  {
    if(confirm("Are You Sure to Delete"))
    {
      $("#"+argument).submit();
    }
    else
    {
      return false;
    }
  }


function clickDisplay(id){
var url = "{{ url('')}}";
if($("#display"+id).is(":checked")){
  $.ajax({
	type: "POST",
	 url:  "{{route('set_city_nav')}}",
	data:({
	  id : id, _token: "{{csrf_token()}}",status:'checked'
	}),
	cache: false,
	success: function(data){
	 if(data==9){
		alert("Only 9 city will select to display for nav");
		return false;
	 }else{
		alert("Success! selected successfully");
	 }
	}
  });
}else{

  $.ajax({
	type: "POST",
	 url:  "{{route('set_city_nav')}}",
	data:({
	  id : id, _token: "{{csrf_token()}}",status:'unchecked'
	}),
	cache: false,
	success: function(data){
	  alert("Success! Un-selected successfully");
	}
  });
}
}
</script>
@stop
