@extends('layouts.adminapp')
@section('titletag')
City Management : Edit
@stop
@section('content')

          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">City Management : Edit</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">


              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         {!! Form::model($categorylist, ["route"=>['city-management.update',$categorylist->city_id],'files'=>true]) !!}

@method('PUT')
						<div class="form-row">
						   @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif
                            </div>
                            <div class="form-row">
	                                 <div class="form-group col-md-3">
                                <label for="#DDDDDD"> {!! Form::label('city_name','City Name *', []) !!}</label>
										{!! Form::text('city_name',null, ['class'=>'form-control','id'=>'city_name','maxlength'=>'30','required']) !!}

										 @if ($errors->has('city_name'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('city_name') }}</strong>
                        </span>
                     @endif
                                 </div>


                              <div class="form-group col-md-3">
                                <label for="fePassword"> {!! Form::label('state_id','State Name', []) !!}</label>
								<?php

								$stateid = App\State::whereStateId($categorylist->state_id)->first();

								?>
								   {!! Form::select('state_id',$statenew,$stateid->state_id, ['class'=>'form-control','placeholder'=>'Select State','required']) !!}
								  @if ($errors->has('state_id'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('state_id') }}</strong>
                        </span>
                     @endif
                                 </div>




                            </div>




                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('city-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<script>
 $(function () {
      $('#city_name').on('keypress', function (e) {
        if((e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || e.which == 32)  {

}else{
    e.preventDefault();
}

      });
  });
</script>
<script type="text/javascript" src="{{ URL::asset('public/storage/js/plugin/ckeditor/ckeditor.js') }}"></script>
 <script type="text/javascript">

$("#country_id").change(function(){

	$.post('{{ route('get.country') }}', {country_id:$("#country_id").val(), "_token": "{{ csrf_token() }}" }, function(data, textStatus, xhr) {
	$("#state_id").empty();
	$.each(data, function(index, val) {
	  value="{{ $categorylist->state_id }}";
	  //alert(val.city_id);
	  select=val.state_id==value?'selected':'';
		 $("#state_id").append("<option value='"+val.state_id+"' "+select+">"+val.state+"</option>");

	});
	});
}).change();
 CKEDITOR.replace('description' );
</script>
@stop
