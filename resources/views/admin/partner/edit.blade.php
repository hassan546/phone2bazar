@extends('layouts.adminapp')

@section('titletag')

Partner Management : Edit

@stop

@section('content')

          <!-- / .main-navbar -->
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
            <span class="clearfix"></span>
            <!-- Page Header -->

           <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Partner Management : Edit</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                      @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif
                        <div class="col">
                         {!! Form::model($customerdetails, ["route"=>['partner-management.update',$customerdetails->id],'files'=>true]) !!}
@method('PUT')
						<div class="form-row">


                            </div>

                            <div class="form-row">

	                                 <div class="form-group col-md-3">

                                <label for="#DDDDDD"> {!! Form::label('company_name','Company Name *', []) !!}</label>

										{!! Form::text('company_name',null, ['class'=>'form-control','id'=>'company_name','placeholder'=>'Company Name','required','maxlength'=>'25']) !!}



										 @if ($errors->has('company_name'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('company_name') }}</strong>

                        </span>

                     @endif

                                 </div>


								 <div class="form-group col-md-3">

                                <label for="#DDDDDD"> {!! Form::label('email','Email', []) !!}</label>

										{!! Form::email('email',null, ['class'=>'form-control','id'=>'email' ,'placeholder'=>'Email','maxlength'=>'115']) !!}

										 @if ($errors->has('email'))

                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                        </span>
                     @endif
							</div>
                            <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('password','Password *', []) !!}</label>

        {!! Form::text('textpassword',null, ['class'=>'form-control','id'=>'password','placeholder'=>'Password','readonly','maxlength'=>'15']) !!}

         @if ($errors->has('password'))

<span class="text-danger" role="alert">

<strong>{{ $errors->first('password') }}</strong>

</span>

@endif

 </div>
                            <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('mobile_number','Mobile Number *', []) !!}</label>

        {!! Form::text('mobile_number',null, ['class'=>'form-control','id'=>'mobile_number','placeholder'=>'Mobile Number','readonly','maxlength'=>'10']) !!}

         @if ($errors->has('mobile_number'))

<span class="text-danger" role="alert">

<strong>{{ $errors->first('mobile_number') }}</strong>

</span>

@endif

 </div>
 </div>
 <div class="form-row">

 <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('whatsapp_number','WhatsApp Number *', []) !!}</label>

        {!! Form::text('whatsapp_number',null, ['class'=>'form-control','id'=>'whatsapp_number','placeholder'=>'Mobile Number','readonly','maxlength'=>'10']) !!}

         @if ($errors->has('whatsapp_number'))

<span class="text-danger" role="alert">

<strong>{{ $errors->first('whatsapp_number') }}</strong>

</span>

@endif

 </div>



 <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('state','State *', []) !!} </label>
                    {!! Form::select('state',$states,null, ['class'=>'form-control select2','placeholder'=>'Select State','required']) !!}

                      @if ($errors->has('state'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('state') }}</strong>

                        </span>

                     @endif

                </div>

            <div class="form-group col-md-3">

<label for="#DDDDDD">  {!! Form::label('city','City *', []) !!} </label>



                      <select name="city" id="city" class="form-control select2" required>



                      </select>

                      @if ($errors->has('city'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('city') }}</strong>

                        </span>

                     @endif



                </div>

            <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('category_id','Category *', []) !!}</label>



                   <!-- {!! Form::select('category_id',$categories,explode(',',$customerdetails->category_id),['class'=>'form-control','required']) !!} -->
                    {!! Form::select('category_id',$categories,null, ['class'=>'form-control select2','id'=>'category_id','placeholder'=>'Select Category','required']) !!}
                      @if ($errors->has('category_id'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('category_id') }}</strong>

                        </span>

                     @endif

                </div>

            </div>
            <div class="form-row">

            <div class="form-group col-md-3">

            <label for="#DDDDDD">
                {!! Form::label('category_id','Sub Category *', []) !!} </label>

                {!! Form::select('sub_cat_id',array(),null, ['class'=>'form-control select2','id'=>'sub_cat_id','required']) !!}

                @if ($errors->has('category_id'))

                    <span class="text-danger" role="alert">

                    <strong>{{ $errors->first('sub_cat_id') }}</strong>

                    </span>

                @endif

            </div>
<div class="form-group col-sm-3">

<label for="#DDDDDD"> {!! Form::label('name','Opening Time *', []) !!}</label></label>

{!! Form::select('openning_time',$viewtimeings,null, ['class'=>'form-control select2','id'=>'openning_time',]) !!}


    @if ($errors->has('openning_time'))

<span class="text-danger" role="alert">

<strong>{{ $errors->first('openning_time') }}</strong>

</span>

@endif

</div>

<div class="form-group col-sm-3">

<label for="#DDDDDD"> {!! Form::label('closing_time','Closing Time *', []) !!}</label>
 {!! Form::select('closing_time',$viewtimeings,null, ['class'=>'form-control select2','id'=>'closing_time',]) !!}

    @if ($errors->has('closing_time'))

<span class="text-danger" role="alert">

<strong>{{ $errors->first('closing_time') }}</strong>

</span>

@endif

</div>



                           <div class="form-group col-md-3">

                                <label for="#DDDDDD"> {!! Form::label('facebook','Facebook Link', []) !!}</label>

										{!! Form::text('facebook',null, ['class'=>'form-control','id'=>'facebook','placeholder'=>'Facebook Link',]) !!}

										 @if ($errors->has('facebook'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('facebook') }}</strong>

                        </span>

                     @endif

                                 </div>
                                 </div>
                                 <div class="form-row">

                                 <div class="form-group col-md-3">

                                <label for="#DDDDDD"> {!! Form::label('instagram','Instagram Link', []) !!}</label>

										{!! Form::text('instagram',null, ['class'=>'form-control','id'=>'instagram','placeholder'=>'Instagram']) !!}

										 @if ($errors->has('instagram'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('instagram') }}</strong>

                        </span>

                     @endif

                                 </div>
                                 <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('web_link','Website Link', []) !!}</label>

        {!! Form::text('web_link',null, ['class'=>'form-control','id'=>'web_link','placeholder'=>'web_link','maxlength'=>'115']) !!}

         @if ($errors->has('web_link'))

        <span class="text-danger" role="alert">

        <strong>{{ $errors->first('web_link') }}</strong>

        </span>

        @endif

        </div>
        <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('profile_image','Profile Image ') !!}( maximum 2MB)</label>
{!! Form::file('profile_image', ['class'=>'form-control','id'=>'profile_image']) !!}

        <span id="file_error"></span>

        <br>

         @if ($errors->has('profile_image'))

        <span class="text-danger" role="alert">

        <strong>{{ $errors->first('profile_image') }}</strong>

        </span>

    @endif
    <a href="{{ asset("public/storage/upload/partnerimage/$customerdetails->image") }}" target="blank"> <img  class="imagestack" src="{{ asset("public/storage/upload/partnerimage/$customerdetails->image") }}" style="width: 20%;padding: 10px; cursor:pointer; "></a>

 </div>
                        <div class="form-group col-md-3">
                        <label for="#DDDDDD">{!! Form::label('image','More Images *') !!}(maximum 2MB)</label>
                        {!! Form::file('image[]', ['class'=>'form-control','id'=>'gallery-photo-add','multiple',]) !!}
                                <span id="file_error"></span>
                                <br>
                                @if ($errors->has('image'))
                                <span class="text-danger" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                            @foreach($gallery as $gal)
                            <a href="{{ asset("public/storage/upload/partnergallery/$gal->image") }}" target="blank"><img  class="imagestack" src="{{ asset("public/storage/upload/partnergallery/$gal->image") }}" style="width: 20%;padding: 10px; cursor:pointer; "></a>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-row">

        <div class="form-group col-md-3">
            {!! Form::label('description','Description *', []) !!}
            {!! Form::textarea('description',null, ['class'=>'form-control','id'=>'description','rows'=>'2','placeholder'=>'Description','required','maxlength'=>'150']) !!}
            @if ($errors->has('description'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group col-md-3">
            {!! Form::label('address','Address *', []) !!}
            {!! Form::textarea('address',null, ['class'=>'form-control','id'=>'address','rows'=>'2','placeholder'=>'Address','required','maxlength'=>'100']) !!}
            @if ($errors->has('address'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('adddress') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group col-md-2">
            <input type="button" value="Locate on Map" class="btn" onclick="codeAddress()" style="margin-top:30px;">
        </div>
        <div class="form-group col-md-2">
            <label for="#DDDDDD"> {!! Form::label('latitude','Latitude *', []) !!}</label>
            {!! Form::text('latitude',null, ['class'=>'form-control','id'=>'latitude','placeholder'=>'Latitude','required','readonly']) !!}
            @if ($errors->has('latitude'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('latitude') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group col-md-2">
            <label for="#DDDDDD"> {!! Form::label('longitude','Longitude *', []) !!}</label>
            {!! Form::text('longitude',null, ['class'=>'form-control','id'=>'longitude','placeholder'=>'Longitude','required','readonly']) !!}
            @if ($errors->has('longitude'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('longitude') }}</strong>
                </span>
            @endif
        </div>

    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <div id="map_canvas" style="height:90%;top:30px;overflow: initial !important;width:100%;height: 400px;margin-bottom: 35px;"></div>
        </div>
    </div>
    <div class="form-row">
                        </div>
                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('partner-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>

                           {!! Form::close() !!}

                        </div>

                      </div>

                    </li>

                  </ul>

                </div>

              </div>



              </div>

            </div>

            <!-- End Default Light Table -->

          </div>



@stop

@section('pagescript')

<script type="text/javascript" src="{{ URL::asset('public/storage/js/plugin/ckeditor/ckeditor.js') }}"></script>

<script type="text/javascript">

	$("#category_id").change(function(){
            $.post('{{ route('get-subcategory') }}', {main_category:$("#category_id").val(), "_token": "{{ csrf_token() }}" }, function(data, textStatus, xhr) {

               $("#sub_cat_id").empty();
			     $("#tags").empty();
				 $("#sub_cat_id").append('<option  value="">Select Sub Category</option>');
                $.each(data, function(index, val) {
					value="{{ $customerdetails->sub_cat_id }}";

					select=val.id==value?'selected':'';
                     $("#sub_cat_id").append("<option value='"+val.id+"'  "+select+" >"+val.category+"</option>");
                });

				@if($customerdetails->sub_cat_id)
				$("#sub_cat_id").trigger("change");
			    @endif


            });
          }).change();
$("#state").change(function(){
    $.post('{{ route('get.city') }}', {state_id:$("#state").val(), "_token": "{{ csrf_token() }}" }, function(data, textStatus, xhr) {
        $("#city").empty();
        $.each(data, function(index, val) {
            value="{{ $customerdetails->city }}";
            //alert(val.city_id);
            select=val.city_id==value?'selected':'';
            $("#city").append("<option value='"+val.city_id+"' "+select+">"+val.city_name+"</option>");
            if(select=='selected'){
                value2="{{$customerdetails->area }}";
                $.post('{{ route('get.location') }}', {state_id:$("#state").val(),city_id:$("#city").val(), "_token": "{{ csrf_token() }}" }, function(data, textStatus, xhr) {
                    $("#location").empty();
                    $.each(data, function(index, val) {
                    //alert(val.location);
                        select2=val.id==value2?'selected':'';
                        $("#location").append("<option value='"+val.id+"' "+select2+" >"+val.locality+"</option>");
                    });
                });
            }
        });
    });
}).change();
function ThumImg(image,id,counter)
{
    var imagecount=$(".imagestack").length;
    $.post("{{ route('thumb.image') }}", {image:image,id:id,"_token":"{{ csrf_token() }}"}, function(data, textStatus, xhr) {
        $("#img"+id+counter).css("border","2px solid");
        $(".imagestack").each(function() {
            if(this.id!="img"+id+counter)
                $("#"+this.id+"").css("border","");
        });
    });
}

function removeImg(image,id,counter)
{
    var imagecount=$(".imagestack").length;
    if(imagecount==1)
    {

			$("#file_error").html("Please upload atleat one image then delete previous one");

			$(".demoInputBox").css("border-color","#FF0000");

			return false;

		}



          $("#fa"+id+counter).remove();

          $("#img"+id+counter).remove();

          $.post("{{ route('remove.image') }}", {image:image,id:id,"_token":"{{ csrf_token() }}"}, function(data, textStatus, xhr) {



          });

        }





$(function() {

    // Multiple images preview in browser

    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {

            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {

                var reader = new FileReader();



                reader.onload = function(event) {

                    $($.parseHTML('<img style="padding-right:5px;" width="41" height="31" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);

                }

                reader.readAsDataURL(input.files[i]);

            }

        }

    };

    $('#gallery-photo-add').on('change', function() {

		$('.gallery').html('');

        imagesPreview(this, 'div.gallery');

    });

});



CKEDITOR.replace('description' );

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJYMlJpOtVoDNWGic8udVeJApdnqghJas"></script>
<script>
var geocoder;
var map;
var marker;
var infowindow = new google.maps.InfoWindow({
  size: new google.maps.Size(150, 50)
});
function initialize() {

  geocoder = new google.maps.Geocoder();
  if({{$customerdetails->latitude}}){
    var latlng = new google.maps.LatLng({{$customerdetails->latitude}},{{$customerdetails->longitude}});
  }else{
    var latlng = new google.maps.LatLng(17.385044,78.486671);
  }

  var mapOptions = {
    zoom: 8,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  codeAddress();
  map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
  google.maps.event.addListener(map, 'click', function() {
    infowindow.close();
  });
}

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      marker.formatted_address = responses[0].formatted_address;
    } else {
      marker.formatted_address = 'Cannot determine address at this location.';
    }
    infowindow.setContent(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
    infowindow.open(map, marker);
    var coordinates = marker.getPosition().toUrlValue(6);
    var latlong = coordinates.split(",");
    $("#latitude").val(latlong[0]);
    $("#longitude").val(latlong[1]);
	$("#address").val(marker.formatted_address);
  });
}

function codeAddress() {
  var address = document.getElementById('address').value;
  geocoder.geocode({
    'address': address
  }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      if (marker) {
        marker.setMap(null);
        if (infowindow) infowindow.close();
      }
      marker = new google.maps.Marker({
        map: map,
        draggable: true,
        position: results[0].geometry.location
      });
      google.maps.event.addListener(marker, 'dragend', function() {
        geocodePosition(marker.getPosition());
      });
      google.maps.event.addListener(marker, 'click', function() {
        if (marker.formatted_address) {
            infowindow.setContent(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
      	    $("#address").val(address);
            var coordinates = marker.getPosition().toUrlValue(6);
            var latlong = coordinates.split(",");
            $("#latitude").val(latlong[0]);
            $("#longitude").val(latlong[1]);
        } else {

        	$("#address").val(address);
            infowindow.setContent(address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
            var coordinates = marker.getPosition().toUrlValue(6);
            var latlong = coordinates.split(",");
            $("#latitude").val(latlong[0]);
            $("#longitude").val(latlong[1]);
        }
        infowindow.open(map, marker);
      });
      google.maps.event.trigger(marker, 'click');
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

google.maps.event.addDomListener(window, "load", initialize);
</script>
@stop



