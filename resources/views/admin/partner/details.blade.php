@extends('layouts.adminapp')
@section('titletag')
Partner Management : Details
@stop
@section('content')
<span class="clearfix"></span>
    <div class="main-content-container container-fluid px-4">
        <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
                <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                    <h3 class="page-title">Partner Details</h3>
                </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="edit-profile update_log">
                        <div class="">
                            <div class="panel">
                                <div class="info_uploader" style="border:none;padding:0px;">
                                    <div class="tab-block" id = "tab-block">


                                        <div class="tab-cont">
                                            <!-- LOGO -->

                                            <!-- COMPANY INFO -->
                                            <div class="tab-pane">

                                                    <h4>Company Info</h4>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="avatar-upload">
                                                                <div class="avatar-edit">
                                                                    <label for="imageUpload"></label>
                                                                </div>
                                                            <div class="avatar-preview">
                                                                <div id="imagePreview" style="background-image: url({{($customerdetails->image ? asset('public/storage/upload/partnerimage/'.$customerdetails->image.'') : 'https://i.pravatar.cc/500?img=7' )  }}   );">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <input type="hidden" value="compnayinfor" name="type">
                                                        <div class="form-group col-md-6">
                                                            <label>Company Name *</label>

                                                            {!! Form::text('company_name',$customerdetails->company_name, ['class'=>'form-control','id'=>'company_name','placeholder'=>'Company Name','required']) !!}
                                                            @if ($errors->has('company_name'))
                                                                <span class="text-danger" role="alert">
                                                                    <strong>{{ $errors->first('company_name') }}</strong>
                                                                </span>
                                                            @endif

                                                        </div>
                                                  <div class="form-group col-md-6">
                                                            <label>Company Email</label>
                                                            {!! Form::text('email',$customerdetails->email, ['class'=>'form-control','id'=>'email','placeholder'=>'Company Email','readonly']) !!}
                                                            @error('email')
                                                            <span style="color:red;font-size:10px;" role="alert">
                                                                {{ $message }}
                                                            </span>
                                                            @enderror
                                                        </div>


                                                        <span class="clearfix"></span>
                                                        <div class="form-group col-md-4">
                                                            <label>Mobile Number</label>
                                                            {!! Form::text('mobile_number',$customerdetails->mobile_number, ['class'=>'form-control','id'=>'mobile_number','placeholder'=>'Mobile Number','readonly']) !!}
                                                            @error('mobile_number')
                                                                <span style="color:red;font-size:10px;" role="alert">
                                                                    {{ $message }}
                                                                </span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Whatsapp Number</label>
                                                            {!! Form::text('whatsapp_number',$customerdetails->whatsapp_number, ['class'=>'form-control','id'=>'whatsapp_number','placeholder'=>'Whatsapp Number','readonly']) !!}
                                                            @error('whatsapp_number')
                                                                <span style="color:red;font-size:10px;" role="alert">
                                                                {{ $message }}
                                                                </span>
                                                            @enderror
                                                        </div>





                                                            <div class="form-group col-md-6">
                                                            <label>Adress *</label>
                                                            {!! Form::text('address',$customerdetails->address, ['class'=>'form-control','id'=>'address','placeholder'=>'Address','required']) !!}
                                                            @if ($errors->has('address'))
                                                                    <span class="text-danger" role="alert">
                                                                        <strong>{{ $errors->first('address') }}</strong>
                                                                    </span>
                                                                @endif

                                                            </div>
                                                            <span class="clearfix"></span>

                                                            <div class="col-md-6 form-group">
                                                            <label>State *</label>
                                                             <div class="styled-select">
                                                            {!! Form::select('state',array($customerdetails->getState->state ?? ''),null, ['class'=>'form-control','id'=>'state','required']) !!}
                                                            @if ($errors->has('state'))
                                                                    <span class="text-danger" role="alert">
                                                                        <strong>{{ $errors->first('state') }}</strong>
                                                                    </span>
                                                                @endif
                                                            <span class="fa fa-sort-desc"></span>
                                                            </div>
                                                            </div>


                                                            <div class="col-md-6 form-group">
                                                            <label>City *</label>
                                                            <div class="styled-select">
                                                            {!! Form::select('city',array($customerdetails->getCity->city_name ?? ''),null, ['class'=>'form-control','id'=>'city','required']) !!}

                                                            <span class="fa fa-sort-desc"></span>
                                                            </div>
                                                            </div>



                                                            <div class="form-group col-md-6">
                                                            <label>Category *</label>
                                                            {!! Form::select('category_id',array($customerdetails->getcategory->category ?? ''),null, ['class'=>'form-control','id'=>'category_id','required']) !!}
                                                            @if ($errors->has('category_id'))
                                                                    <span class="text-danger" role="alert">
                                                                        <strong>{{ $errors->first('category_id') }}</strong>
                                                                    </span>
                                                                @endif

                                                            </div>

                                                            <div class="form-group col-md-6">
                                                            <label>Sub Category *</label>

                                                            {!! Form::select('sub_cat_id',array($customerdetails->getSubCategory->category ?? ''),null, ['class'=>'form-control','id'=>'sub_cat_id','required']) !!}




                                                            </div>




                                                            <div class="form-group col-md-6">
                                                            <label>Facebook</label>

                                                            {!! Form::text('facebook',$customerdetails->facebook, ['class'=>'form-control','id'=>'facebook','placeholder'=>'Facebook']) !!}


                                                            </div>

                                                            <div class="form-group col-md-6">
                                                            <label>Instagram</label>
                                                            {!! Form::text('instagram',$customerdetails->instagram, ['class'=>'form-control','id'=>'instagram','placeholder'=>'Instagram']) !!}
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                            <label>Web Site Link</label>
                                                            {!! Form::text('web_link',$customerdetails->web_link, ['class'=>'form-control','id'=>'web_link','placeholder'=>'Web Site Link']) !!}
                                                            </div>




                                                        </div>
                                                    </div>
                                                </div>
                                        </div>



                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!-- End Default Light Table -->
</div>


@stop
@section('pagescript')
<script>
$(document).ready(function(){

  var tabWrapper = $('#tab-block'),
      tabMnu = tabWrapper.find('.tab-mnu  li'),
      tabContent = tabWrapper.find('.tab-cont > .tab-pane');

  tabContent.not(':first-child').hide();

  tabMnu.each(function(i){
    $(this).attr('data-tab','tab'+i);
  });
  tabContent.each(function(i){
    $(this).attr('data-tab','tab'+i);
  });

  tabMnu.click(function(){
    var tabData = $(this).data('tab');
    tabWrapper.find(tabContent).hide();
    tabWrapper.find(tabContent).filter('[data-tab='+tabData+']').show();
  });

  $('.tab-mnu > li').click(function(){
    var before = $('.tab-mnu li.active');
    before.removeClass('active');
    $(this).addClass('active');
   });

});</script>
<script type="text/javascript" src="{{ URL::asset('public/storage/js/plugin/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
<script type="text/javascript">


  $(document).ready(function() {
  $('#state, #city,#category_id, #sub_cat_id ').select2();


})


      $("#category_id").change(function(){
          $.post('{{ route('get-subcategory') }}', {main_category:$("#category_id").val(), "_token": "{{ csrf_token() }}" }, function(data, textStatus, xhr) {

             $("#sub_cat_id").empty();
               $("#tags").empty();
               $("#sub_cat_id").append('<option  value="">Select Sub Category</option>');
              $.each(data, function(index, val) {
                  value="{{ $customerdetails->sub_cat_id }}";

                  select=val.id==value?'selected':'';
                   $("#sub_cat_id").append("<option value='"+val.id+"'  "+select+" >"+val.category+"</option>");
              });

              @if($customerdetails->sub_cat_id)
              $("#sub_cat_id").trigger("change");
              @endif


          });
        }).change();




        $("#state").change(function(){

          $.post('{{ route('get.city') }}', {state_id:$("#state").val(), "_token": "{{ csrf_token() }}" }, function(data, textStatus, xhr) {

             $("#city").empty();
              $.each(data, function(index, val) {
                  value="{{ $customerdetails->city }}";

                  select=val.city_id==value?'selected':'';


                   $("#city").append("<option value='"+val.city_id+"'  "+select+">"+val.city_name+"</option>");
              });

              @if($customerdetails->city)
              $("#city").trigger("change");
              @endif

          });
        }).change();



function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
          $('#imagePreview2').css('background-image', 'url('+e.target.result +')');
          $('#imagePreview2').hide();
          $('#imagePreview2').fadeIn(650);
      }
      reader.readAsDataURL(input.files[0]);
      var data = new FormData($('#imageuploadsubmit')[0]);
      var ctrlUrl = "";
       $.ajax({
             type: "POST",
             url: ctrlUrl,
             data:data,
             mimeType: "multipart/form-data",
             contentType: false,
             cache: false,
             processData: false,
             success: function(data)
             {
                   $('#modal23 p').html('<div class="alert alert-info">Profile Image has been updated</div>');
                  $('#modal23').modal('show');


              }
         });

  }
}
$("#imageUpload2").change(function() {
      readURL(this);
});




</script>
@stop

