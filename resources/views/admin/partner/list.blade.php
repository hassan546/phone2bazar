@extends('layouts.adminapp')
@section('titletag')
Partner Management
@stop
@section('pagecss')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
@stop
@section('content')
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Partner Management</h3>


              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->

            <div class="row">
            @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">

				   <button onclick="window.location.href='{{route('partner-management.create')}}';" class="btn btn-sm btn-accent ml-auto">
                          <i class="material-icons">file_copy</i> Add Partner</button>


                     <table class="table mb-0 table" id="example">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">Serial No</th>
                          <th scope="col" class="border-0">Partner ID </th>
                          <th scope="col" class="border-0">Company Name </th>
						              <th scope="col" class="border-0">Category  </th>
                          <th scope="col" class="border-0">Sub Category  </th>
						              <th scope="col" class="border-0">Subscription </th>
                          <th scope="col" class="border-0">Email </th>
                          <th scope="col" class="border-0">Mobile </th>
                          <th scope="col" class="border-0">Profile % </th>
						 <th scope="col" class="border-0">Wallet Balance(&#8377;)</th>
                         <th scope="col" class="border-0">Registered On </th>
                          <th scope="col" class="border-0">Status</th>
						              <th scope="col" class="border-0">Action</th>

                        </tr>
                      </thead>
					@php
					$count=0;
					@endphp
                      <tbody>

					   @foreach ($partners as $partner)
                       @php $color = ($partner->profile_percentage == 100) ? "green" : "red";	@endphp
                       <tr>
                          <td>{{ ++$count }}</td>
                          <td>{{$partner->unique_id }}</td>
                          <td>{{$partner->company_name }}</td>
                          <td>{{$partner->getcategory->category ?? '' }}</td>
                          <td>{{$partner->getSubCategory->category ?? '' }}</td>
                          <td>{{$partner->getSubscription->name ?? '' }}</td>
                          <td>{{$partner->email }}</td>
                          <td>{{$partner->mobile_number }}</td>
                          <td style="color:{{ $color }}">{{$partner->profile_percentage }} %</td>

						   <td> {{$partner->wallet_balance }}</td>
                           <td>{{$partner->created_at }}</td>
						  <td> <a style="cursor: pointer;" onclick="changeStatus({{ $partner->id }},'{{ $partner->status==0 ? "0" : "1"}}')">{{ $partner->status==1 ? "Active" : "Inactive"}} </a> </td>
                         <td><a href="{{ route('partner-management.edit',$partner->id) }}">View Details</a> |  <a href="javascript:void(0);" onclick="amount_popup('{{ $partner->id }}')">Add Amount</a>

						 </td>
                        </tr>
						 @endforeach


                      </tbody>
                    </table>
                  </div>



                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>
          <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Amount to Wallet</h4>
                      </div>
                      <form action="" method="post" >
                        @csrf
                      <div class="modal-body" style="height: 150px;">
											<div class="form-group">
												<label class="col-md-12">Amount</label>
												<div class="col-md-12">
													<input class="form-control form-control-line" maxlength="8" type="text" placeholder="Enter Amount"  id="amount" name="amount">
													<span class="amountErr" style="color: red;"></span>
												</div>
											</div>
                      </div>
                      <input type="hidden" name="user_id" id="user_id" >
                      <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="return submitAmount();">Submit</button>
                      </div>
                      </form>
                    </div>

                  </div>
                </div>

@stop
@section('pagescript')
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script>
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search" style="width:70%;" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    var table = $('#example').DataTable( {
        orderCellsTop: true,
        fixedHeader: false
    } );
    function amount_popup(partner_id){
        $('#myModal').modal('toggle');
        $('#myModal').modal('show');
        $("#user_id").val(partner_id);
    }
    $(document).ready(function() {

					 $("#amount").keydown(function(e) {
						 if(event.shiftKey && ((event.keyCode >=48 && event.keyCode <=57)
					             || (event.keyCode >=186 &&  event.keyCode <=222))){
					        // Ensure that it is a number and stop the Special chars
					         event.preventDefault();
					     }
					     else if ((event.shiftKey || event.ctrlKey) && (event.keyCode > 34 && event.keyCode < 40)){
					          // let it happen, don't do anything
					     }
					     else{
					        // Allow only backspace , delete, numbers
					        if (event.keyCode == 9 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 39 ||event.keyCode == 37
					                || (event.keyCode >=48 && event.keyCode <=57)) {
					            // let it happen, don't do anything
					        }
					        else {
					           // Ensure that it is a number and stop the key press
					                event.preventDefault();
					        }
					     }
						});
				});
  function delete_field(argument)
  {
    if(confirm("Are You Sure you want to Delete? If yes then it will delete also related\n data like reviews, slot, booking etc."))
    {
      $("#"+argument).submit();
    }
    else
    {
      return false;
    }
  }

	    function changeStatus(id,status)
		{
        var url = "{{ url('')}}";
          $.ajax({
            type: "POST",
            url: '{{route("change_statuspartner")}}',
            data:({
              id : id, _token: "{{csrf_token()}}",status: status
            }),
            cache: false,
            success: function(data){
             if(data==1){
                alert("Success! Status changed successfully");
                window.location.reload();
              }else{
                alert("Opps! Unable to change status.");
             }
            }
          });
      }

	  function changeStatus2(id,status)
		{
        var url = "{{ url('')}}";
          $.ajax({
            type: "POST",
            url: '{{route("change_statuspartner2")}}',
            data:({
              id : id, _token: "{{csrf_token()}}",status: status
            }),
            cache: false,
            success: function(data){
             if(data==1){
                alert("Success! Show in list status changed successfully");
                window.location.reload();
              }else{
                alert("Opps! Unable to change status.");
             }
            }
          });
      }



      $(document).ready(function() {
    $('#example').DataTable();
} );
function submitAmount(){
    var amount = $("#amount").val();
    var user_id = $("#user_id").val();
    $(".online_exam_idErr").html("");
	$(".online_exam_idErr").hide("");
    if(amount==0 || amount==""){
    	$(".amountErr").slideDown('slow');
		$(".amountErr").html("Please enter amount");
		$("#amount").focus();
		return false;
    }else{
        var res = confirm("Are you sure you want add amount?");
        if(res == true) {
        	var url= '{{route("add_amount")}}';
     		var adminRedirectUrl='{{route("partner-management.index")}}';
    	 // 	var redirectUrl
          $.ajax({
          	type: "POST",
            	url: url,

            	data : {id : user_id,   _token: "{{csrf_token()}}", amount: amount},
            	cache: false,
            	success: function(data)
            		{ //alert(data);
            	  		if(data == 1) {
            	    		swal("Success!","Amount has been added successfully","success");
            	    		setTimeout(function ()
    				 				{
    				        		window.location.href=adminRedirectUrl;
    				        	      },400);
            	    				}else {
    											swal("Error!","Please try again","error");
    											setTimeout(function ()
    			 				  		 {
    			        						window.location.href=adminRedirectUrl;
    			        	     		 },400);
    	              					}
    	           }

       		});
		}
    }
}
    </script>
@stop
