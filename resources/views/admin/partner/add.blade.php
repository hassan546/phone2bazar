@extends('layouts.adminapp')

@section('titletag')

Partner Management : Add

@stop

@section('content')
<style>
/* #map_canvas{
height:90%;
top:30px;
overflow: initial !important;
width:100% !important;
} */
</style>
          <!-- / .main-navbar -->
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
            <span class="clearfix"></span>
            <!-- Page Header -->

           <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Partner Management : Add</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                        {!! Form::open(['route'=>'partner-management.store','files'=>true]) !!}

						<div class="form-row">
						   @if ($message = Session::get('success'))

<div class="alert alert-success alert-dismissible fade show" role="alert">

            <i class="fa fa-check "></i>

          {{ $message }}</div>

@endif

                            </div>

                            <div class="form-row">

	                                 <div class="form-group col-md-3">

                                <label for="#DDDDDD"> {!! Form::label('name','Company Name *', []) !!}</label></label>

										{!! Form::text('company_name',null, ['class'=>'form-control','id'=>'company_name','placeholder'=>'Company Name','required','maxlength'=>'25']) !!}



										 @if ($errors->has('company_name'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('company_name') }}</strong>

                        </span>

                     @endif

                                 </div>


								 <div class="form-group col-md-3">

                                <label for="#DDDDDD"> {!! Form::label('email','Email', []) !!}</label>

										{!! Form::email('email',null, ['class'=>'form-control','id'=>'email' ,'required','placeholder'=>'Email','maxlength'=>'115']) !!}

										 @if ($errors->has('email'))

                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                        </span>
                     @endif
							</div>
                            <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('password','Password', []) !!}</label>

        {!! Form::text('password',null, ['class'=>'form-control','id'=>'password' ,'required','placeholder'=>'Password','maxlength'=>'15']) !!}

         @if ($errors->has('password'))

<span class="text-danger" role="alert">
<strong>{{ $errors->first('password') }}</strong>
</span>
@endif
</div>
<div class="form-group col-md-3">

                                <label for="#DDDDDD"> {!! Form::label('mobile_number','Mobile Number *', []) !!}</label>

										{!! Form::text('mobile_number',null, ['class'=>'form-control','id'=>'mobile_number','placeholder'=>'Mobile Number','required','maxlength'=>'10']) !!}

										 @if ($errors->has('mobile_number'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('mobile_number') }}</strong>

                        </span>

                     @endif

                                 </div>
                            </div>
                            <div class="form-row">

                                 <div class="form-group col-md-3">

                                <label for="#DDDDDD"> {!! Form::label('whatsapp_number','WhatsApp Number *', []) !!}</label>

										{!! Form::text('whatsapp_number',null, ['class'=>'form-control','id'=>'whatsapp_number','placeholder'=>'Mobile Number','required','maxlength'=>'10']) !!}

										 @if ($errors->has('whatsapp_number'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('whatsapp_number') }}</strong>

                        </span>

                     @endif

                                 </div>
                                 <div class="form-group col-md-3">


                                <label for="#DDDDDD">  {!! Form::label('state','State *', []) !!}</label>
                                {!! Form::select('state',$states,null, ['class'=>'form-control select2','placeholder'=>'Select State','required']) !!}

                                @if ($errors->has('state'))

                                    <span class="text-danger" role="alert">

                                    <strong>{{ $errors->first('state') }}</strong>

                                    </span>

                                @endif

                            </div>

                            <div class="form-group col-md-3">


                                <label for="#DDDDDD"> {!! Form::label('city','City *', []) !!}</label>


                                <select name="city" id="city" class="form-control select2" required>



                                </select>

                                @if ($errors->has('city'))

                                    <span class="text-danger" role="alert">

                                    <strong>{{ $errors->first('city') }}</strong>

                                    </span>

                                @endif
                                </div>
                                <div class="form-group col-md-3">


                                <label for="#DDDDDD">   {!! Form::label('category_id','Category *', []) !!}</label>




    {!! Form::select('category_id',$categories,null, ['class'=>'form-control select2','id'=>'category_id','placeholder'=>'Select Category','required']) !!}
      @if ($errors->has('category_id'))

        <span class="text-danger" role="alert">

          <strong>{{ $errors->first('category_id') }}</strong>

        </span>

     @endif
</div>

</div>


		<div class="row">


        <div class="form-group col-md-3">


<label for="#DDDDDD">   {!! Form::label('sub_cat_id','Sub Category *', []) !!}</label>

    {!! Form::select('sub_cat_id',array(),null, ['class'=>'form-control select2','id'=>'sub_cat_id','required']) !!}

    @if ($errors->has('sub_cat_id'))

        <span class="text-danger" role="alert">

          <strong>{{ $errors->first('sub_cat_id') }}</strong>

        </span>

     @endif

</div>

<div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('name','Opening Time *', []) !!}</label></label>

{!! Form::select('openning_time',$viewtimeings,null, ['class'=>'form-control select2','id'=>'openning_time',]) !!}


    @if ($errors->has('openning_time'))

<span class="text-danger" role="alert">

<strong>{{ $errors->first('openning_time') }}</strong>

</span>

@endif

</div>
<div class="form-group col-sm-3">

<label for="#DDDDDD"> {!! Form::label('closing_time','Closing Time *', []) !!}</label>
 {!! Form::select('closing_time',$viewtimeings,null, ['class'=>'form-control select2','id'=>'closing_time',]) !!}

    @if ($errors->has('closing_time'))

<span class="text-danger" role="alert">

<strong>{{ $errors->first('closing_time') }}</strong>

</span>

@endif

</div>
<div class="form-group col-md-3">

                                <label for="#DDDDDD"> {!! Form::label('facebook','Facebook Link', []) !!}</label>

										{!! Form::text('facebook',null, ['class'=>'form-control','id'=>'facebook','placeholder'=>'Facebook Link','maxlength'=>'115']) !!}

										 @if ($errors->has('facebook'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('facebook') }}</strong>

                        </span>

                     @endif

                                 </div>
        </div>


<div class="form-row">

                                 <div class="form-group col-md-3">

                                <label for="#DDDDDD"> {!! Form::label('instagram','Instagram Link', []) !!}</label>

										{!! Form::text('instagram',null, ['class'=>'form-control','id'=>'instagram','placeholder'=>'Instagram','maxlength'=>'115']) !!}

										 @if ($errors->has('instagram'))

                        <span class="text-danger" role="alert">

                          <strong>{{ $errors->first('instagram') }}</strong>

                        </span>

                     @endif

                                 </div>
                                 <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('website','Website Link', []) !!}</label>

        {!! Form::url('website',null, ['class'=>'form-control','id'=>'website','placeholder'=>'Website','maxlength'=>'115']) !!}

         @if ($errors->has('website'))

        <span class="text-danger" role="alert">

        <strong>{{ $errors->first('website') }}</strong>

        </span>

        @endif

        </div>
        <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('profile_image','Profile Image ') !!}( maximum 2MB)</label>
{!! Form::file('profile_image', ['class'=>'form-control','id'=>'profile_image']) !!}

        <span id="file_error"></span>

        <br>

         @if ($errors->has('profile_image'))

        <span class="text-danger" role="alert">

        <strong>{{ $errors->first('profile_image') }}</strong>

        </span>

    @endif


 </div>
 <div class="form-group col-md-3">

<label for="#DDDDDD"> {!! Form::label('image','More Images *') !!}(maximum 2MB)</label>
{!! Form::file('image[]', ['class'=>'form-control','id'=>'gallery-photo-add','multiple']) !!}

        <span id="file_error"></span>

        <br>

         @if ($errors->has('image'))

        <span class="text-danger" role="alert">

        <strong>{{ $errors->first('image') }}</strong>

        </span>

    @endif


 </div>
 </div>
    <div class="form-row">

        <div class="form-group col-md-3">
            {!! Form::label('description','Description *', []) !!}
            {!! Form::textarea('description',null, ['class'=>'form-control','id'=>'description','rows'=>'2','placeholder'=>'Description','required','maxlength'=>'150']) !!}
            @if ($errors->has('description'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group col-md-3">
            {!! Form::label('address','Address *', []) !!}
            {!! Form::textarea('address',"Hyderbad, India", ['class'=>'form-control','id'=>'address','rows'=>'2','placeholder'=>'Address','required','maxlength'=>'100']) !!}
            @if ($errors->has('address'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('adddress') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group col-md-2">
            <input type="button" value="Locate on Map" class="btn" onclick="codeAddress()" style="margin-top:30px;">
        </div>
        <div class="form-group col-md-2">
            <label for="#DDDDDD"> {!! Form::label('latitude','Latitude *', []) !!}</label>
            {!! Form::text('latitude',null, ['class'=>'form-control','id'=>'latitude','placeholder'=>'Latitude','required','readonly']) !!}
            @if ($errors->has('latitude'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('latitude') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group col-md-2">
            <label for="#DDDDDD"> {!! Form::label('longitude','Longitude *', []) !!}</label>
            {!! Form::text('longitude',null, ['class'=>'form-control','id'=>'longitude','placeholder'=>'Longitude','required','readonly']) !!}
            @if ($errors->has('longitude'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('longitude') }}</strong>
                </span>
            @endif
        </div>

    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <div id="map_canvas" style="height:90%;top:30px;overflow: initial !important;width:100%;height: 400px;margin-bottom: 35px;"></div>
        </div>
    </div>
 <div class="form-row">


                            </div>


                            <button type="submit" class="btn btn-accent">Submit</button>

							<button onclick="window.location.href='{{route('partner-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>

                           {!! Form::close() !!}

                        </div>

                      </div>

                    </li>

                  </ul>

                </div>

              </div>



              </div>

            </div>

            <!-- End Default Light Table -->

          </div>



@stop

@section('pagescript')

<script type="text/javascript" src="{{ URL::asset('public/storage/js/plugin/ckeditor/ckeditor.js') }}"></script>

<script type="text/javascript">
$("#category_id").change(function(){
    $.post('{{ route('get-subcategory') }}', {main_category:$("#category_id").val(), "_token": "{{ csrf_token() }}" }, function(data, textStatus, xhr) {
    $("#sub_cat_id").empty();
    $("#sub_cat_id").append("<option value=''>Select Sub Category</option>");
    $.each(data, function(index, val) {
         $("#sub_cat_id").append("<option value='"+val.id+"'>"+val.category+"</option>");
    });
});
}).change();


$("#state").change(function(){
    $.post('{{ route('get.city') }}', {state_id:$("#state").val(), "_token": "{{ csrf_token() }}" }, function(data, textStatus, xhr) {
    $("#city").empty();
    $("#city").append("<option value=''>Select City</option>");
    $.each(data, function(index, val) {
         $("#city").append("<option value='"+val.city_id+"'>"+val.city_name+"</option>");
    });
});
}).change();
CKEDITOR.replace('description' );
</script>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJYMlJpOtVoDNWGic8udVeJApdnqghJas"></script>
<script>
var geocoder;
var map;
var marker;
var infowindow = new google.maps.InfoWindow({
  size: new google.maps.Size(150, 50)
});
function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(17.385044,78.486671);
  var mapOptions = {
    zoom: 8,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  codeAddress();
  map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
  google.maps.event.addListener(map, 'click', function() {
    infowindow.close();
  });
}

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      marker.formatted_address = responses[0].formatted_address;
    } else {
      marker.formatted_address = 'Cannot determine address at this location.';
    }
    infowindow.setContent(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
    infowindow.open(map, marker);
    var coordinates = marker.getPosition().toUrlValue(6);
    var latlong = coordinates.split(",");
    $("#latitude").val(latlong[0]);
    $("#longitude").val(latlong[1]);
	$("#address").val(marker.formatted_address);
  });
}

function codeAddress() {
  var address = document.getElementById('address').value;
  geocoder.geocode({
    'address': address
  }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      if (marker) {
        marker.setMap(null);
        if (infowindow) infowindow.close();
      }
      marker = new google.maps.Marker({
        map: map,
        draggable: true,
        position: results[0].geometry.location
      });
      google.maps.event.addListener(marker, 'dragend', function() {
        geocodePosition(marker.getPosition());
      });
      google.maps.event.addListener(marker, 'click', function() {
        if (marker.formatted_address) {
            infowindow.setContent(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
      	    $("#address").val(address);
            var coordinates = marker.getPosition().toUrlValue(6);
            var latlong = coordinates.split(",");
            $("#latitude").val(latlong[0]);
            $("#longitude").val(latlong[1]);
        } else {

        	$("#address").val(address);
            infowindow.setContent(address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
            var coordinates = marker.getPosition().toUrlValue(6);
            var latlong = coordinates.split(",");
            $("#latitude").val(latlong[0]);
            $("#longitude").val(latlong[1]);
        }
        infowindow.open(map, marker);
      });
      google.maps.event.trigger(marker, 'click');
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

google.maps.event.addDomListener(window, "load", initialize);
</script>
@stop



