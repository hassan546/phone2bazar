@extends('layouts.adminapp')
@section('titletag')
Product Management : Reviews : Edit
@stop
@section('content')
          <!-- / .main-navbar -->
        
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
         
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Product Management : Reviews : Edit</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              
			
              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         {!! Form::model($review_details,["route"=>['showproduct_reviewsupdate'],'files'=>true]) !!}
						<div class="form-row">
						   @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif
                            </div>
							<input type="hidden" name="review_id" value="{{$review_details->id}}">
                            <div class="form-row">
	                                 <div class="form-group col-md-4">
                                <label for="#DDDDDD"> {!! Form::label('name','Name *', []) !!}</label>
										{!! Form::text('name',null, ['class'=>'form-control','id'=>'name','placeholder'=>'Name','required']) !!}
										
										 @if ($errors->has('name'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('name') }}</strong>
                        </span>
                     @endif
                                 </div>
                            </div>
                            <div class="form-group">
                              <label for="feInputAddress">Message *</label>
							  {!! Form::textarea('message',null, ['class'=>'form-control','id'=>'message','row'=>'2', 'required','placeholder'=>'Message']) !!}
							  @if ($errors->has('message'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('message') }}</strong>
                        </span>
						              @endif
							  
							  </div>
                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('product-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
                
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script>

$(document).ready(function() {
    $('.basic-muliple').select2({
    placeholder: "Select category"
});
});
</script>
<script type="text/javascript" src="{{ URL::asset('public/storage/js/plugin/ckeditor/ckeditor.js') }}"></script>
 <script type="text/javascript">
 
function  rowdelte2(id)
{
	$('#'+id+'').remove();
}

function duplicate(id)
{
	$("#"+id+"").clone().insertAfter("#"+id+":last");
}

$('#tablesattab2').on('click','.remove',function() {
	$(this).closest(".block").remove();
});

function showoption()
{
	 var option= $('#option').val();
		$.ajax({
           type: "GET",
		   dataType: "json",
           url:  "{{url('')}}/admin/showproduct_options/"+option,
           success: function(data)
           {
			   if(data.success){
					
				if($('#optheader').length)
				   {
					   if(!$('#optheader'+data.success3+'').length)
					   {
							if(!$('#optheader2'+data.success3+'').length)
								$('#tablesattab2').append(data.success5+data.success2);
							else
								$('#tablesattab2').append(data.success5+data.success2);
							
					   }
					   
				   }
				   else
				   $('#tablesattab2').append(data.success+data.success5+data.success4+data.success2);
					
			
			   }
			   
           }
         });
		 
	 
	 
	 
}

function rowdelte(id)
{
	$('#'+id+'').remove();
}
function showtr()
{
	 var att= $('#attribute').val();
		$.ajax({
           type: "GET",
		   dataType: "json",
           url:  "{{url('')}}/admin/showproduct_attributes/"+att,
           success: function(data)
           {
			   if(data.success){
				   
				   if($('#attheader').length)
				   {
					   if(!$('#attheader'+data.success3+'').length)
					   $('#tablesattab').append(data.success2);
					   
				   }
					else
					{
						
						
							$('#tablesattab').append(data.success+data.success2);
					}
			
			   }
			   
           }
         });
		 
	 
	 
	 
}

function checkall(label,t)
{
	if (t.is(':checked')) {
      $("."+label+"" ).prop( "checked", true );
    } else {
    $("."+label+"" ).prop( "checked", false );
    }
}





function ThumImg(image,id,counter) 
{
		var imagecount=$(".imagestack").length;
		
		 
  
          $.post("{{ route('thumb.image') }}", {image:image,id:id,"_token":"{{ csrf_token() }}"}, function(data, textStatus, xhr) {
			$("#img"+id+counter).css("border","2px solid");
			
			$(".imagestack").each(function() {  
					if(this.id!="img"+id+counter)
						$("#"+this.id+"").css("border","");
						
  
			}); 
  
          });
}
		
		

function removeImg(image,id,counter) 
        {
		   var imagecount=$(".imagestack").length;
		  
		if(imagecount==1)
		{
			$("#file_error").html("Please upload atleat one image then delete previous one");
			$(".demoInputBox").css("border-color","#FF0000");
			return false;
		}
		
          $("#fa"+id+counter).remove();
          $("#img"+id+counter).remove();
          $.post("{{ route('remove.image') }}", {image:image,id:id,"_token":"{{ csrf_token() }}"}, function(data, textStatus, xhr) {
            
          });
        }
		
	
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img style="padding-right:5px;" width="41" height="31" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };
    $('#gallery-photo-add').on('change', function() {
		$('.gallery').html('');
        imagesPreview(this, 'div.gallery');
    });
});

CKEDITOR.replace('description' );
</script> 
@stop

