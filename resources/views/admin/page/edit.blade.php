@extends('layouts.adminapp')
@section('titletag')
Page Management : Edit
@stop
@section('content')

          <!-- / .main-navbar -->
        
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
         
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Page Management : Edit</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              
			
              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         {!! Form::model($page, ["route"=>['page-management.update',$page->id],'files'=>true]) !!}

@method('PUT')
						<div class="form-row">
                           
					   @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif

                            </div>
                            <div class="form-row">
	                                 <div class="form-group col-md-6">
                                <label for="#DDDDDD"> {!! Form::label('title','Page Title *', []) !!}</label>
										{!! Form::text('title',null, ['class'=>'form-control','id'=>'title','placeholder'=>'Page Title','required']) !!}
										 @if ($errors->has('title'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('title') }}</strong>
                        </span>
                     @endif
					 
								
                                 </div>
								 
								  

                            </div>
							
			
                            <div class="form-group">
                              <label for="feInputAddress">Page Description</label>
  
							  {!! Form::textarea('content',null, ['class'=>'form-control','id'=>'content','row'=>'2','placeholder'=>'Page Description']) !!}
                             @if ($errors->has('content'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('content') }}</strong>
                        </span>
						              @endif
							  </div>

                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('page-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
                
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>


@stop
@section('pagescript')
  <script src="https://cdn.ckeditor.com/4.13.0/standard-all/ckeditor.js"></script>
<!--<script type="text/javascript" src="{{ URL::asset('public/storage/js/plugin/ckeditor/ckeditor.js') }}"></script>-->

 <script type="text/javascript">
 
  CKEDITOR.replace('content', {
      height: 300,

      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '<?php echo URL::to('/').'/public/storage/js/plugin/ckfinder/ckfinder.html'; ?>',
      filebrowserImageBrowseUrl: '<?php echo URL::to('/').'/public/storage/js/plugin/ckfinder/ckfinder.html?type=Images'; ?>',   
     filebrowserUploadUrl: '<?php echo URL::to('/').'/public/storage/js/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'; ?>',  
      filebrowserImageUploadUrl: '<?php echo URL::to('/').'/public/storage/js/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'; ?>'
    });
	
	

</script>



@stop
