@extends('layouts.adminapp')
@section('titletag')
Page Management List
@stop
@section('pagecss')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
@stop
@section('content')
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Page Management</h3>


              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">

              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">

				    <!--<button onclick="window.location.href='{{route('page-management.create')}}';" class="btn btn-sm btn-accent ml-auto">
                          <i class="material-icons">file_copy</i> Add</button> -->


                     <table class="table mb-0 table" id="example">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">Serial No</th>
                          <th scope="col" class="border-0">Page Title </th>
                          <th scope="col" class="border-0">Status</th>
						  <th scope="col" class="border-0">Action</th>

                        </tr>
                      </thead>
					@php
					$count=0;
					@endphp
                      <tbody>

					   @foreach ($pages as $page)
                       <tr>
                          <td>{{ ++$count }}</td>

                          <td>{{$page->title}}</td>


						  <td> <a style="cursor: pointer;" onclick="changeStatus({{ $page->id }},'{{ $page->status==0 ? "0" : "1"}}')">{{ $page->status==1 ? "Active" : "Inactive"}} </a> </td>


                         <td><a href="{{ route('page-management.edit',$page->id) }}">Edit</a> <!-- | <a href="javascript:void(0);" onclick="delete_field('delete-form{{ $page->id }}')">Delete</a>
						 <form action="{{route('page-management.destroy',[$page->id])}}" method="POST" style="display: none;" id="delete-form{{ $page->id }}">
				@method('DELETE')
				@csrf
				</form> -->

						 </td>
                        </tr>









						 @endforeach


                      </tbody>
                    </table>
                  </div>



                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script>

	function delete_field(argument)
  {
    if(confirm("Are You Sure to Delete"))
    {
      $("#"+argument).submit();
    }
    else
    {
      return false;
    }
  }

	      function changeStatus(id,status){
        var url = "{{ url('')}}";
          $.ajax({
            type: "POST",
            url: '{{route("change_statuspage")}}',
            data:({
              id : id, _token: "{{csrf_token()}}",status: status
            }),
            cache: false,
            success: function(data){
             if(data==1){
                alert("Success! Status changed successfully");
                window.location.reload();
              }else{
                alert("Opps! Unable to change status.");
             }
            }
          });
      }


      $(document).ready(function() {
    $('#example').DataTable();
} );
    </script>
@stop
