@extends('layouts.adminapp')
@section('titletag')
State Management
@stop
@section('pagecss')
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
@stop
@section('content')
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Reffered Employee List</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">

              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                     <table class="table mb-0 table" id="example">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">Serial No</th>
						  <th scope="col" class="border-0">Employee Number</th>
                          <th scope="col" class="border-0">Reffered Date</th>
                          <th scope="col" class="border-0">Partner Name</th>
						   <!--<th scope="col" class="border-0">Action</th>-->
						  <!--
						  <th scope="col" class="border-0">Display to Nav</th>
						  !-->
                        </tr>
                      </thead>
					@php
					$count=0;
					@endphp
                      <tbody>
					   @foreach ($employee as $cat)
                      <tr>
                      <td>{{ ++$count }}</td>
                      <td>{{ $cat->mobile_number }}</td>
					  <td> {{ $cat->created_at }} </td>
                      <td>{{$cat->getPartner->company_name ?? ''}}</td>
					  <!--
                      <td> <input type="checkbox" id="display{{ $cat->city_id }}" name="display" onclick="clickDisplay({{ $cat->city_id }})" {{ $cat->is_display ? "checked" : ""}}/> </td>
					  !-->
					 <!-- <td><a href="javascript:void(0);" onclick="delete_field('delete-form{{ $cat->id }}')">Delete</a>
						 <form action="{{route('state-management.destroy',[$cat->id])}}" method="POST" style="display: none;" id="delete-form{{ $cat->id }}">
				@method('DELETE')
				@csrf
				</form>
						 </td> -->
                    </tr>
						 @endforeach


                      </tbody>
                    </table>
                  </div>



                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script>
 $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search" style="width:70%;" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    var table = $('#example').DataTable( {
        orderCellsTop: true,
        fixedHeader: false
    } );
      $(document).ready(function() {
    $('#example').DataTable();
} );


	function delete_field(argument)
  {
    if(confirm("Are You Sure to Delete"))
    {
      $("#"+argument).submit();
    }
    else
    {
      return false;
    }
  }

</script>
@stop
