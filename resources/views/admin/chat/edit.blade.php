@extends('layouts.adminapp')
@section('titletag')
Chat Management : Edit
@stop
@section('content')

<style type="text/css">
.ul {
	list-style-type: none;
}

table {
	width: 100%;
}

.horizontal-table th {
	width: 30%;
}
/* .horizontal-table th{
                   border-top: 0px;
                   }
                   .horizontal-table td{
                   border-top: 0px;
                   }*/
.chat-box ul li {
	display: block;
	width: 70%;
}

.chat-box ul .msg {
	float: left;
}

.chat-box ul .msg span {
	float: left;
	float: left;
	margin-bottom: 10px;
	font-size: 10px;
	color: #97D;
}

.chat-box ul li p {
	margin-bottom: 3px;
	color: #000;
}

.chat-box ul .reply span {
	float: right;
	margin-bottom: 10px;
	font-size: 10px;
	color: #97D;
}

.chat-box ul .msg p {
	background: #ddd;
	padding: 10px 15px;
    border-top-left-radius: 17px;
    border-top-right-radius: 17px;
    border-bottom-right-radius: 17px;
}

.chat-box ul .reply p {
	background: #91E0B7;
	padding: 7px 15px;
    border-top-left-radius: 17px;
    border-top-right-radius: 17px;
    border-bottom-left-radius: 17px;
}

.chat-box ul .reply {
	float: right;
	text-align: right;
}
.chat-box ul{
    padding-left:0;
}
.card {
    padding:60px;
}
</style>

<!-- / .main-navbar -->
<span class="clearfix"></span>
<div class="main-content-container container-fluid px-4">
    <span class="clearfix"></span>
    <!-- Page Header -->
     <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
        <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
            <h3 class="page-title">Chat Management : Edit</h3>
        </div>
    </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-small mb-4">

                <div class="chat-box">
                    <div class="row">
                        <div class="col-md-12">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <i class="fa fa-check "></i>
                                {{ $message }}</div>
@endif
                        @if(!empty($chatdata))
                        {!! Form::model($chatdata, ["route"=>['chat-management.update',$userid],'files'=>true]) !!}

                        @method('PUT')
                            <ul>
                            @if(!empty($chatdata))
                                @foreach ($chatdata as $chat)
                                    @if ($chat->sender_type == 'Customer')
                                    <li class="msg">
                                        <p>{{ $chat->message }}</p> <span>{{ $chat->created_at }}</span>
                                    </li>
                                    @else
                                    <li class="reply">
                                            <p> {{ $chat->message }}</p> <span> {{ $chat->created_at }}</span>
                                    </li>
                                    @endif
                               @endforeach
                            </ul>
                          @endif
                        @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                {!! Form::hidden('user_id',$userid , ['class'=>'form-control','id'=>'user_id']) !!}
                {!! Form::textarea('message',null, ['class'=>'form-control','required'=>'required', 'id'=>'message','rows'=>'2','placeholder'=>'Customer Query']) !!}



                                        @if ($errors->has('message'))
                       <span class="text-danger" role="alert">
                         <strong>{{ $errors->first('message') }}</strong>
                       </span>
                    @endif

                </div>
                <div class="row">
                <div class="col-md-1">
                <button type="submit" class="btn btn-accent">Submit</button>
                </div>
                <div class="col-md-1">
                <button onclick="window.location.href='{{route('customer-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                </div>
                </div>
                
                
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop
@section('pagescript')

@stop

