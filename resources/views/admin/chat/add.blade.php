@extends('layouts.adminapp')
@section('titletag')
Chat Management List : Add
@stop
@section('content')

          <!-- / .main-navbar -->
        
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
         
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Chat Management : Add</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              
              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                          {!! Form::open(['route'=>'chat-management.store','files'=>true]) !!}
						<div class="form-row">
                           
						   
						   @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif
                       
                            </div>
                            <div class="form-row">
	                                 <div class="form-group col-md-12">
                                <label for="#DDDDDD"> {!! Form::label('message','Customer Query *', []) !!}</label>
										
										{!! Form::textarea('message',null, ['class'=>'form-control','required'=>'required', 'id'=>'message','rows'=>'2','placeholder'=>'Customer Query']) !!}
										
										
										
										 @if ($errors->has('message'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('message') }}</strong>
                        </span>
                     @endif
                                 </div>
                              
                            </div>
                            <div class="form-group">
                              <label for="feInputAddress">Admin Reply</label>
  
							  {!! Form::textarea('description',null, ['class'=>'form-control','required'=>'required',  'id'=>'description','rows'=>'2','placeholder'=>'Admin Reply']) !!}
                             
							  </div>
                            
                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('chat-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
                
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')

@stop
