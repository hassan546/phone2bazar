@extends('layouts.adminapp')
@section('titletag')
Subscription Management : Edit
@stop
@section('content')

          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Subscription Management : Edit</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">


              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         {!! Form::model($banner, ["route"=>['subscriiption-management.update',$banner->id],'files'=>true]) !!}

@method('PUT')
						<div class="form-row">


						   @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif



                            </div>
                            <div class="form-row">
	                                 <div class="form-group col-md-3">
                                <label for="#DDDDDD"> {!! Form::label('name','Subscription Title *', []) !!}</label>
			{!! Form::text('name',null, ['class'=>'form-control','id'=>'name','maxlength'=>'30','required']) !!}

 @if ($errors->has('name'))
	 <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('name') }}</strong>
                        </span>
                     @endif


                                 </div>


								  <div class="form-group col-md-3">
                                 {!! Form::label('price','Subscription Price *', []) !!}

                   {!! Form::text('price',null, ['class'=>'form-control','id'=>'price','maxlength'=>'8','required']) !!}

                      @if ($errors->has('price'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('price') }}</strong>
                        </span>
                     @endif



                                 </div>

								  <div class="form-group col-md-3">
                                 {!! Form::label('type','Plan Type *', []) !!}

                    {!! Form::select('type',$ratings,null, ['class'=>'form-control','required']) !!}
                      @if ($errors->has('type'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('type') }}</strong>
                        </span>
                     @endif



                                 </div>

                              <div class="form-group col-md-3">
                                <label for="fePassword"> {!! Form::label('image','Image * (Image size <2MB )', []) !!}</label>
								@if($banner->image)
									{!! Form::file('image', ['class'=>'form-control','id'=>'image']) !!}
								@else
								 {!! Form::file('image', ['class'=>'form-control','id'=>'image','required']) !!}
							 @endif
								 <span><a href="{{asset('public/storage/upload/subscription/'.$banner->image.'')}}" target="_blank">{{$banner->image}}</a></span>
								  <br>@if ($errors->has('image'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('image') }}</strong>
                        </span>
                     @endif
                                 </div>
                            </div>


							<div class="form-row ">
								    <div class="form-group col-md-3">
                                <label for="#DDDDDD"> {!! Form::label('no_of_advertisement','No Of Advertisement *', []) !!}</label>
			{!! Form::text('no_of_advertisement',null, ['class'=>'form-control','id'=>'no_of_advertisement','maxlength'=>'4','required']) !!}

 @if ($errors->has('no_of_advertisement'))
	 <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('no_of_advertisement') }}</strong>
                        </span>
                     @endif


                                 </div>
    <div class="form-group col-md-3">
                                <label for="#DDDDDD"> {!! Form::label('duration','Duration (In Month) *', []) !!}</label>
			{!! Form::text('duration',null, ['class'=>'form-control','id'=>'duration','maxlength'=>'3','required']) !!}

 @if ($errors->has('duration'))
	 <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('duration') }}</strong>
                        </span>
                     @endif


                                 </div>


							 <div class="form-group col-md-6">
                              <label for="feInputAddress">Description</label>

							  {!! Form::textarea('description',null, ['class'=>'form-control', 'id'=>'description','rows'=>'2','placeholder'=>'Description']) !!}
							  </div>



							 </div>




                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('subscriiption-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              </div>
            </div>
            <!-- End Default Light Table -->
          </div>
          @stop
          @section('pagescript')
          <script>
    $(function () {
        $('#name').on('keypress', function (e) {
            if((e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || e.which == 32)  {

            }else{
                e.preventDefault();
            }
        });
        $('#duration').on('keypress', function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                e.preventDefault();
            }
        });
        $('#no_of_advertisement').on('keypress', function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                e.preventDefault();
            }
        });
        $('#price').on('keypress', function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                e.preventDefault();
            }
        });
  });
</script>
@stop
