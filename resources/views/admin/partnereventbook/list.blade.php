@extends('layouts.adminapp')
@section('titletag')
Partner Event Book Management List
@stop
@section('pagecss')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
@stop
@section('content')
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Partner Event Book Management</h3>


              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">

              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">

                  {!! Form::open(['route'=>'adminpartner-eventbookmangement.index']) !!}

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <div class="styled-select">
                                {!! Form::select('partner_id',$partnerlist,null, ['class'=>'form-control select2','placeholder'=>'Select Partner','required']) !!}
                                @if ($errors->has('partner_id'))
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('partner_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <button type="submit" class="btn btn-accent">Submit</button>
                        </div>
                    </div>
                    {!! Form::close() !!}


                     <table class="table mb-0 table" id="example">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">Serial No</th>
                          <th scope="col" class="border-0">Partner Name</th>
						   <th scope="col" class="border-0">Booking ID</th>
                          <th scope="col" class="border-0">Booked for</th>
						  <th scope="col" class="border-0">Customer Name</th>
						  <th scope="col" class="border-0">Booked On</th>
						   <th scope="col" class="border-0">Action</th>

                        </tr>
                      </thead>
					@php
					$count=0;
					@endphp
                      <tbody>
					   @foreach ($banners as $banner)
                        <tr>
                            <td>{{ ++$count }}</td>
                            <td>{{ $banner->getPartner->company_name }}</td>
						    <td>{{$banner->id.$banner->created_at->format('Ymd')}}</td>
							<td>{{$banner->booking_date }}</td>
							<td>{{$banner->client_name}}</td>
							<td>{{$banner->created_at}}</td>
							<td><a href="javascript:void(0);" onclick="delete_field('delete-form{{ $banner->id }}')">Delete</a>
                                <form action="{{route('adminpartner-eventbookmangement.destroy',[$banner->id])}}" method="POST" style="display: none;" id="delete-form{{ $banner->id }}">
                                @method('DELETE')
                                @csrf
                                </form>
						    </td>
                        </tr>

						 @endforeach


                      </tbody>
                    </table>
                  </div>



                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script>

	function delete_field(argument)
  {
    if(confirm("Are You Sure to Delete"))
    {
      $("#"+argument).submit();
    }
    else
    {
      return false;
    }
  }

	      function changeStatus(id,status){
        var url = "{{ url('')}}";
          $.ajax({
            type: "POST",
            url: '{{route("manage-eventstatus")}}',
            data:({
              id : id, _token: "{{csrf_token()}}",status: status
            }),
            cache: false,
            success: function(data){
             if(data==1){
                alert("Success! Status changed successfully");
                window.location.reload();
              }else{
                alert("Opps! Unable to change status.");
             }
            }
          });
      }


      $(document).ready(function() {
    $('#example').DataTable();
} );
    </script>
@stop
