@extends('layouts.adminapp')
@section('titletag')
Partner Advertisement Management List : Add
@stop
@section('content')

          <!-- / .main-navbar -->
        
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
         
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Partner Advertisement Management : Add</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              
              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                          {!! Form::open(['route'=>'adminpartner-advertisement.store','files'=>true]) !!}
						<div class="form-row">
                           
						   
						   @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif
                       
                            </div>
                            <div class="form-row">
	                                 <div class="form-group col-md-3">
                                <label for="#DDDDDD"> {!! Form::label('name','Advertisement Title *', []) !!}</label>
										{!! Form::text('name',null, ['class'=>'form-control','id'=>'name','placeholder'=>'Advertisement Title','required']) !!}
										
										 @if ($errors->has('name'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('name') }}</strong>
                        </span>
                     @endif
					 
								
                                 </div>
								 
								  <div class="form-group col-md-3">
                                 {!! Form::label('partner_id','Partner Name *', []) !!}

						{!! Form::select('partner_id',$partnerdetails,null, ['class'=>'form-control','required']) !!}
										
                      @if ($errors->has('partner_id'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('partner_id') }}</strong>
                        </span>
                     @endif
					 
					 
								
                                 </div>
								 
								 <div class="form-group col-md-3">
                                 {!! Form::label('position','Position *', []) !!}

                    {!! Form::select('position',$positiontype,null, ['class'=>'form-control','required']) !!}
                      @if ($errors->has('position'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('position') }}</strong>
                        </span>
                     @endif
					 
					 
								
                                 </div>
								 
								 <div class="form-group col-md-2">
                               <label for="#DDDDDD">  {!! Form::label('todate','Add End Date', []) !!}</label>
									  	
										{!! Form::text('todate',null, ['class'=>'form-control','id'=>'check_out_date','placeholder'=>'Add End Date','required']) !!}
										
										 @if ($errors->has('todate'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('todate') }}</strong>
                        </span>
                     @endif
                                 </div>
								 
								 
                              <div class="form-group col-md-3">
                                <label for="fePassword"> {!! Form::label('image','Image * (Image size <2MB )', []) !!}</label>
								 {!! Form::file('image', ['class'=>'form-control','id'=>'image','required']) !!}
								  @if ($errors->has('image'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('image') }}</strong>
                        </span>
                     @endif
                                 </div>
                            </div>
							
							
							
							<div class="form-row ">
							
							
			
							
							
							 <div class="form-group col-md-12">
                              <label for="feInputAddress">Description</label>
  
							  {!! Form::textarea('description',null, ['class'=>'form-control', 'maxlength'=>'50','id'=>'description','rows'=>'2','placeholder'=>'Description']) !!}
							  </div>

							 </div>
                           
                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('adminpartner-advertisement.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
                
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<script>

$(function() {
	
	 $( "#check_in_date" ).datepicker({ dateFormat: 'dd-mm-yy' }).val();
  $( "#check_out_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
              // $("#datepicker").datepicker({ dateFormat: "yy-mm-dd" }).val()
       });
</script>
@stop