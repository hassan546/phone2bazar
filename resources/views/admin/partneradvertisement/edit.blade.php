@extends('layouts.adminapp')
@section('titletag')
Partner Advertisement Management : Edit
@stop
@section('content')

          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Partner Advertisement Management : Edit</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">


              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         {!! Form::model($banner, ["route"=>['adminpartner-advertisement.update',$banner->id],'files'=>true]) !!}

@method('PUT')
						<div class="form-row">




                            </div>
                            <div class="form-row">
								  <div class="form-group col-md-3">
                                 {!! Form::label('partner_id','Partner Name *', []) !!}

						{!! Form::select('partner_id',$partnerlist,null, ['class'=>'form-control','required']) !!}

                      @if ($errors->has('partner_id'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('partner_id') }}</strong>
                        </span>
                     @endif



                                 </div>

								  <div class="form-group col-md-3">
                                 {!! Form::label('position','Position *', []) !!}

                    {!! Form::select('position',$positiontype,null, ['class'=>'form-control','required']) !!}
                      @if ($errors->has('position'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('position') }}</strong>
                        </span>
                     @endif



                                 </div>




                              <div class="form-group col-md-3">
                                <label for="fePassword"> {!! Form::label('image','Image * (Image size <2MB )', []) !!}</label>
								@if($banner->image)
									{!! Form::file('image', ['class'=>'form-control','id'=>'image']) !!}
								@else
								 {!! Form::file('image', ['class'=>'form-control','id'=>'image','required']) !!}
							 @endif
								 <span><a href="{{asset('public/storage/upload/partneradvertisement/'.$banner->image.'')}}" target="_blank">{{$banner->image}}</a></span>
								  <br>@if ($errors->has('image'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('image') }}</strong>
                        </span>
                     @endif
                                 </div>
                            </div>

                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('adminpartner-advertisement.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<script>

$(function() {

		 $( "#fromdate" ).datepicker({ dateFormat: 'dd-mm-yy',minDate: 0 });
  $( "#todate" ).datepicker({ dateFormat: 'dd-mm-yy',minDate: 0});
              // $("#datepicker").datepicker({ dateFormat: "yy-mm-dd" }).val()
       });

</script>
@stop
