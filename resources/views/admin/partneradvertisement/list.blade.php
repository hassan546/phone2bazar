@extends('layouts.adminapp')
@section('titletag')
Partner Advertisement Management List
@stop
@section('pagecss')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
@stop
@section('content')
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Partner Advertisement Management</h3>


              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">

            @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif

              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">

				   <!-- <button onclick="window.location.href='{{route('adminpartner-advertisement.create')}}';" class="btn btn-sm btn-accent ml-auto">
                          <i class="material-icons">file_copy</i> Add</button> -->

                          {!! Form::open(['route'=>'adminpartner-advertisement.index']) !!}

<div class="form-row">
    <div class="form-group col-md-4">
        <div class="styled-select">
            {!! Form::select('partner_id',$partnerlist,null, ['class'=>'form-control select2','placeholder'=>'Select Partner','required']) !!}
            @if ($errors->has('partner_id'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('partner_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-4">
        <button type="submit" class="btn btn-accent">Submit</button>
    </div>
</div>
{!! Form::close() !!}
                     <table class="table mb-0 table" id="example">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">Serial No</th>
						  <th scope="col" class="border-0">Position</th>
                          <th scope="col" class="border-0">Partner Name</th>
                          <th scope="col" class="border-0">Image</th>
                          <th scope="col" class="border-0">Status</th>
						  <th scope="col" class="border-0">Action</th>

                        </tr>
                      </thead>
					@php
					$count=0;
					@endphp
                      <tbody>

					   @foreach ($banners as $banner)
                       <tr>
                          <td>{{ ++$count }}</td>
                          <td>{{$banner->position}}</td>
                          <td>{{$banner->getPartner->company_name}}</td>
                          <td><a href="{{asset('public/storage/upload/partneradvertisement/'.$banner->image.'')}}" target="_blank"><img src="{{asset('public/storage/upload/partneradvertisement/'.$banner->image.'')}}" width="60" height="40"/></a></td>

						  <td> <a style="cursor: pointer;" onclick="changeStatus({{ $banner->id }},'{{ $banner->admin_status==0 ? "0" : "1"}}')">{{ $banner->admin_status==1 ? "Active" : "Inactive"}} </a> </td>


                         <td><a href="{{ route('adminpartner-advertisement.edit',$banner->id) }}">Edit</a> | <a href="javascript:void(0);" onclick="delete_field('delete-form{{ $banner->id }}')">Delete</a>
						 <form action="{{route('adminpartner-advertisement.destroy',[$banner->id])}}" method="POST" style="display: none;" id="delete-form{{ $banner->id }}">
				@method('DELETE')
				@csrf
				</form>

						 </td>
                        </tr>









						 @endforeach


                      </tbody>
                    </table>
                  </div>



                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script>

	function delete_field(argument)
  {
    if(confirm("Are You Sure to Delete"))
    {
      $("#"+argument).submit();
    }
    else
    {
      return false;
    }
  }

	      function changeStatus(id,status){
        var url = "{{ url('')}}";
          $.ajax({
            type: "POST",
            url: '{{route("manage-advertisementstatus")}}',
            data:({
              id : id, _token: "{{csrf_token()}}",status: status
            }),
            cache: false,
            success: function(data){
             if(data==1){
                alert("Success! Status changed successfully");
                window.location.reload();
              }else{
                alert("Opps! Unable to change status.");
             }
            }
          });
      }


      $(document).ready(function() {
    $('#example').DataTable();
} );
    </script>
@stop
