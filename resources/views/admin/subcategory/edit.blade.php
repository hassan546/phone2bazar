@extends('layouts.adminapp')
@section('titletag')
Sub Category Management : Edit
@stop
@section('content')

          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Sub Category Management : Edit</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                         {!! Form::model($categorylist, ["route"=>['subcategory-management.update',$categorylist->id],'files'=>true]) !!}

@method('PUT')
						<div class="form-row">
						   @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif
                            </div>
                            <div class="form-row">
	                                 <div class="form-group col-md-3">
                                <label for="#DDDDDD"> {!! Form::label('category','Sub Category *', []) !!}</label>
										{!! Form::text('category',null, ['class'=>'form-control','id'=>'category','placeholder'=>'Sub Category','required','maxlength'=>'30']) !!}

										 @if ($errors->has('category'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('category') }}</strong>
                        </span>
                     @endif
                                 </div>



								   <div class="form-group col-md-3">
                                <label for="#DDDDDD"> {!! Form::label('main_category','Main Category *', []) !!}</label>


									  {!! Form::select('main_category',$category,null, ['class'=>'form-control','placeholder'=>'Select Main Category','required']) !!}


										 @if ($errors->has('main_category'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('main_category') }}</strong>
                        </span>
                     @endif
                                 </div>
                                 <div class="form-group col-md-3">
                                <label for="#DDDDDD"> {!! Form::label('display_order','Order *', []) !!}</label>
										{!! Form::text('display_order',null, ['class'=>'form-control','maxlength'=>'3','id'=>'display_order','placeholder'=>'Order','required']) !!}

										 @if ($errors->has('display_order'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('display_order') }}</strong>
                        </span>
                     @endif
                                 </div>
                              <div class="form-group col-md-3">
                                <label for="fePassword"> {!! Form::label('image','Sub Category Image', []) !!}</label>
								 {!! Form::file('image', ['class'=>'form-control','id'=>'image']) !!}

								 <span><a href="{{asset('public/storage/upload/subcategory/'.$categorylist->image.'')}}" target="_blank"> <img width="50" height="50" src="{{asset('public/storage/upload/subcategory/'.$categorylist->image.'')}}"></a></span>
								  <br>
								  @if ($errors->has('image'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('image') }}</strong>
                        </span>
                     @endif
                                 </div>
                                 <div class="form-group col-md-3">
                                <label for="fePassword"> {!! Form::label('icon','Sub Category Icon Image', []) !!}</label>
								 {!! Form::file('icon', ['class'=>'form-control','id'=>'icon']) !!}

								 <span><a href="{{asset('public/storage/upload/subcategory/'.$categorylist->icon.'')}}" target="_blank">

								 <img width="50" height="50" src="{{asset('public/storage/upload/subcategory/'.$categorylist->icon.'')}}">
								</a></span>
								  <br>

								  @if ($errors->has('icon'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('icon') }}</strong>
                        </span>
                     @endif
                                 </div>
                            </div>
                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('subcategory-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<!--<script>
 $(function () {
      $('#category').on('keypress', function (e) {

        if((e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || e.which == 32)  {

        }else{
            e.preventDefault();
        }


      });
  });
</script> -->
@stop
