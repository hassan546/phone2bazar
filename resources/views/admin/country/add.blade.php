@extends('layouts.adminapp')
@section('titletag')
Country Management List : Add
@stop
@section('content')

          <!-- / .main-navbar -->
        
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
         
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Country Management Management : Add</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              
              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                          {!! Form::open(['route'=>'country-management.store','files'=>true]) !!}
						<div class="form-row">
                           
						   
						   @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif
                       
                            </div>
                            <div class="form-row">
	                                 <div class="form-group col-md-6">
                                <label for="#DDDDDD"> {!! Form::label('name','Country Name *', []) !!}</label>
										{!! Form::text('name',null, ['class'=>'form-control','id'=>'name','placeholder'=>'Country Name','required']) !!}
										
										 @if ($errors->has('name'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('name') }}</strong>
                        </span>
                     @endif
					 
								
                                 </div>
                              <div class="form-group col-md-6">
                                <label for="fePassword"> {!! Form::label('image','Country Image * (Image size -  <2MB )', []) !!}</label>
								 {!! Form::file('image', ['class'=>'form-control','id'=>'image','required']) !!}
								  @if ($errors->has('image'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('image') }}</strong>
                        </span>
                     @endif
                                 </div>
                            </div>
							
							
							
							
                            
                         
                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('country-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
                
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
