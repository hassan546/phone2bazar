<script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

    <script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>

    <script src="{{ asset('public/assets/admin/scripts/extras.1.0.0.min.js')}}"></script>

    <script src="{{ asset('public/assets/admin/scripts/sweetalert.min.js')}}"></script>

    <script src="{{ asset('public/assets/admin/scripts/shards-dashboards.1.0.0.min.js')}}"></script>

    <script src="{{ asset('public/assets/admin/scripts/app/app-blog-overview.1.0.0.js')}}"></script>

    <script src="https://use.fontawesome.com/b4b0414965.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">
     $('.select2').select2();
</script>
    <script>

      $(document).ready(function() {

  $(".nav a").click(function() {

    var link = $(this);

    var closest_ul = link.closest("ul");

    var parallel_active_links = closest_ul.find(".active");

    var closest_li = link.closest("li");

    var link_status = closest_li.hasClass("active");

    var count = 0;



    closest_ul.find("ul").slideUp("fast", function() {

      if (++count == closest_ul.find("ul").length)

        parallel_active_links.removeClass("active");

    });



    if (!link_status) {

      closest_li.children("ul").slideDown("fast");

      closest_li.addClass("active");

    }

  });


    setInterval(function () {
        var url = "{{ url('')}}";
          $.ajax({
            type: "POST",
            url: '{{route("admin.getChatCount")}}',
            data:({
               _token: "{{csrf_token()}}"
            }),
            cache: false,
            success: function(data){

                    $("#badge_count").html(data);
            }
        });
    },3000);

});



    </script>

