 <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">

	<div class="nav-wrapper nav">
		<ul class=" flex-column nav__list" style="padding: 0; float: left; width: 100%;">
			<li class="nav-item">
				<a class="nav-link active" href="{{route('admin.home')}}">
					<i class="material-icons">home</i>
					<span>Dashboard</span>
				</a>
			</li>
            <li class="nav-item">
				<a href="#" class="nav-link"><i class="material-icons">view_module </i>
					<span>Category Management <em class="fa fa-angle-right"></em></span> </a>
				<ul class="group-list">
					<li><a href="{{route('category-management.index')}}">Category List</a></li>
					<li><a href="{{route('subcategory-management.index')}}">Sub Category</a></li>

				</ul>
			</li>

            <li class="nav-item">
				<a href="{{route('subscriiption-management.index')}}" class="nav-link"><i class="material-icons">power_settings_new </i>
					<span>Subscription Management</span>
				</a>
			</li>
            <li class="nav-item">
				<a href="#" class="nav-link"><i class="material-icons">view_module </i>
					<span>State Management <em class="fa fa-angle-right"></em></span> </a>
				<ul class="group-list">
					<li><a href="{{route('state-management.index')}}">State List</a></li>
					<li><a href="{{route('city-management.index')}}">City List</a></li>

				</ul>
			</li>

            	<li class="nav-item">
				<a href="#" class="nav-link"><i class="material-icons">view_module </i>
					<span>Partner Management <em class="fa fa-angle-right"></em></span> </a>
				<ul class="group-list">
					<li><a href="{{route('partner-management.index')}}">Partner List</a></li>
					<li><a href="{{route('wallet-transaction.index')}}">Partner Wallet Transaction</a></li>
					<li><a href="{{route('purchased-subscription.index')}}">Partner Subscription</a></li>
					<li><a href="{{route('adminpartner-advertisement.index')}}">Partner Advertisement</a></li>
                    <li><a href="{{route('offer-management.index')}}">Partner Offers</a></li>
                    <li><a href="{{route('adminpartner-eventbookmangement.index')}}">Partner Booked Availability</a></li>
					<!--<li><a href="{{route('hotelier-management-registration')}}">Register as Hotelier</a></li>
					<!{1}**<li><a href="{{route('room-details.index')}}">Hotel Price List</a></li>**{1}> -->
				</ul>
			</li>
            <li class="nav-item">
				<a href="#" class="nav-link"><i class="material-icons">view_module </i>
					<span>Customer Management <em class="fa fa-angle-right"></em></span> </a>
				<ul class="group-list">
					<li><a href="{{route('customer-management.index')}}">Customer List</a></li>
				</ul>
			</li>
            <li class="nav-item">
				<a href="{{route('admin.refered_employee')}}" class="nav-link"><i class="material-icons">power_settings_new </i>
					<span>Reffered Employee</span>
				</a>
			</li>
            <li class="nav-item">
				<a href="#" class="nav-link"><i class="material-icons">note_add</i>
					<span>Page Management <em class="fa fa-angle-right"></em></span> </a>
				<ul class="group-list">
					<li><a href="{{route('page-management.index')}}">Page List</a></li>
				</ul>
			</li>
            <li class="nav-item">
				<a href="{{route('admin.logout')}}" class="nav-link"><i class="material-icons">power_settings_new </i>
					<span>Logout</span>
				</a>
			</li>

		</ul>
	</div>
</aside>
