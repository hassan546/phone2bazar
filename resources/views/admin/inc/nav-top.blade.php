		  <div class="main-navbar sticky-top bg-white">
            <!-- Main Navbar -->
            <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">

              <div style="cursor:pointer;" onclick="window.location.href='{{route('admin.home')}}';" class="input-group input-group-seamless ml-3">
                <img id="main-logo" class="d-inline-block align-top mr-1" src="{{ asset('public/assets/admin/img/logo.png')}}" alt="Highway">

                  </div>
                  <ul class="navbar-nav border-left flex-row ">
               <li class="nav-item border-right dropdown notifications">
                 <a class="nav-link nav-link-icon text-center" href="{{route('customer-management.index')}}" id="dropdownMenuLink">
                   <div class="nav-link-icon__wrapper">
                     <i class="material-icons">&#xE7F4;</i>
                     <span class="badge badge-pill badge-danger" id="badge_count"></span>
                   </div>
                 </a>

               </li>
              <li class="nav-item">
                          <a class="nav-link  text-nowrap px-3">

                          <span class="d-none d-md-inline-block">{{auth()->guard('admin')->user()->name}}</span>
                          </a>

                       </li>
             </ul>
              <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>

