@extends('layouts.adminapp')
@section('titletag')
Sub Category Management
@stop
@section('pagecss')

@stop
@section('content')

          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Wallet Transaction list</h3>


              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">

                <div class="col-lg-12">
                    <div class="card card-small mb-4">
                        <div class="card-header border-bottom">
                        {!! Form::open(['route'=>'wallet-transaction.index']) !!}

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <div class="styled-select">
                                        {!! Form::select('partner_id',$partnerlist,null, ['class'=>'form-control select2','placeholder'=>'Select Partner','required']) !!}
                                        @if ($errors->has('partner_id'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('partner_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="submit" class="btn btn-accent">Submit</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                     <table class="table mb-0 table" id="example">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">Serial No</th>
						  <th scope="col" class="border-0">Partner Name</th>
                          <th scope="col" class="border-0">Contact No</th>
						  <th scope="col" class="border-0">Transaction Amount (&#8377;)</th>
                          <th scope="col" class="border-0">Trans Type</th>
                          <th scope="col" class="border-0">Date</th>
                          <th scope="col" class="border-0">Remarks</th>

                        </tr>
                      </thead>
					@php
					$count=0;
					@endphp
                      <tbody>

					   @foreach ($transactionlist as $trans)
                       @php $color = ($trans->trans_type == 'cr') ? "green" : "red";	@endphp
                       <tr>
                          <td>{{ ++$count }}</td>
						   <td>{{$trans->getPartner->company_name ?? ''}}</td>
                         <td>{{$trans->getPartner->mobile_number}}</td>

						  <td>{{ floor($trans->amount) }}  </td>


						  <td  style="color:{{ $color }}">{{ $trans->trans_type }}</td>


                         <td>{{ $trans->created_at }}</td>
                         <td>{{ $trans->remark }}</td>
                        </tr>
						 @endforeach
                      </tbody>
                    </table>
                  </div>



                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script>

	function delete_field(argument)
  {
    if(confirm("Are You Sure to Delete"))
    {
      $("#"+argument).submit();
    }
    else
    {
      return false;
    }
  }

	      function changeStatus(id,status){
        var url = "{{ url('')}}";
          $.ajax({
            type: "POST",
            url: '{{route("change_statussubcategory")}}',
            data:({
              id : id, _token: "{{csrf_token()}}",status: status
            }),
            cache: false,
            success: function(data){
             if(data==1){
                alert("Success! Status changed successfully");
                window.location.reload();
              }else{
                alert("Opps! Unable to change status.");
             }
            }
          });
      }

	  function changeStatus2(id,status){
        var url = "{{ url('')}}";
          $.ajax({
            type: "POST",
            url: '{{route("change_statussubcategory2")}}',
            data:({
              id : id, _token: "{{csrf_token()}}",status: status
            }),
            cache: false,
            success: function(data){
             if(data==1){
                alert("Success! Show in listing status changed successfully");
                window.location.reload();
              }else{
                alert("Opps! Unable to change status.");
             }
            }
          });
      }



      $(document).ready(function() {
    $('#example').DataTable();
} );
    </script>

@stop
