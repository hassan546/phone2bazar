@extends('layouts.adminapp')
@section('titletag')
Customer Management
@stop
@section('pagecss')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
@stop
@section('content')
          <!-- / .main-navbar -->

            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">

            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Customer List</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <table class="table mb-0 table" id="example">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">Serial No</th>
                          <th scope="col" class="border-0">Customer Name</th>
						  <th scope="col" class="border-0">Email</th>
						  <th scope="col" class="border-0">Mobile Number</th>
                          <th scope="col" class="border-0">Registered On</th>
                          <th scope="col" class="border-0">Status</th>
						  <th scope="col" class="border-0">Action</th>

                        </tr>
                      </thead>
					@php
					$count=0;
					@endphp
                      <tbody>

					   @foreach ($customers as $customer)
              @php
              $chatcount = $customer->getChat->where('is_read','=', 0)->where('sender_type','=', 'Customer')->count();
              $color = ($chatcount > 0) ? "color:red" : "";
				 	    @endphp

                       <tr>
                          <td>{{ ++$count }}</td>

                          <td>{{$customer->name}}</td>
						  <td>{{$customer->email}}</td>
						   <td>{{$customer->mobile_number}}</td>
                           <td>{{$customer->created_at}}</td>

						  <td> <a style="cursor: pointer;" onclick="changeStatus({{ $customer->id }},'{{ $customer->status==0 ? "0" : "1"}}')">{{ $customer->status==1 ? "Active" : "Inactive"}} </a> </td>


                         <td><a href="{{ route('chat-management.edit',$customer->id) }}" style="{{ $color }}">Chat</a> | <!-- <a href="{{ route('addamounttocustoemr',$customer->id) }}">Add Amount</a> |--> <a href="javascript:void(0);" onclick="delete_field('delete-form{{ $customer->id }}')">Delete</a>
						 <form action="{{route('customer-management.destroy',[$customer->id])}}" method="POST" style="display: none;" id="delete-form{{ $customer->id }}">
				@method('DELETE')
				@csrf
				</form>

						 </td>
                        </tr>









						 @endforeach


                      </tbody>
                    </table>
                  </div>



                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
@section('pagescript')
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script>
 $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search" style="width:70%;" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    var table = $('#example').DataTable( {
        orderCellsTop: true,
        fixedHeader: false
    } );
	function delete_field(argument)
  {
    if(confirm("Are You Sure to Delete"))
    {
      $("#"+argument).submit();
    }
    else
    {
      return false;
    }
  }

	      function changeStatus(id,status){
        var url = "{{ url('')}}";
          $.ajax({
            type: "POST",
            url: '{{route("change_statuscustomer")}}',
            data:({
              id : id, _token: "{{csrf_token()}}",status: status
            }),
            cache: false,
            success: function(data){
             if(data==1){
                alert("Success! Status changed successfully");
                window.location.reload();
              }else{
                alert("Opps! Unable to change status.");
             }
            }
          });
      }


      $(document).ready(function() {
    $('#example').DataTable();
} );
    </script>
@stop
