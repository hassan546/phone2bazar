@extends('layouts.adminapp')
@section('titletag')
Customer Management : View
@stop
@section('content')
<!-- / .main-navbar -->
<span class="clearfix"></span>
<div class="main-content-container container-fluid px-4">
	<span class="clearfix"></span>
	<!-- Page Header -->
	<div class="page-header row no-gutters py-4">
		<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
			<!-- <span class="text-uppercase page-subtitle">Overview</span> -->
			<h3 class="page-title">Customer : View</h3>
		</div>
	</div>
	<!-- End Page Header -->
	<!-- Default Light Table -->
	

	<div class="row">
		<div class="col-lg-12">
			<div class="card card-small mb-4">
				<div class="card-header border-bottom">
					<table class="table mb-0">
					<tr>
					<td>
					<table  class="table mb-0">
					<tr>
								<td>
									Customer Name
								</td>
								<td>
									{{$data->name}}
								</td>
							</tr>
							<tr>
								<td>
									Email
								</td>
								<td>
									{{$data->email}}
								</td>
							</tr>
							<tr>
								<td>
									Mobile Number
								</td>
								<td>
									{{$data->mobile_number}}
								</td>
							</tr>
							<tr>
								<td>
									Address
								</td>
								<td>
									{{$data->address}}
								</td>
							</tr>
							@if($data->state!=0)
							<tr>
								<td>
									State
								</td>
								<td>
									{{$data->statename->state}}
								</td>
							</tr>
							@else
								<tr>
								<td>
									State
								</td>
								<td>
									
								</td>
							</tr>
							@endif
							@if($data->city!=0)
							<tr>
								<td>
									City
								</td>
								<td>
									{{$data->cityname->city_name}}
								</td>
							</tr>
							@else
								<tr>
								<td>
									City
								</td>
								<td>
									
								</td>
							</tr>
							
							@endif
							<tr>
								<td>
									Marital Status
								</td>
								<td>
									{{$data->gender}}
								</td>
							</tr>
							<tr>
								<td>
									Nationality
								</td>
								<td>
									{{$data->nationality}}
								</td>
							</tr>
							
					
					
					</table>
					</td>
					<td>
					
					<table class="table mb-0" >
					<tr>
								<td>
									Total Bookings
								</td>
								<td>
									{{$data->getBookings->count()}}
								</td>
							</tr>
							<tr>
								<td>
									Total Cancellations
								</td>
								<td>
									{{$data->getBookings->where('booking_status','=', 2)->count()}}
								</td>
							</tr>
							<tr>
								<td>
									Total Check ins
								</td>
								<td>
									{{$data->getBookings->where('check_in_flag','=', 1)->count()}}
								</td>
							</tr>
				
					</table>
					
					</td>
					</tr>
						
							
						
					</table>
					<br>
						<a href="{{route('customer-management.index')}}" class="btn btn-success" style="color: #fff;">Back</a>
					</div>
				</div>
			</div>
		</div>
		<!-- End Default Light Table -->
	</div>

	@stop
	