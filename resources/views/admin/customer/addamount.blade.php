@extends('layouts.adminapp')
@section('titletag')
Add Amount to User Wallet : Add
@stop
@section('content')

          <!-- / .main-navbar -->
        
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
         
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Add Amount to User Wallet : Add</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              
              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                           {!! Form::model($data, ["route"=>['addamounttocustoemr',$data->id],'files'=>true]) !!}
						<div class="form-row">
                           
						   {{ Form::hidden('user_id', $data->id) }}
						   @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif


                            </div>
                            <div class="form-row">
	                                 <div class="form-group col-md-2">
                                <label for="#DDDDDD"> {!! Form::label('wallet_balance','Wallet Amount *', []) !!}</label>
										{!! Form::text('wallet_balance',null, ['class'=>'form-control','id'=>'wallet_balance','placeholder'=>'0.00','required']) !!}
										
										 @if ($errors->has('wallet_balance'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('wallet_balance') }}</strong>
                        </span>
                     @endif
	
                                 </div>
                              
                            </div>
                           
                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('customer-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
                
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
