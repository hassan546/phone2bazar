@extends('layouts.adminapp')
@section('titletag')
Promotional Banner Management List : Add
@stop
@section('content')

          <!-- / .main-navbar -->
        
            <span class="clearfix"></span>
          <div class="main-content-container container-fluid px-4">
         
            <span class="clearfix"></span>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-5 text-center text-sm-left mb-0">
                <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
                <h3 class="page-title">Promotional Banner Management : Add</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              
              <div class="col-lg-12">
                <div class="card card-small mb-4 pt-3">
               <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                          {!! Form::open(['route'=>'promotional-banner-management.store','files'=>true]) !!}
						<div class="form-row">
                           
						   
						   @if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check "></i>
          {{ $message }}</div>
@endif


                        
                            </div>
                            <div class="form-row">
	                                 <div class="form-group col-md-6">
                                <label for="#DDDDDD"> {!! Form::label('banner_title','Banner Title *', []) !!}</label>
										{!! Form::text('banner_title',null, ['class'=>'form-control','id'=>'banner_title','placeholder'=>'Banner Title','required']) !!}
										
										 @if ($errors->has('banner_title'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('banner_title') }}</strong>
                        </span>
                     @endif
					 
								
                                 </div>
                              <div class="form-group col-md-6">
                                <label for="fePassword"> {!! Form::label('banner_image','Banner Image *', []) !!}</label>
								 {!! Form::file('banner_image', ['class'=>'form-control','id'=>'banner_imaged','required']) !!}
								  @if ($errors->has('banner_image'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('banner_image') }}</strong>
                        </span>
                     @endif
                                 </div>
                            </div>
							
							 <div class="form-row">
	                                 <div class="form-group col-md-6">
                                <label for="#DDDDDD"> {!! Form::label('banner_title','Banner Type', []) !!}</label>
										 <select name="type" class="form-control" required="">
										<option value="Web">For Web Page</option>
										<option value="App">For Application</option>
									</select>
										
										
					 
								
                                 </div>
                              <div class="form-group col-md-6">
                                <label for="fePassword"> {!! Form::label('banner_image','Banner Position', []) !!}</label>
								 <select name="topbottom" class="form-control" required="">
           
            <option value="Top">For Top</option>
            <option value="Bottom">For Bottom</option>
          </select>
                                 </div>
                            </div>
							
							
                            <div class="form-group">
                              <label for="feInputAddress">Banner Description</label>
  
							  {!! Form::textarea('description',null, ['class'=>'form-control','id'=>'description','row'=>'2','placeholder'=>'Banner Description']) !!}
                             
							  </div>
                            <div class="form-group">
                                <label for="feInputCity">Product Link</label>
								{!! Form::text('link',null, ['class'=>'form-control','id'=>'link','placeholder'=>'Banner Link','required']) !!}
								 @if ($errors->has('link'))
                        <span class="text-danger" role="alert">
                          <strong>{{ $errors->first('link') }}</strong>
                        </span>
						              @endif
                            </div>
                            <button type="submit" class="btn btn-accent">Submit</button>
							<button onclick="window.location.href='{{route('promotional-banner-management.index')}}';" type="button" class="btn btn-accent">Cancel</button>
                           {!! Form::close() !!}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
                
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@stop
