@extends('layouts.app')
@section('titletag')
Reset Password
@stop
@section('pagecss')
@stop
@section('content')
         <div class="signup" >
            <div class="bg-signup" style="background-image: url({{ asset('public/assets/frontend/img/bg.png')}});">
            <h2>Welcome</h2>
            <p>Please Reset you Password</p>
            </div>
            <div class="panel">
			 <form method="POST" action="{{ route('password.email') }}">
                        @csrf
						
		
				
				
            <div class="form">
            <h4>{{ __('Reset Password') }}</h4>
			@if (Session::has('status'))
			<div class="form-group">

				   <strong>{{ Session::get('status') }}</strong>
                </div>
			 @endif	
				
            <div class="form-group">
			 <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback"  style="color:red;font-size:10px;" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
            </div>
                <div class="form-group">

				   <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                </div>
            </div>
			</form>
			
            </div>
            <span class="clearfix"></span>
         </div>
@endsection
