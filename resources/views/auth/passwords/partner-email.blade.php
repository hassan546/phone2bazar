@extends('layouts.app')
@section('titletag')
Password Reset Link
@stop
@section('pagecss')
@stop
@section('content')
	<!--Correct username: invitado / pass: hgm2015-->

          <div class="signup" >
            <div class="bg-signup" style="background-image: url({{ asset('public/assets/frontend/img/bg.png')}});">
            <h2>{{ __('Welcome') }}</h2>
            <p>{{ __('Enter your email address and we will send instructions on resetting your
                                    password.') }}</p>
            </div>
			
			 <form method="POST" action="{{ route('partner.password.email') }}">
                        @csrf
            <div class="panel">
            <div class="form">
            <h4>{{ __('Forgot Password') }} </h4>
			
			<div class="flash_messages">
   @if (Session::has('status'))
        <div data-alert class="alert-box success radius wow fadeInLeft">
            <i class=""></i><strong>{{ Session::get('status') }}</strong>
        </div>
    @endif
	</div>
            <div class="form-group">

			 <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-Mail Address') }}" value=""  type="email" name="email" <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
            </div>
          
                 <div class="form-group">
				 <button  style="width: 100%;border-radius: 2px;" type="submit" class="btn">
                                    {{ __('Send Password Reset Link') }}</button>
									
									
					</div>
					
					<div class="form-group">
				 
									
									<button onclick="window.location.href='{{route('partner.login.submit')}}'" style="width: 100%;border-radius: 2px;"  type="button" class="btn" style="width: 100%;border-radius: 2px;">
                                    {{ __('Login') }}
                                </button>
								
					</div>
					
            </div>
            </div>
			</form>
            <span class="clearfix"></span>
         </div>


@stop
@section('pagescript')
@stop