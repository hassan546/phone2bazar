	<!DOCTYPE html>
	<html>
	<head>
		<title>Login | Phone2bazaar</title>
		  <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
   <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.0.0" href="{{ asset('public/assets/admin/styles/shards-dashboards.1.0.0.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/admin/styles/extras.1.0.0.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/admin/css/styles.css')}}">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
		<style>
		/* Design based on Blue Login Field of Kevin Sleger https://codepen.io/MurmeltierS/pen/macKb */
body {
  background: #F5F6FA;
  background-size: cover;
  font-family: "Roboto";
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
body::before {
  z-index: -1;
  content: '';
  position: fixed;
  top: 0;
  left: 0;
  background: #F5F6FA;
  /* IE Fallback */
  /*background: rgba(68, 196, 231, 0.8);*/
  width: 100%;
  height: 100%;
}
.form {
  position: absolute;
  top: 44%;
  left: 50%;
  background: #fff;
  width: 340px;
  margin: -140px 0 0 -182px;
  padding: 40px;
  box-shadow: 0 2px 0 rgba(90,97,105,.11),0 4px 8px rgba(90,97,105,.12),0 10px 10px rgba(90,97,105,.06),0 7px 70px rgba(90,97,105,.1);
	border-radius: 7px;
}
.form h2 {
  margin: 0 0 20px;
  line-height: 1;
  color: #44c4e7;
  font-size: 18px;
  font-weight: 400;
}
.form input {
  outline: none;
  display: block;
  width: 100%;
  margin: 0 0 20px;
  padding: 10px 15px;
  border: 1px solid #ccc;
  color: #444;
  font-family: "Roboto";
  box-sizing: border-box;
  font-size: 14px;
  font-wieght: 400;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  transition: 0.2s linear;
}
.form inputinput:focus {
  color: #333;
  border: 1px solid #44c4e7;
}
.form button {
  cursor: pointer;
  /*background: #44c4e7;*/
  width: 100%;
  padding: 10px 15px;
  border: 0;
  color: #fff;
  /*font-family: "Roboto";*/
  font-size: 16px;
  font-weight: 400;
}
/*.form button:hover {
  background: #369cb8;
}*/
.error,
.valid {
  display: none;
}
.btn{
	color: #fff;

border-color: #C39E00;

background-color: #C39E00;

box-shadow: none;
}
.form-control:focus
		</style>
	</head>
	<body>
	<!--Correct username: invitado / pass: hgm2015-->

<section class="form animated flipInX">
  <!-- <h2>Highways</h2> -->
  <img src="{{ asset('public/assets/admin/img/logo.png')}}" style="width: 100%;margin-bottom: 15px;">
  <div class="flash_messages">
   @if (Session::has('status'))
        <div data-alert class="alert-box success radius wow fadeInLeft">
            <i class="fa fa-thumbs-up fa-lead"></i><strong>{{ Session::get('status') }}</strong>
        </div>
    @endif
	</div>
 <label class="label">{{ __('Enter your email address and we will send instructions on resetting your
                                    password.') }}</label>
 <form method="POST" action="{{ route('admin.password.email') }}">
                        @csrf

    <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-Mail Address') }}" value=""  type="email" name="email" <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror



<button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>

								<button onclick="window.location.href='{{route('admin.login')}}'" type="button" class="btn btn-primary" style="margin-top:10px;">
                                    {{ __('Login') }}
                                </button>
</form>
</section>
</body>
	</html>
