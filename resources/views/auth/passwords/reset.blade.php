@extends('layouts.app')
@section('titletag')
Reset Password
@stop
@section('pagecss')
@stop
@section('content')
         <div class="signup" >
            <div class="bg-signup" style="background-image: url({{ asset('public/assets/frontend/img/bg.png')}});">
            <h2>Welcome</h2>
            <p>Please Reset you Password</p>
            </div>
            <div class="panel">
			 <form method="POST" action="{{ route('password.update') }}">
                        @csrf
				 <input type="hidden" name="token" value="{{ $token }}">		
            <div class="form">
            <h4>{{ __('Reset Password') }}</h4>
            <div class="form-group">
			<input id="email" type="email" placeholder="E-Mail Address"  class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" style="color:red;font-size:10px;" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
								
            </div>
			
			<div class="form-group">
					 <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" style="color:red;font-size:10px;" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
								
            </div>
			
			<div class="form-group">
					 <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation" required autocomplete="new-password">
	
            </div>
                <div class="form-group">
				<button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                         </button>
                </div>
            </div>
			</form>
			
            </div>
            <span class="clearfix"></span>
         </div>
@endsection
