@extends('layouts.app')
@section('titletag')
Password Reset
@stop
@section('pagecss')
@stop
@section('content')


          <div class="signup" >
            <div class="bg-signup" style="background-image: url({{ asset('public/assets/frontend/img/bg.png')}});">
            <h2>{{ __('Welcome') }}</h2>
            <p>{{ __('Password Reset. Enter your new password.') }}</p>
            </div>
			
			 <form method="POST" action="{{ route('partner.password.update') }}">
  @csrf
<input type="hidden" name="token" value="{{ $token }}">


            <div class="panel">
            <div class="form">
            <h4>{{ __('Password Reset') }} </h4>
			

            <div class="form-group">

			  <input id="email" type="email" placeholder="{{ __('E-Mail Address') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
            </div>
			
			
			<div class="form-group">
			 <input id="password" type="password" placeholder="{{ __('Password') }}" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
		
            </div>
			
			<div class="form-group">
			<input id="password-confirm" type="password" placeholder="{{ __('Confirm Password') }}" class="form-control" name="password_confirmation" required autocomplete="new-password">
		
            </div>
			
			
			
          
                 <div class="form-group">
				 
				  <button type="submit" style="width: 100%;border-radius: 2px;"  class="btn">
                                    {{ __('Reset Password') }}
                                </button>
								
								
				
									
									
					</div>
					
					<div class="form-group">
									<button onclick="window.location.href='{{route('partner.login.submit')}}'" style="width: 100%;border-radius: 2px;"  type="button" class="btn" style="width: 100%;border-radius: 2px;">
                                    {{ __('Login') }}
                                </button>
								
					</div>
					
            </div>
            </div>
			</form>
            <span class="clearfix"></span>
         </div>


@stop
@section('pagescript')
@stop