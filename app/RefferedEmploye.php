<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefferedEmploye extends Model
{
    protected $fillable=['partner_id','mobile_number'];

    public function getPartner()
    {
    	return $this->belongsTo(Partner::class,'partner_id');
    }
}
