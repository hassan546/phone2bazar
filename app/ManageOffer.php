<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class ManageOffer extends Model
{

    protected $fillable=[];
    public $timestamps=true;
    protected $appends = array('imagepath');
	public function getImagePathAttribute()
    {
        return asset('public/storage/upload/partneroffer/');
    }
    public function getPartner()
    {
    	return $this->belongsTo(Partner::class,'partner_id');
    }







}
