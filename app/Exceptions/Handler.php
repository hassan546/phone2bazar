<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
		 $class = get_class($exception);
		
		switch($class) {
        case 'Illuminate\Auth\AuthenticationException':
            $guard = array_get($exception->guards(), 0);
             
            switch ($guard) {
                case 'admin':
                    $login = 'admin.login';
                    break;
				case 'partner':
                    $login = 'partner.login.submit';
                    break;
				case 'api':
                    return response()->json(['result'=>0,'message' => 'Unauthorised']);
                default:
                    $login = 'front.home';
                    break;
            }

            return redirect()->route($login);
        }
		
		
		if($exception instanceof \Illuminate\Http\Exceptions\PostTooLargeException)
		{			session_start();
					$_SESSION["imgealert"] = "Size of attached file should be less < ".ini_get("upload_max_filesize")."B";
			
                    return redirect()->back()->withErrors("Size of attached file should be less ".ini_get("upload_max_filesize")."B", 'addNote');
        }
			
			
		
	
        return parent::render($request, $exception);
    }
}
