<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletTransaction extends Model
{
    //
    //protected $fillable=[];
    public function getPartner()
    {
    	return $this->belongsTo(Partner::class,'partner_id');
    }
}
