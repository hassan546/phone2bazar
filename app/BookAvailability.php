<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookAvailability extends Model
{
    //
    public function getPartner()
    {
    	return $this->belongsTo(Partner::class,'partner_id');
    }
}
