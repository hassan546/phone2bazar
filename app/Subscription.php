<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable=[];
	
	protected $appends = array('imagepath');

    public function getImagePathAttribute()
    {
        return asset('public/storage/upload/subscription/');  
    }
}
