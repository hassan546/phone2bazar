<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile_number','provider','provider_id','invite_code'
    ];

	protected $appends = array('imagepath');
	public function getImagePathAttribute()
    {
        return asset('public/storage/upload/userimage/');
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

	//
	public function statename()
    {
		return $this->belongsTo(State::class,'state');

    }

	public function cityname()
    {
		return $this->belongsTo(City::class,'city');

    }

	public function getEventBooking()
    {
        return $this->hasMany(CustomerEventBook::class,"user_id","id")->orderBy('created_at','desc');
    }

	public function getBookings()
    {
        return $this->hasMany(BookHotel::class,"user_id","id");
    }

	public function getReviews()
    {
        return $this->hasMany(CustomerPartnerReview::class,"user_id","id");
    }
    public function getChat()
    {
        return $this->hasMany(Chat::class,"user_id","id");
    }



}
