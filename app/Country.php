<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	  protected $fillable=[];
	  protected $appends = array('imagepath');
	
	public function getImagePathAttribute()
    {
        return asset('public/storage/upload/country/');  
    }
	
	public function getStates()
    {
        return $this->hasMany(State::class,"country_id","id");
    }
	
	public function getCity()
    {
        return $this->hasMany(City::class,"country_id","id");
    }
}

