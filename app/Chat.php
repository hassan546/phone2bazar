<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable=[];


	public function customer()
    {
    	return $this->belongsTo(User::class,'user_id');
    }

}
