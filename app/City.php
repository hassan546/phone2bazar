<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Area;
class City extends Model
{
    public $primaryKey='city_id';
    public $timestamps=false;
	//protected $appends = array('imagepath');
	
	public function getImagePathAttribute()
    {
        return asset('public/storage/upload/city/');  
    }
	
    public function getArea()
    {
    	return $this->hasMany(Area::class,'city_id','city_id');
    }
    public function getStateNameAttribute($value)
    {
		$state = State::find($this->state_id);
		if($state)
			return $state->state;
		else
			return '';
		
       
    }
	
	public function getCountry()
    {
    	return $this->belongsTo(Country::class,'country_id');
    }
	
}
