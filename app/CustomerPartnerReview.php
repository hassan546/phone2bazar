<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class CustomerPartnerReview extends Model
{

    protected $fillable = [];
	protected $appends = array();
	
	public function getUser()
    {
    	return $this->belongsTo(User::class,'user_id')->withDefault();
    }
	public function getPartner()
    {
    	return $this->belongsTo(Partner::class,'partner_id')->withDefault();
    }
	

}
