<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class PartnerAdvertisement extends Model
{
    protected $fillable = [];
	
	protected $appends = array('imagepath');
	public function getImagePathAttribute()
    {
        return asset('public/storage/upload/partneradvertisement/');
    }

	public function getPartner()
    {
    	return $this->belongsTo(Partner::class,'partner_id');
    }
	
}
