<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class HelperComposer
{
    public $movieList = [];
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        /*$this->movieList = [
            'Shawshank redemption',
            'Forrest Gump',
            'The Matrix',
            'Pirates of the Carribean',
            'Back to the future',
        ];*/
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
		$category= array();
        $area = \App\Category::whereStatus(1)->orderBy('category', 'asc')->get()->toArray();

		foreach($area as $kye=>$value)
	      $category[$value['id']]= $value['category'];


		/*$service= array();
        $area = \App\ManageService::whereStatus(1)->orderBy('name', 'asc')->get()->toArray();

		foreach($area as $kye=>$value)
	      $service[$value['id']]= $value['name'];


		$country= array();
        $area = \App\Country::whereStatus(1)->orderBy('name', 'asc')->get()->toArray();
		foreach($area as $kye=>$value)
	      $country[$value['id']]= $value['name'];

		  $organizaton= array();
        $area = \App\Organizaton::whereStatus(1)->orderBy('organizaton', 'asc')->get()->toArray();
		foreach($area as $kye=>$value)
	      $organizaton[$value['id']]= $value['organizaton'];


		 //added by hassan
	  $subscriptionplan= array();
        $subsc= \App\Subscription::whereStatus(1)->orderBy('name', 'asc')->get()->toArray();
        foreach($subsc as $kye=>$value)
            $subscriptionplan[$value['id']]= $value['name'];*/



		$subcategory= array();
        $area = \App\SubCategory::whereStatus(1)->orderBy('category', 'asc')->get()->toArray();

		foreach($area as $kye=>$value)
	      $subcategory[$value['id']]= $value['category'];


		$partnerdetails= array();
        $area = \App\Partner::whereStatus(1)->orderBy('company_name', 'asc')->get()->toArray();

		foreach($area as $kye=>$value)
	      $partnerdetails[$value['id']]= $value['company_name'];

		//print_r($arealist);
		//exit;
        $view->with(array('category'=>$category,'subcategory'=>$subcategory,'partnerlist'=>$partnerdetails));
    }
}
