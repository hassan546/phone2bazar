<?php

namespace App\Http\Controllers\Auth;
use Auth;
use Password;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class PartnerForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:partner');
    }
	
	
	public function broker(){
        return Password::broker('partners');
    }
	
	
	 /**
     * Get the guard to be used during authentication
     * after password reset.
     * 
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    public function guard(){
        return Auth::guard('partner');
    }
	
	    /**
     * Show the reset email form.
     * 
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestFormAdmin(){

        return view('auth.passwords.partner-email');
    }
	
	
	
	
	
	/**
	 * Show the reset email form.
	 * 
	 * @return \Illuminate\Http\Response
	 */
	/* public function showLinkRequestForm(){
	   return view('auth.passwords.email',[
		  'title' => 'Password Reset',
		  'passwordEmailRoute' => 'password.email'
	   ]);
	} */



	
}
