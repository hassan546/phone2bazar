<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Password;

class AdminLoginController extends Controller
{
    //


    use AuthenticatesUsers;
    protected $guard = 'admin';

	    public function __construct()
    {
       //$this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
    }


	protected function guard(){
        return Auth::guard('admin');
    }

    //Password Broker for Seller Model
    public function broker()
    {
        return Password::broker('admins');
    }

	public function showLoginForm()
    {
        return view('auth.adminLogin');
    }

	public function login(Request $request)
    {


        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password]))
		{
			 return redirect()->route('admin.home');

        }

        return back()->withErrors(['email' => 'Email or password are wrong.']);
    }

	public function logout(Request $request)
    {
		//auth('admin')->logout();
        auth()->guard('admin')->logout();
        return redirect()->route('admin.login');
    }



}
