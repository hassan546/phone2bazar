<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Password;
use App\Partner;
use App\User;

class PartnerLoginController extends Controller
{

    use AuthenticatesUsers;
    protected $guard = 'partner';

	public function __construct()
    {
       //$this->middleware('guest')->except('logout');
        $this->middleware('guest:partner')->except('logout');
    }


	protected function guard(){
        return Auth::guard('partner');
    }

    //Password Broker for Seller Model
    public function broker()
    {
        return Password::broker('partners');
    }
    public function showSignupForm(){
        return view('auth.partner_signup');
    }

	public function showLoginForm()
    {
        return view('auth.partnerLogin');
    }
    public function signupSubmit(Request $request){
        $data=array();
        $partner = new Partner;
        $this->validate($request, [
            'cname' => 'required|max:50',
            'email' => 'required|email|unique:partners',
            'password' => 'required|min:6'
         ]);
        $partner->company_name = $request->cname;
        $partner->email = $request->email;
        $partner->password =  bcrypt($request->password);
        $partner->textpassword =  $request->password;
        $partner->save();
		
		$order="ORR00".rand(1000,999999);
		$user = User::create([
            'name' => '',
            'email' => $order.'@dfsdfsd.com',
			'mobile_number' => $order,
            'password' => ''
        ]);
		
		
		$recordcheck = User::find($partner->id);
		if(!$recordcheck)
			$bookings= User::whereId($user->id)->update(array('id'=>$partner->id));
		else
		{
			$bookings= Partner::whereId($partner->id)->update(array('id'=>$user->id));
		}
		
        if (Auth::guard('partner')->attempt(['email' => $request->email, 'password' => $request->password]))
        {
             return redirect()->route('partner.home')->with(array(
                'success' => "You have registered successfully"
             ));

        }

    }
	public function login(Request $request)
    {


        if (Auth::guard('partner')->attempt(['email' => $request->email, 'password' => $request->password,'status' => 1]))
		{
			if(auth()->guard('partner')->user()->profile_percentage!=100)
				return redirect()->route('partner.home');
			else
				return redirect()->route('partner.dashboard');

        }

        return back()->withErrors(['email' => 'Either Email/password is wrong or profile is iactivated']);
    }

	public function logout(Request $request)
    {
		auth('partner')->logout();
        //auth()->guard('admin')->logout();
        return redirect()->route('partner.login.submit');
    }



}
