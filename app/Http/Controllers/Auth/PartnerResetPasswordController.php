<?php

namespace App\Http\Controllers\Auth;
use Auth;
use Password;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PartnerResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/partner/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:partner');
    }
	
	
	/**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    protected function broker()
	{
        return Password::broker('partners');
    }
    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
	{
        return Auth::guard('partner');
    }
	
	    //Show form to seller where they can reset password
    public function showResetForm(Request $request, $token = null)
    {

        return view('auth.passwords.partner-reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
	
	
	/* public function showResetForm(Request $request, $token = null){
		return view('auth.passwords.reset',['token' => $token,]);
	} */
	
}


