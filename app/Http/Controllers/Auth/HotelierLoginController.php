<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Password;

class HotelierLoginController extends Controller
{
	
    use AuthenticatesUsers;
    protected $guard = 'hotelier';
	
	public function __construct()
    {
       //$this->middleware('guest')->except('logout');
        $this->middleware('guest:hotelier')->except('logout');
    }
	
	
	protected function guard(){
        return Auth::guard('hotelier');
    }

    //Password Broker for Seller Model
    public function broker()
    {
        return Password::broker('hoteliers');
    }
	
	public function showLoginForm()
    {
        return view('auth.hotelierLogin');
    }
	
	public function login(Request $request)
    {
		

        if (Auth::guard('hotelier')->attempt(['email' => $request->email, 'password' => $request->password])) 
		{
			 return redirect()->route('hotelier.home');
			
        }
		
        return back()->withErrors(['email' => 'Email or password are wrong.']);
    }
	
	public function logout(Request $request)
    {
		auth('hotelier')->logout();
        //auth()->guard('admin')->logout();
        return redirect()->route('front.home');
    }
	
	
	
}
