<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Chat;
use App\User;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {

        $customers = Chat::with('customer')->orderBy('created_at', 'desc')->get();
        //print_r($customers);exit;
        //return view('admin.purchasesubscription.list',compact('transactionlist'));
        //return view('admin.customer.list', compact('customers'));
        return view('admin.chat.list', compact('customers'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view("admin.chat.add");

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $banner = new Chat;
         $this->validate($request, [
           'message' => 'required',
            'description' => 'required',
         ]);

		 $banner->message = $request->message;
		 $banner->description = $request->description;
		 $banner->save();

		return redirect()->route('chat-management.create')->with(array(
            'success' => "Chat has been added"
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Notificaiton $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit($userid)
    {
        //echo $userid;exit;
        $chatdata=Chat::whereUserId($userid)->get();
        Chat::whereUserId($userid)->whereSenderType('Customer')->update(['is_read' => '1']);

		return view('admin.chat.edit',compact('chatdata'),['userid'=>$userid]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$userid)
    {
        $this->validate($request, [
            'message' => 'required'
          ]);
        $chatdata = new Chat;
        $chatdata->message = $request->message;
        $chatdata->user_id = $userid;
        $chatdata->sender_type = 'Admin';
		$chatdata->save();
        $usertoken = User::whereId($userid)->first();

		$fields = array (
            'to' => $usertoken->fcm_token,
            'notification' => array (
                'title' => 'Phone2bazaar',
                'body' => $request->message,
                'message' => $request->message,
                'page_id' =>'chat'
            ),
            'priority' => 'high'
        );

		getPushNotificaiton($fields);
      return back()->with('success','Message send successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chat $banner,$id)
    {
		$banner = Chat::find($id);


        $banner->delete();
		return back()->with('success','Chat Deleted');
    }

	public function timestamp_ms()
   {
    $time=round(microtime(true) * 1000);
    return $time;
  }

    public function ChangeStatus(){
    $id = $_POST['id'];
    $status = $_POST['status'];
    if($status == '1'){
      $result = Chat::where('id','=',$id)->update(['status' => '0']);
    }else{
      $result = Chat::where('id','=',$id)->update(['status' => '1']);
    }
    echo $result;exit;
  }

}
