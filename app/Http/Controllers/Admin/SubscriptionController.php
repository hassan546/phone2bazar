<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
		$banners=Subscription::orderBy('created_at', 'DESC')->get();
		return view('admin.subscription.list',compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view("admin.subscription.add");

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $banner = new Subscription;
         $this->validate($request, [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:2048',
            'no_of_advertisement' => 'required',
            'duration' => 'required',
            'type' => 'required',
			'price' => 'required',
         ]);

		if($request->hasfile('image'))
		 {
			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/subscription');
			  $file->move($destinationPath, $name);
		 }

		 $banner->name = $request->name;
		 $banner->type = $request->type;
		 $banner->price = $request->price;
         $banner->duration = $request->duration;
		 $banner->no_of_advertisement = $request->no_of_advertisement;
		 $banner->image = $name;
		 $banner->description = $request->description;
		 $banner->save();

		return redirect()->route('subscriiption-management.index')->with(array(
            'success' => "Subscription has been added"
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit($banner)
    {
        $banner=Subscription::find($banner);

		return view('admin.subscription.edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$banner)
    {

        $banner=Subscription::find($banner);

		if($request->hasfile('banner_image'))
		{
			$this->validate($request, [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:2048',
            'no_of_advertisement' => 'required',
            'duration' => 'required',
            'price' => 'required',
            'type' => 'type'
         ]);

		}
		else
		{
			$this->validate($request, [
            'name' => 'required',
			'price' => 'required',
            'no_of_advertisement' => 'required',
            'duration' => 'required',
            'type' => 'required'
         ]);
		}
		if($request->hasfile('image'))
		 {
			if($banner ->image && file_exists(public_path('storage/upload/subscription').'/'.$banner ->image))
				unlink(public_path('storage/upload/subscription').'/'.$banner ->image);

			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/subscription');
			  $file->move($destinationPath, $name);
			  $banner->image = $name;
		 }


		$banner->name = $request->name;
        $banner->price = $request->price;
        $banner->type = $request->type;
		$banner->description = $request->description;
        $banner->duration = $request->duration;
        $banner->no_of_advertisement = $request->no_of_advertisement;
		$banner->save();
      //  return back()->with('success','Subscription Updated');
        return redirect()->route('subscriiption-management.index')->with(array(
            'success' => "Subscription has been updated"
        ));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $banner,$id)
    {
		$banner = Subscription::find($id);

		if($banner ->image)
			unlink(public_path('storage/upload/subscription').'/'.$banner ->image);

        $banner->delete();
		return back()->with('success','Subscription Deleted');
    }

	public function timestamp_ms()
   {
    $time=round(microtime(true) * 1000);
    return $time;
  }

    public function ChangeStatus(){
    $id = $_POST['id'];
    $status = $_POST['status'];
    if($status == '1'){
      $result = Subscription::where('id','=',$id)->update(['status' => '0']);
    }else{
      $result = Subscription::where('id','=',$id)->update(['status' => '1']);
    }
    echo $result;exit;
  }

}
