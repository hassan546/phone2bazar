<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Area;
use Illuminate\Http\Request;
use App\State;
use App\City;
use App\Country;
use Illuminate\Validation\Rule;
class CityController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 public function __construct()
    {
        $this->middleware('auth:admin');
    }

     public function index()
     {
     	$cities=City::orderBy('city_name', 'asc')->get();
		//print_r($cities);
		//exit;
     	$country=Country::whereStatus(1)->get();
     	return view('admin.city.view',compact('cities','country'));
     }

	public function create()
    {

		foreach (State::all() as $state)
		{
		  $statenew[$state->state_id] = $state->state;
		}



       return view("admin.city.add",compact('statenew'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		$size="2048";


		$category = new City;

		$this->validate($request, [
		'city_name' => 'required|'.Rule::unique('cities')->ignore($category->city_id),
		 //'image' => 'required|mimes:jpeg,jpg,png|max:'.$size.'',
		 'state_id' => 'required',
		]);




		 $category->city_name = $request->city_name;
		 $category->state_id = $request->state_id;
		 $category->save();

		return redirect()->route('city-management.index')->with(array(
            'success' => "City has been added"
        ));

    }

	public function edit($category)
    {

		foreach (State::all() as $state)
		{
		  $statenew[$state->state_id] = $state->state;
		}
        $categorylist=City::find($category);

		return view('admin.city.edit',compact('categorylist','statenew'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cityid)
    {

		$size="2048";
		$category=City::find($cityid);

		if($request->hasfile('image'))
		{
			$this->validate($request, [
            'city_name' => 'required|'.Rule::unique('cities')->ignore($cityid),
            'image' => 'required|mimes:jpeg,jpg,png|max:'.$size.'',
            'state_id' => 'required',
         ]);

		}
		else
		{
			$this->validate($request, [
            'city_name' => 'required|'.Rule::unique('cities')->ignore($cityid,'city_id'),
			'state_id' => 'required',
         ]);
		}

		if($request->hasfile('image'))
		{

			if($category->image && file_exists(public_path('storage/upload/city').'/'.$category->image))
				unlink(public_path('storage/upload/city').'/'.$category->image);

			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/city');
			  $file->move($destinationPath, $name);
			  $category->image = $name;
		 }

		$category->city_name = $request->city_name;
		$category->state_id = $request->state_id;
		$category->save();
       // return back()->with('success','City Updated');
        return redirect()->route('city-management.index')->with(array(
            'success' => "City has been updated"
        ));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $category,$id)
    {
		 $category = City::find($id);
    	$category->delete();
    	return back()->with('success','City Deleted');
    }

	public function GetCountry(Request $request)
    {
    	$states=State::where('country_id',$request->country_id)->get();
    	return response($states, $status = 200);
    }


    public function GetCity(Request $request)
    {
    	$city=City::where('state_id',$request->state_id)->get();

    	return response($city, $status = 200);
    }
    /**
     * Get the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function GetLocation(Request $request)
    {
    	$state_id=$request->state_id;
    	$city_id=$request->city_id;
    	$area=Area::where('state_id','=',$state_id)->where('city_id','=',$city_id)->get();

    	return response($area);
    }
    public function set_city_nav(){
    	$city_id = $_POST['id'];
    	$status = $_POST['status'];
    	if($status == 'checked'){
    		$displaylist = City::where('is_display', '=', 1)->get();
			$displayCount = $displaylist->count();
			if($displayCount >= 9){
				$result = 9;
			}else{
				$result = City::where('city_id','=',$city_id)->update(['is_display' => 1]);
			}

    	}else{
			$result = City::where('city_id','=',$city_id)->update(['is_display' => 0]);
    	}
    	echo $result;exit;
    }

	public function Setstatus(Request $request)
    {

       $review = City::find($request->id);

	   //print_r($review);

	   if($review->is_active==0)
		   $is_active=1;
	   if($review->is_active==1)
		   $is_active=0;

	   $review->is_active=$is_active;
	   $review->save();

	   return json_encode(array('status'=>"Status change successfully"));

    }

	public function Setstatus3(Request $request)
    {

       $review = City::find($request->id);

	   //print_r($review);

	   if($review->show_in_listing==0)
		   $show_in_listing=1;
	   if($review->show_in_listing==1)
		   $show_in_listing=0;

	   $review->show_in_listing=$show_in_listing;
	   $review->save();

	   return json_encode(array('status'=>"Show in Listing Status change successfully"));

    }


	public function timestamp_ms()
    {
		$time=round(microtime(true) * 1000);
		return $time;
    }

}
