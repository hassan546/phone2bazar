<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\BookAvailability;
use Carbon\Carbon;
use Event;
use Illuminate\Support\Facades\DB;
class PartnerBookAvlController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	protected $perPage = 10;
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->partner_id){
            // echo "asdf";exit;
             $banners=BookAvailability::wherePartnerId($request->partner_id)->orderBy('id', 'desc')->get();
         }else{
            $banners= BookAvailability::orderBy('created_at', 'DESC')->get();
         }


    	return view("admin.partnereventbook.list",compact('banners'));

      // echo "<a href='".route('admin.logout')."'>Logout</a>";
    }

	public function create()
    {


       return view("admin.partneradvertisement.add");
    }

	    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$todate=date("Y-m-d",strtotime($request->todate));

		 $size="2048";
         $banner = new PartnerAdvertisement;
         $this->validate($request, [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:'.$size.'',
            'description' => 'required',
			'position' => 'required',
         ]);

		if($request->hasfile('image'))
		 {
			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/partneradvertisement');
			  $file->move($destinationPath, $name);
		 }

		 $banner->name = $request->name;
		 $banner->status = 1;
		 $banner->description = $request->description;
		 $banner->position = $request->position;
		 $banner->todate  = $todate;
		 $banner->partner_id = $request->partner_id;
		 $banner->image = $name;
		 $banner->save();

		return redirect()->route('adminpartner-advertisement.create')->with(array(
            'success' => "Advertisement has been added"
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(PartnerAdvertisement $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit($banner)
    {
        $banner=PartnerAdvertisement::find($banner);

		return view('admin.partneradvertisement.edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$banner)
    {
		$size="2048";

		$todate=date("Y-m-d",strtotime($request->todate));
        $banner=PartnerAdvertisement::find($banner);

		if($request->hasfile('image'))
		{
			$this->validate($request, [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:'.$size.'',
            'description' => 'required',
			'position' => 'required',
         ]);

		}
		else
		{
			$this->validate($request, [
            'name' => 'required',
            'description' => 'required',
			'position' => 'required',
         ]);
		}
		if($request->hasfile('image'))
		 {
			if($banner ->image && file_exists(public_path('storage/upload/partneradvertisement').'/'.$banner ->image))
				unlink(public_path('storage/upload/partneradvertisement').'/'.$banner ->image);

			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/partneradvertisement');
			  $file->move($destinationPath, $name);
			  $banner->image = $name;
		 }


		 $banner->name = $request->name;
		 $banner->status = 1;
		 $banner->description = $request->description;
		 $banner->position = $request->position;
		 $banner->todate  = $todate;
		 $banner->partner_id = $request->partner_id;
		 $banner->save();

      return back()->with('success','Advertisement Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerEventBook $banner,$id)
    {
		$banner = CustomerEventBook::find($id);

		if($banner ->image)
			unlink(public_path('storage/upload/partnerevents').'/'.$banner ->image);

        $banner->delete();
		return back()->with('success','Booking Deleted');
    }

	public function timestamp_ms()
   {
    $time=round(microtime(true) * 1000);
    return $time;
  }

    public function ChangeStatus(Request $request){

    $id = $_POST['id'];
    $status = $_POST['status'];

	 if($status == '1'){
      $result = PartnerEvent::where('id','=',$id)->update(['admin_status' => '0']);
    }else{
      $result = PartnerEvent::where('id','=',$id)->update(['admin_status' => '1']);
    }

    echo $result;exit;
  }





}
