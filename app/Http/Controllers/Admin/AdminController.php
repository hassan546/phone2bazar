<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\User;
use App\RefferedEmploye;
use App\Admin;
use App\Product;
use App\Chat;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $total_users=DB::table('users')->select(DB::raw('*'))->where('mobile_number','NOT LIKE','%ORR%')
        ->get()->count();
        $totalpartners = DB::table('partners')->select(DB::raw('*'))
        ->get()->count();


    	/*$today_booking=$records;
		$total_booking=$totalrecords;

		$today_earning = DB::table('book_hotels')->select(DB::raw('*'))
                  ->whereRaw('Date(created_at) = CURDATE()')->where('payment_status','=','Paid')->orWhere('paid','=','Paid')->groupBy('order_id')->get()->sum("total_price");

    	//$today_earning=BookHotel::whereCreatedAt(Carbon::today())->sum("total_price");

		$today_earning=$today_earning;



		$total_earning = DB::table('book_hotels')->select(DB::raw('*'))->where('payment_status','=','Paid')->orWhere('paid','=','Paid')->groupBy('order_id')->get()->sum("total_price");

		$cancelbooking = DB::table('book_hotels')->select(DB::raw('*'))
                  ->whereRaw('booking_status = 2')->groupBy('order_id')->get()->count();
		$toodaycancelbooking = DB::table('book_hotels')->select(DB::raw('*'))
                  ->whereRaw('booking_status = 2')->whereRaw('Date(created_at) = CURDATE()')->groupBy('order_id')->get()->count(); */

    	return view("admin-home",compact('total_users','totalpartners'));



       // echo "<a href='".route('admin.logout')."'>Logout</a>";
    }

	public function Settings(Request $request)
    {

		$settings=Setting::find(1);
		$settingsview = unserialize($settings->settings);
		if($request->all())
		{
			$settingspdte = serialize($request->all());
			$settings->settings=$settingspdte;
			$settings->save();
			return redirect()->route('admin.settings');
		}

		return view("admin.settings-edit",compact('settingsview'));

    }


	public function adminprofileupdate(Request $request)
    {

		$profile=Admin::find(1);
		if($request->all())
		{

			$profile->name=$request->name;
			$profile->facebook=$request->facebook;
			$profile->instagram=$request->instagram;
			$profile->linkedin=$request->linkedin;
			$profile->email=$request->email;
			$profile->password=bcrypt($request->password);
			$profile->save();
			return redirect()->route('admin.adminprofileupdate');
		}

		return view("admin.adminprofile-edit",compact('profile'));

    }

    public function referedEmployee(){
        $employee = RefferedEmploye::orderBy('id', 'desc')->get();

        return view("admin.reffered_employee",compact('employee'));
    }

    public function getChatCount(){
       echo  $chatCount = Chat::whereSenderType('Customer')->whereIsRead('0')->get()->count();

    }









}
