<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\ManageOffer;
use Illuminate\Http\Request;
use App\Partner;

class ManageOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        if($request->partner_id){
            // echo "asdf";exit;
            $banners=ManageOffer::wherePartnerId($request->partner_id)->orderBy('id', 'desc')->get();
         }else{
            $banners=ManageOffer::orderBy('created_at', 'DESC')->get();
         }

		return view('admin.manageoffer.list',compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view("admin.manageserice.add");

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

			$size="2048";


         $banner = new ManageOffer;
         $this->validate($request, [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:'.$size.'',
         ]);

		if($request->hasfile('image'))
		 {
			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/service');
			  $file->move($destinationPath, $name);
		 }

		 $banner->name = $request->name;
		 $banner->image = $name;
		 $banner->save();

		return redirect()->route('service-management.create')->with(array(
            'success' => "Service has been added"
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(ManageOffer $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit($banner)
    {
        $banner=ManageOffer::find($banner);

		return view('admin.manageoffer.edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$banner)
    {

			$size="2048";


        $banner=ManageOffer::find($banner);

		if($request->hasfile('banner_image'))
		{
			$this->validate($request, [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:'.$size.'',
         ]);

		}
		else
		{
			$this->validate($request, [
            'name' => 'required',
         ]);
		}
		if($request->hasfile('image'))
		 {
			if($banner->image && file_exists(public_path('storage/upload/service').'/'.$banner ->image))
				unlink(public_path('storage/upload/service').'/'.$banner ->image);

			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/service');
			  $file->move($destinationPath, $name);
			  $banner->image = $name;
		 }


		$banner->name = $request->name;
		$banner->save();
      return back()->with('success','Service Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManageOffer $banner,$id)
    {
		$banner = ManageOffer::find($id);

		if($banner ->image)
			unlink(public_path('storage/upload/partneroffer').'/'.$banner ->image);

        $banner->delete();
		return back()->with('success','Offer Deleted');
    }

	public function timestamp_ms()
   {
    $time=round(microtime(true) * 1000);
    return $time;
  }

    public function change_statusoffer(){
    $id = $_POST['id'];
    $status = $_POST['status'];
    if($status == '1'){
      $result = ManageOffer::where('id','=',$id)->update(['status' => '0']);
    }else{
      $result = ManageOffer::where('id','=',$id)->update(['status' => '1']);
    }
    echo $result;exit;
  }

}
