<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
		$categorylist=SubCategory::orderBy('category', 'asc')->get();
		return view('admin.subcategory.list',compact('categorylist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view("admin.subcategory.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Responses
     */
    public function store(Request $request)
    {
        $category = new SubCategory;


		$this->validate($request, [
            'category' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:1024',
            'icon' => 'required|mimes:jpeg,jpg,png|max:1024'
         ]);


		if($request->hasfile('image'))
		 {
			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/subcategory');
			  $file->move($destinationPath, $name);
			  $category->image = $name;
         }
         if($request->hasfile('icon'))
		 {
			  $file = $request->file('icon');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/subcategory');
			  $file->move($destinationPath, $name);
			  $category->icon = $name;
		 }

		 $category->category = $request->category;
         $category->main_category = $request->main_category;
         $category->display_order = $request->display_order;
		 $category->save();

		return redirect()->route('subcategory-management.index')->with(array(
            'success' => "Sub Category has been added"
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($category)
    {
        $categorylist=SubCategory::find($category);

		return view('admin.subcategory.edit',compact('categorylist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$category)
    {
         $category=SubCategory::find($category);

		if($request->hasfile('image'))
		{
			$this->validate($request, [
            'category' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:1024'
         ]);

		}
		else
		{
			$this->validate($request, [
            'category' => 'required',
         ]);
		}
		if($request->hasfile('image'))
		 {
			if($category ->image && file_exists(public_path('storage/upload/subcategory').'/'.$category ->image))
				unlink(public_path('storage/upload/subcategory').'/'.$category ->image);

			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/subcategory');
			  $file->move($destinationPath, $name);
			  $category->image = $name;
         }
         if($request->hasfile('icon'))
		 {
			if($category ->icon && file_exists(public_path('storage/upload/subcategory').'/'.$category ->icon))
				unlink(public_path('storage/upload/subcategory').'/'.$category ->icon);

			  $file = $request->file('icon');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/subcategory');
			  $file->move($destinationPath, $name);
			  $category->icon = $name;
		 }

		$category->category = $request->category;
        $category->main_category = $request->main_category;
        $category->display_order = $request->display_order;

		$category->save();
      //return back()->with('success','Sub Category Updated');
      return redirect()->route('subcategory-management.index')->with(array(
        'success' => "Sub Category has been Updated"
    ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $category,$id)
    {
        $category = SubCategory::find($id);
		if($category ->image && file_exists(public_path('storage/upload/subcategory').'/'.$category ->image))
				unlink(public_path('storage/upload/subcategory').'/'.$category ->image);

		$category->delete();
		return back()->with('success','Sub Category Deleted');
    }

    public function timestamp_ms()
   {
    $time=round(microtime(true) * 1000);
    return $time;
  }

    public function ChangeStatus(){
    $id = $_POST['id'];
    $status = $_POST['status'];
    if($status == '1'){
      $result = SubCategory::where('id','=',$id)->update(['status' => '0']);
    }else{
      $result = SubCategory::where('id','=',$id)->update(['status' => '1']);
    }
    echo $result;exit;
  }

   public function ChangeStatus2(){
    $id = $_POST['id'];
    $status = $_POST['status'];
    if($status == '1'){
      $result = SubCategory::where('id','=',$id)->update(['show_in_listing' => '0']);
    }else{
      $result = SubCategory::where('id','=',$id)->update(['show_in_listing' => '1']);
    }
    echo $result;exit;
  }


}
