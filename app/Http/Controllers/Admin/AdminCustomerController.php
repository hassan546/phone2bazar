<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class AdminCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $customers = User::where('mobile_number', 'NOT LIKE', '%ORR0%')->orderBy('created_at', 'desc')->get();

       return view('admin.customer.list', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'status' => $request->status,
            'password' => bcrypt($request->password)
        ]);


        $user->save();

        return redirect()->route('adminusers.index')->with(array(
            'flash_message' => "User has been added successfully",
            'flash_type' => 'info'
        ));


    }

	public function AddAmounttoCustoemr(Request $request,$id)
    {
		if($request->all())
		{	 $user = User::find($request->user_id);
			 $user->wallet_balance = $request->wallet_balance;
			 $user->save();
			 return redirect()->route('addamounttocustoemr',array($request->user_id))->with(array(
            'success' => "Amount Updated"
        ));


		}
		$data = array();
        $user = User::find($id);
        $data['data'] = $user;

        return view('admin.customer.addamount', $data);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = array();
        $user = User::find($id);
        $data['data'] = $user;

        return view('admin.customer.view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $users = User::where('id', $id)->first();
        $data['data'] = $users;
        $data['id'] = $id;
        return view('admin.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
               $input = $request->all();
                print_r($input);

                exit;

        $user = User::find($id);

        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'email' => Rule::unique('users')->ignore($id),
            'password' => 'required|min:6',
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->status = $request->status;
        $user->save();


        return redirect()->route('adminusers.index')->with(array(
            'flash_message' => "User has been updated",
            'flash_type' => 'info'
        ));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$user = User::find($id);
        $user->delete();
		return back()->with('success','Customer Deleted');



    }

	public function ChangeStatusCustomer(){
    $id = $_POST['id'];
    $status = $_POST['status'];
    if($status == '1'){
      $result = User::where('id','=',$id)->update(['status' => '0']);
    }else{
      $result = User::where('id','=',$id)->update(['status' => '1']);
    }
    echo $result;exit;
  }



}
