<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Area;
use Illuminate\Http\Request;
use App\State;
use App\Country;
use Illuminate\Validation\Rule;
class StateController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 public function __construct()
    {
        $this->middleware('auth:admin');
    }

     public function index()
     {
     	$states=State::orderBy('state', 'asc')->get();
		//print_r($cities);
		//exit;
     	$country=Country::whereStatus(1)->get();
     	return view('admin.state.view',compact('states','country'));
     }

	public function create()
    {

	foreach (Country::all() as $country)
     {
      $countrynew[$country->id] = $country->name;
    }




       return view("admin.state.add",compact('countrynew'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

			$size="2048";


		$category = new State;

		$this->validate($request, [
		'state' => 'required|'.Rule::unique('states')->ignore($category->id),
		// 'image' => 'required|mimes:jpeg,jpg,png|max:'.$size.'',
		// 'country_id' => 'required',
		]);




		 $category->state = $request->state;
		 //$category->image = $name;
		 $category->country_id = 1;
		 $category->save();

		return redirect()->route('state-management.index')->with(array(
            'success' => "State has been added"
        ));

    }

	public function edit($category)
    {

		foreach (Country::all() as $country)
		{
		  $countrynew[$country->id]=$country->name;
		}


        $categorylist=State::find($category);

		return view('admin.state.edit',compact('categorylist','countrynew'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category)
    {

		$size="2048";

		$category=State::find($category);

		if($request->hasfile('image'))
		{
			$this->validate($request, [
            'state' => 'required|'.Rule::unique('states')->ignore($category->id),
           // 'image' => 'required|mimes:jpeg,jpg,png|max:'.$size.'',
           // 'country_id' => 'required',
         ]);

		}
		else
		{
			$this->validate($request, [
           'state' => 'required',
			//'country_id' => 'required',
         ]);
		}



		 $category->state = $request->state;

		$category->save();
        //return back()->with('success','State Updated');
        return redirect()->route('state-management.index')->with(array(
            'success' => "State has been updated"
        ));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $category,$id)
    {
		 $category = State::find($id);
    	$category->delete();
    	return back()->with('success','State Deleted');
    }
    public function GetCity(Request $request)
    {
    	$city=City::where('state_id',$request->state_id)->get();

    	return response($city, $status = 200);
    }
    /**
     * Get the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function GetLocation(Request $request)
    {
    	$state_id=$request->state_id;
    	$city_id=$request->city_id;
    	$area=Area::where('state_id','=',$state_id)->where('city_id','=',$city_id)->get();

    	return response($area);
    }
    public function set_city_nav(){
    	$city_id = $_POST['id'];
    	$status = $_POST['status'];
    	if($status == 'checked'){
    		$displaylist = City::where('is_display', '=', 1)->get();
			$displayCount = $displaylist->count();
			if($displayCount >= 9){
				$result = 9;
			}else{
				$result = City::where('city_id','=',$city_id)->update(['is_display' => 1]);
			}

    	}else{
			$result = City::where('city_id','=',$city_id)->update(['is_display' => 0]);
    	}
    	echo $result;exit;
    }

	public function Setstatus(Request $request)
    {

       $review = State::find($request->id);

	   //print_r($review);

	   if($review->is_active==0)
		   $is_active=1;
	   if($review->is_active==1)
		   $is_active=0;

	   $review->is_active=$is_active;
	   $review->save();

	   return json_encode(array('status'=>"Status change successfully"));

    }

		public function timestamp_ms()
   {
    $time=round(microtime(true) * 1000);
    return $time;
  }

}
