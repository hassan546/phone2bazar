<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
		$categorylist=Category::orderBy('category', 'asc')->get();
		return view('admin.category.list',compact('categorylist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view("admin.category.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Responses
     */
    public function store(Request $request)
    {
        $category = new Category;


		if($request->hasfile('image'))
		{
			$this->validate($request, [
            'category' => 'required|unique:categories,category',
            'image' => 'required|mimes:jpeg,jpg,png|max:2048',
            'icon' => 'required|mimes:jpeg,jpg,png|max:2048'
			]);

		}
		else
		{
			 $this->validate($request, [
            'category' => 'required|unique:categories,category'
			]);
		}

		if($request->hasfile('image'))
		 {
			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/category');
			  $file->move($destinationPath, $name);
			  $category->image = $name;
		 }
         if($request->hasfile('icon'))
		 {
			  $file = $request->file('icon');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/category');
			  $file->move($destinationPath, $name);
			  $category->icon = $name;
		 }

		$category->category = $request->category;
		$category->display_order = $request->display_order;
		$category->save();

		return redirect()->route('category-management.index')->with(array(
            'success' => "Category has been added"
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($category)
    {
        $categorylist=Category::find($category);

		return view('admin.category.edit',compact('categorylist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$category)
    {
        $category=Category::find($category);

			$this->validate($request, [
            'category' => 'required|unique:categories,category,'.$category->id,
           // 'image' => 'required|mimes:jpeg,jpg,png|max:2048'
         ]);


		if($request->hasfile('image'))
		 {
			if($category ->image && file_exists(public_path('storage/upload/category').'/'.$category ->image))
				unlink(public_path('storage/upload/category').'/'.$category ->image);

			  $file = $request->file('image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/category');
			  $file->move($destinationPath, $name);
			  $category->image = $name;
		 }

		 if($request->hasfile('icon'))
		 {
			if($category ->icon && file_exists(public_path('storage/upload/category').'/'.$category ->icon))
				unlink(public_path('storage/upload/category').'/'.$category ->icon);

			  $file = $request->file('icon');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/category');
			  $file->move($destinationPath, $name);
			  $category->icon = $name;
		 }

		 if($request->hasfile('banner_image'))
		 {
			if($category ->banner_image && file_exists(public_path('storage/upload/category').'/'.$category ->banner_image))
				unlink(public_path('storage/upload/category').'/'.$category ->banner_image);

			  $file = $request->file('banner_image');
			  $name=$this->timestamp_ms().'.'.$file->getClientOriginalExtension();
			  $destinationPath = public_path('storage/upload/category');
			  $file->move($destinationPath, $name);
			  $category->banner_image = $name;
		 }
		$category->category = $request->category;
		$category->display_order = $request->display_order;

		$category->save();
      //return back()->with('success','Category Updated');
      return redirect()->route('category-management.index')->with(array(
        'success' => "Category has been Updated"
    ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category,$id)
    {
        $category = Category::find($id);
		if($category ->image && file_exists(public_path('storage/upload/category').'/'.$category ->image))
				unlink(public_path('storage/upload/category').'/'.$category ->image);
		if($category ->icon && file_exists(public_path('storage/upload/category').'/'.$category ->icon))
				unlink(public_path('storage/upload/category').'/'.$category ->icon);

		if($category ->banner_image && file_exists(public_path('storage/upload/category').'/'.$category ->banner_image))
				unlink(public_path('storage/upload/category').'/'.$category ->banner_image);

		$category->delete();
		return back()->with('success','Category Deleted');
    }

    public function timestamp_ms()
   {
    $time=round(microtime(true) * 1000);
    return $time;
  }

    public function ChangeStatus(){
    $id = $_POST['id'];
    $status = $_POST['status'];
    if($status == '1'){
      $result = Category::where('id','=',$id)->update(['status' => '0']);
    }else{
      $result = Category::where('id','=',$id)->update(['status' => '1']);
    }
    echo $result;exit;
  }

}
