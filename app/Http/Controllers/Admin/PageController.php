<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
       $pages=Page::orderBy('content', 'asc')->get();
	  return view('admin.page.list',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.page.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = new Page;
         $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
         ]);


		 $page->title = $request->title;
		 $page->slug = str_slug($request->title, "-");
		 $page->content  = $request->content ;
		 $page->save();

		return redirect()->route('page-management.index')->with(array(
            'success' => "Page has been added"
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit($page)
    {
        $page=Page::find($page);
		return view('admin.page.edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $page)
    {
        $page=Page::find($page);
	    $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
         ]);


		$page->title = $request->title;
		$page->slug = str_slug($request->title, "-");

		$page->content = $request->content;
		$page->save();
        //return back()->with('success','Page Updated');
        return redirect()->route('page-management.index')->with(array(
            'success' => "Page has been added"
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page,$id)
    {
       $page = Page::find($id);
       $page->delete();
	   return back()->with('success','Page Deleted');
    }

	public function ChangeStatusPage(){
    $id = $_POST['id'];
    $status = $_POST['status'];
    if($status == '1'){
      $result = Page::where('id','=',$id)->update(['status' => '0']);
    }else{
      $result = Page::where('id','=',$id)->update(['status' => '1']);
    }
    echo $result;exit;
  }
}
