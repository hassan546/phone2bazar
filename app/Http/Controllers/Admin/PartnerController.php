<?php

namespace App\Http\Controllers\Admin;

use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\ProductReview;
use Image; //Intervention Image
use App\Country;
use App\User;
use App\State;
use App\City;
use App\Category;
use App\SubCategory;
use App\WalletTransaction;
use App\PartnerGallery;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Carbon\Carbon;



class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
    {
		ini_set('memory_limit', '100M');
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $partners=Partner::orderBy('created_at', 'DESC')->get();
		return view('admin.partner.list',compact('partners'));
    }

	public function HotelierRegistration()
    {
        $hoteliers=Hotelier::whereRegistrationFrom('Others')->whereStatus(0)->orderBy('created_at', 'DESC')->get();
		return view('admin.hotelier.list-registration',compact('hoteliers'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	foreach (Category::all() as $category)
      {
       $categories[$category->id]=$category->category;
     }
     foreach (State::all() as $state)
     {
      $states[$state->state_id]=$state->state;
    }



        return view("admin.partner.add",compact('categories','states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		 $data=array();
         $partner = new Partner;
         $this->validate($request, [
            'company_name' => 'required|max:50',
            'email' => 'required|email|unique:partners',
            'password' => 'required',
            'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"|unique:partners',
            'whatsapp_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"|unique:partners',
            'description' => 'required',
            'address' => 'required',
			//'image' => 'required',
         ],["mobile_number.regex"=>"The mobile number must be start with 9 or 8 or 7 or 6"],
         ["whatsapp_number.regex"=>"The mobile number must be start with 9 or 8 or 7 or 6"]);
        $name="";
		if($request->hasfile('profile_image'))
		 {
            $image = $request->profile_image;
				  $time=$this->timestamp_ms();
				  $name =$time.'.'.$image->getClientOriginalExtension();

				  $filenametostore=$time.'_thumb.'.$image->getClientOriginalExtension();
				  $destinationPath = public_path('storage/upload/partnerimage');


				  $destinationPath = public_path('storage/upload/partnerimage');
				  $image->move($destinationPath, $name);

         }
         if($request->hasfile('image'))
		 {
			    $i=1;
				foreach($request->file('image') as $img)
				{
				  $time1=$this->timestamp_ms();
				  $name1=$time1.$i.'.'.$img->getClientOriginalExtension();
				  /* $thumb_img2 = Image::make($image->getRealPath())->resize(570, 405);
				  $thumb_img2->save($destinationPath2.'/'.$filenametostore2,80); */


				  $destinationPath1 = public_path('storage/upload/partnergallery');
				  $img->move($destinationPath1, $name1);
				  $data[] = $name1;
				  //$data[] = $filenametostore;
				  $i++;
				}
		 }

        $password = $request->password;
        $partner->unique_id = "P2B".strtotime(Carbon::now());
		$partner->company_name = $request->company_name;
		 $partner->email = $request->email;
		 $partner->mobile_number = $request->mobile_number;
		 $partner->whatsapp_number = $request->whatsapp_number;
         $partner->category_id  =  $request->category_id;
         $partner->sub_cat_id = $request->sub_cat_id;
		 $partner->city = $request->city;
		 $partner->password =  bcrypt($password);
		 $partner->textpassword =  $password;
         $partner->state = $request->state;
         $partner->reference_id = 'Self';
		 $partner->openning_time   = date('Y-m-d H:i',strtotime($request->openning_time));
		 $partner->closing_time   = date('Y-m-d H:i',strtotime($request->closing_time));
         $partner->profile_percentage = 100;
		 $partner->description = $request->description;
         $partner->image = ($name) ? $name : "";
         $partner->web_link = $request->website;
         $partner->facebook = $request->facebook;
         $partner->instagram = $request->instagram;
         $partner->latitude = $request->latitude;
         $partner->longitude = $request->longitude;
         $partner->address = $request->address;
         $partner->save();
         $order="ORR00".rand(1000,999999);
         $usernew = User::create([
             'name' => '',
             'email' => $order.'@dfsdfsd.com',
             'mobile_number' => $order,
             'password' => ''
         ]);
         $recordcheck = User::find($partner->id);


		if(!$recordcheck)
			$bookings= User::whereId($usernew->id)->update(array('id'=>$partner->id));
		else
		{
			$bookings= Partner::whereId($partner->id)->update(array('id'=>$usernew->id));
        }
        foreach($data as $dt){
             $partnergallery = New PartnerGallery;
             $partnergallery->partner_id  = $partner->id;
             $partnergallery->image = $dt;
             $partnergallery->save();
         }

		return redirect()->route('partner-management.index')->with(array(
            'success' => "Partner has been added"
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Hotelier $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($partnerid)
    {
        $customerdetails=Partner::find($partnerid);
        foreach (Category::all() as $category)
        {
        $categories[$category->id]=$category->category;
        }
        foreach (State::all() as $state)
        {
        $states[$state->state_id]=$state->state;
        }
        $gallery = PartnerGallery::wherePartnerId($partnerid)->get();
		return view('admin.partner.edit',compact('customerdetails','categories','states','gallery'));
    }

    public function view($partnerid)
    {
        $partner=Partner::find($partnerid);
        print_r($partner);exit;

		return view('admin.partner.edit',compact('categories','states','partners'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$product)
    {

		$databaseimage=array();
		$data=[];

        $partner=Partner::find($product);
       // print_r($partner);die;

		if($request->hasfile('image'))
		{
			$this->validate($request, [
            'company_name' => 'required|max:50',
            'email' => 'required|email|'.Rule::unique('partners')->ignore($product),
			'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"|'.Rule::unique('partners')->ignore($product),
			'description' => 'required',
			'profile_image' => 'required',
			'profile_image.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
            ],["mobile_number.regex"=>"The mobile number must be start with 9 or 8 or 7 or 6"]);


		}
		else
		{

			$this->validate($request, [
            'company_name' => 'required|max:50',
			'description' => 'required',
            'email' => 'required|email|'.Rule::unique('partners')->ignore($product),
			'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"|'.Rule::unique('partners')->ignore($product),

         ],["mobile_number.regex"=>"The mobile number must be start with 9 or 8 or 7 or 6"]);


		}
		if($request->hasfile('profile_image'))
		{
            $image = $request->profile_image;
			$time = $this->timestamp_ms();
			$name = $time.'.'.$image->getClientOriginalExtension();

			$destinationPath = public_path('storage/upload/partnerimage');
			$image->move($destinationPath, $name);
		}else{
			$name = $partner->image;
        }
       //print_r($request->all());exit;
         $partner->category_id  = $request->category_id;
         $partner->sub_cat_id  = $request->sub_cat_id;
		 $partner->city = $request->city;
		 $partner->state = $request->state;
         $partner->openning_time   = date('H:i',strtotime($request->openning_time));
         $partner->closing_time = date('H:i',strtotime($request->closing_time));
		 $partner->address   = $request->address ;
		 $partner->image = $name;
         $partner->description = $request->description;
         $partner->facebook = $request->facebook;
         $partner->instagram = $request->instagram;
         $partner->web_link = $request->web_link;
         $partner->address = $request->address;
         $partner->latitude = $request->latitude;
         $partner->longitude = $request->longitude;
         $partner->unique_id = "P2B".strtotime($partner->created_at);

        $partner->save();
       // print_r($partner);exit;
       // return back()->with('success','Partner profile Updated');
       return redirect()->route('partner-management.index')->with(array(
        'success' => "Partner Profile has been updated"
    ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotelier $product,$id)
    {
       $hotelier = Hotelier::find($id);

	   $review = HotelierReview::whereHotelierId($hotelier->id)->delete();
	   $slot = HotelierSlot::whereHotelierId($hotelier->id)->delete();
	   $bookhotel = BookHotel::whereHotelNo($hotelier->hotel_number)->delete();
	   if($hotelier->getOriginal('image'))
	   {
		$image_arr=json_decode($hotelier->getOriginal('image'));
	   	for($i=0;$i<count($image_arr);$i++)
	    {
		if($image_arr[$i] && file_exists(public_path('storage/upload/hotelier').'/'.$image_arr[$i]))
			unlink(public_path('storage/upload/hotelier').'/'.$image_arr[$i]);

		}
	   }
        $hotelier->delete();
		return back()->with('success','Hotelier Deleted');
    }

	public function timestamp_ms()
    {
    $time=round(microtime(true) * 1000);
    return $time;
    }



    public function ChangeStatus(Request $request){

	//print_r($request->all());
	//exit;
    $id = $_POST['id'];
    $status = $_POST['status'];
    if($status == '1'){
      $result = Partner::where('id','=',$id)->update(['status' => '0']);
    }else{
      $result = Partner::where('id','=',$id)->update(['status' => '1']);
    }

	if($request->others)
	{
		$result2 = Partner::where('id','=',$id)->update(['registration_from' => 'Admin','status' => '0']);
	}

	echo $result;exit;
  }

    public function ChangeStatus2(Request $request){

        $id = $_POST['id'];
        $status = $_POST['status'];
        if($status == '1'){
          $result = Partner::where('id','=',$id)->update(['show_in_listing' => '0']);
        }else{
          $result = Partner::where('id','=',$id)->update(['show_in_listing' => '1']);
        }
        echo $result;exit;
	}
    public function AddAmountPartner(Request $request){
        $id = $_POST['id'];
        $amount = $_POST['amount'];
        if($amount){
          $result = Partner::where('id','=',$id)->increment('wallet_balance',$amount);
          if($result){
            $wallet_trans = new WalletTransaction;
            $wallet_trans->partner_id = $id;
            $wallet_trans->amount = $amount;
            $wallet_trans->trans_type = 'cr';
            $wallet_trans->remark = 'Wallet credited by Admin';
            $wallet_trans->created_at = Carbon::now();
            $wallet_trans->save();
          }
        }
        echo $result;exit;
    }



      public function ChangeStatusReviws()
	  {
		$id = $_POST['id'];
		$status = $_POST['status'];
		if($status == '1'){
		  $result = ProductReview::where('id','=',$id)->update(['status' => '0']);
		}else{
		  $result = ProductReview::where('id','=',$id)->update(['status' => '1']);
		}
		echo $result;exit;
	  }



 public function thumbImage(Request $request)
  {
	    $id=$request->id;
		$image=$request->image;

		$hotel=Hotelier::find($id);
		$hotel->thum_image=$image;
		$hotel->save();
  }
  public function removeImage(Request $request)
  {
	$array2=array();
	//echo json_encode($array);
	//exit;

    $id=$request->id;
    $image=$request->image;

    $hotel=Hotelier::find($id);
    $image_arr=json_decode($hotel->getOriginal('image'));

		if(in_array($image,$image_arr))
		{
				$pos = array_search($image, $image_arr);
				unset($image_arr[$pos]);

				$split=explode('.',$image);
				$thumimage= $split[0].'_thumb.'.$split[1];
				$pos1 = array_search($thumimage, $image_arr);
				unset($image_arr[$pos1]);

				$medium= $split[0].'_medium.'.$split[1];
				$pos2 = array_search($medium, $image_arr);
				unset($image_arr[$pos2]);


				if($image && file_exists(public_path('storage/upload/hotelier').'/'.$image))
				{
				unlink(public_path('storage/upload/hotelier').'/'.$image);
				unlink(public_path('storage/upload/hotelier').'/'.$thumimage);
				unlink(public_path('storage/upload/hotelier').'/'.$medium);
				}


		}


	foreach($image_arr as $value)
	{
		$array2[]=$value;
	}
    $hotel->image=json_encode($array2);
    $hotel->save();
  }


  	public function GetPublishedprice(Request $request)
	{
		$gst="";
		$publisedprice="";
		$sitesettings=SiteSetting::find(1);
		if($request->base_price<=999)
		{	$gst= $request->base_price * $sitesettings->gst0/100;
			$publisedprice= $request->base_price + $gst;
			return response(round($publisedprice));
		}
		if($request->base_price>=1000 && $request->base_price<=2499 )
		{
			$gst= $request->base_price * $sitesettings->gst12/100;
			$publisedprice= $request->base_price + $gst;
			return response(round($publisedprice));
		}
		if($request->base_price>=2500 && $request->base_price<=7499 )
		{
			$gst= $request->base_price * $sitesettings->gst18/100;
			$publisedprice= $request->base_price + $gst;
			return response(round($publisedprice));
		}
		if($request->base_price>=7500 )
		{
			$gst= $request->base_price * $sitesettings->gst28/100;
			$publisedprice= $request->base_price + $gst;
			return response(round($publisedprice));
		}

	}

    public function Getsubcategory(Request $request)
    {
    	$subcategory=SubCategory::where('main_category',$request->main_category)->get();
    	return response($subcategory, $status = 200);
    }




}
