<?php

namespace App\Http\Controllers\Api\Partner;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Partner;
use App\User;
use App\RefferedEmploye;
use Auth;
use App\Mail\MailEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PassportController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

	public function register(Request $request)
    {

		$validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"|unique:partners',
			'email' => 'required|email|unique:partners',
            'reference_id' => 'required',
            'password' => 'required|max:12'
        ],["mobile_number.regex"=>"The mobile number must be start with 9 or 8 or 7 or 6"]);


		/* $validator = $this->validate($request, [
            'name' => 'required|min:3|max:50',
			'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"|unique:partners',
            'email' => 'required|email|unique:partners',
            'password' => 'required|min:8',
        ],["mobile_number.regex"=>"The mobile number must be start with 9 or 8 or 7 or 6"]); */



		if ($validator->fails()) {
            $errMsg = json_decode($validator->messages());
            if(isset($errMsg->name[0])){
                $err = $errMsg->name[0];
            }elseif(isset($errMsg->mobile_number[0])){
                $err = $errMsg->mobile_number[0];
            }else if(isset($errMsg->email[0])){
                 $err = $errMsg->email[0];
            }else if(isset($errMsg->reference_id[0])){
                 $err = $errMsg->reference_id[0];
            }else {
                $err = "Please pass required parameters";
            }
			return response()->json(['result' => 0,'message' => $err], 200);
		}

        $user = Partner::create([
            'company_name' => $request->name,
            'email' => $request->email,
            'unique_id' => "P2B".strtotime(Carbon::now()),
			'mobile_number' => $request->mobile_number,
            'password' => bcrypt($request->password),
            'textpassword' => $request->password,
            'reference_id' =>$request->reference_id,
            'reference_mobile' => $request->reference_mobile,
            'profile_percentage'=> 15
        ]);

		$order="ORR00".rand(1000,999999);
		$usernew = User::create([
            'name' => '',
            'email' => $order.'@dfsdfsd.com',
			'mobile_number' => $order,
            'password' => ''
        ]);

		$recordcheck = User::find($user->id);


		if(!$recordcheck)
			$bookings= User::whereId($usernew->id)->update(array('id'=>$user->id));
		else
		{
			$bookings= Partner::whereId($user->id)->update(array('id'=>$usernew->id));
        }
        if($request->reference_mobile){
            $reffered = RefferedEmploye::create([
                'partner_id' => $user->id,
                'mobile_number' => $request->reference_mobile,
                'created_at'=> Carbon::now()
            ]);
        }

		$token = $user->createToken('Eartiapartner')->accessToken;
		if($token)
		{
			$data['user'] = $user;
			$data['token']= $token;
            return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
		}
		else
			 return response()->json(['result' => 0,'message' => 'Something Error'], 200);


    }

	public function login(Request $request)
    {

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->guard('partner')->attempt($credentials)) {
            $token = auth()->guard('partner')->user()->createToken('Eartiapartner')->accessToken;
			$data['token']=$token;
            $userdata = auth()->guard('partner')->user();
            $status = $userdata->status;

                $data['user'] = [
                    'id'=>$userdata->id,
                    'company_name'=>$userdata->company_name,
                    'email'=>$userdata->email,
                    'mobile_number'=>$userdata->mobile_number,
                    'category_id'=>($userdata->category_id) ? $userdata->category_id : "",
                    'sub_cat_id'=>($userdata->sub_cat_id) ? $userdata->sub_cat_id :"",
                    'address'=>($userdata->address) ? $userdata->address : "",
                    'state_id'=>($userdata->state_id) ? $userdata->state_id : "",
					'city_id'=>($userdata->city) ? $userdata->city : "",
					'textpassword' =>($userdata->textpassword) ? $userdata->textpassword : "",
                    'openning_time'=>($userdata->openning_time) ? $userdata->openning_time : "",
                    'closing_time'=> ($userdata->closing_time) ? $userdata->closing_time : "",
                    'subscription_id'=>($userdata->subscription_id)? $userdata->subscription_id : "",
                    'profile_percentage'=>($userdata->profile_percentage)? (string)$userdata->profile_percentage : "",
                    'facebook'=>($userdata->facebook)? $userdata->facebook : "",
                    'twitter'=>($userdata->twitter)? $userdata->twitter : "",
                    'linkedin'=>($userdata->linkedin)? $userdata->linkedin : "",
                    'instagram'=>($userdata->instagram)? $userdata->instagram : "",
                    'web_link'=>($userdata->web_link)? $userdata->web_link : "",
                    'user_type' => 'vendor'
                ];
                return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);



        } else {
            return response()->json(['result' => 0,'message' => 'UnAuthorised'], 200);
        }
    }


	public function forgotpassword(Request $request)
    {

		$message="";
		$field="email";
		$validation="required";
		$regualr=array('login.required'=>'The email must be required');
		$data=array();
		if(is_numeric($request->input('login')))
		{
		     $field = 'mobile_number';
			 $validation='required|numeric|digits:10|regex:"^[9876]\d{9}"';
			 $regualr=array('login.digits'=>'The mobile number must be 10 digit','login.regex'=>'The mobile number must be start with 9 or 8 or 7 or 6');
		}
		elseif(filter_var($request->input('login'), FILTER_VALIDATE_EMAIL))
		{
			$field = 'email';
		}


		$validator = Validator::make($request->all(), [
            'login' => $validation,
        ],$regualr);

		if ($validator->fails()) {

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
		}



		$otp = rand(100000,999999);
		if($field=="mobile_number")
		{

			$user=Partner::where("mobile_number","=",$request->input('login'))->first();
			if($user)
			{
				$message.= 'Please use this temporary password and login into your account : '.$otp;
                $user->password =  bcrypt($otp);
                $user->textpassword =  $otp;
				$user->save();
				getSMMMobile1($user->mobile_number,$message);
				return response()->json(['result' => 1,'message' => 'Success'], 200);
			}
			else
				 return response()->json(['result' => 0,'message' => 'Not Found'], 200);
		}
		elseif($field=="email")
		{

			$user=Partner::where("email","=",$request->input('login'))->first();
			if($user)
			{

				$message.= 'Please use this temporary password and login into your account : '.$otp;
				$user->password =  bcrypt($otp);
				$user->textpassword =  $otp;
				$user->save();

				$response = new \stdClass();
				$response->mailtemplate = "mails.common";
				$response->subject = "Temporary password";
				$response->mailcontent = $message;

			if(!\App::environment('local'))
			  Mail::to($user->email)->send(new MailEmail($response));
				return response()->json(['result' => 1,'message' => 'Success'], 200);
			}
			else
				 return response()->json(['result' => 0,'message' => 'Not Found'], 200);
		}
		else
			return response()->json(['result' => 0,'message' => 'Not Found'], 200);
    }


}
