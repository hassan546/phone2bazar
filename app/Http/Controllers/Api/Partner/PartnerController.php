<?php

namespace App\Http\Controllers\Api\Partner;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use App\User;
use App\Partner;
use App\PartnerGallery;
use App\CustomerPartnerReview;
use App\PartnerJob;
use App\PartnerCourse;
use App\PartnerProduct;
use App\ManageOffer;
use App\PartnerAdvertisement;
use App\PartnerKeyword;
use App\PartnerEventPrice;
use App\PartnerEvent;
use App\Testimonial;
use App\PartnerMenu;
use App\PartnerLead;
use App\Subscription;
use App\PurchaseSubscription;
use App\WalletTransaction;
use App\BookAvailability;
use App\Analytic;
use Illuminate\Http\Request;
use Arr;
use Event;
use File;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
class PartnerController extends Controller
{
    public function partnerdetails()
    {
		$userdata = Partner::whereId(auth()->guard('vendor-api')->user()->id)->get();
           $data['user'] = [
                    'id'=>$userdata[0]->id,
                    'company_name'=>$userdata[0]->company_name,
                    'email'=>$userdata[0]->email,
                    'mobile_number'=>$userdata[0]->mobile_number,
                    'category_id'=>($userdata[0]->category_id) ? $userdata[0]->category_id : "",
                    'sub_cat_id'=>($userdata[0]->sub_cat_id) ? $userdata[0]->sub_cat_id :"",
                    'address'=>($userdata[0]->address) ? $userdata[0]->address : "",
                    'state_id'=>($userdata[0]->state_id) ? $userdata[0]->state_id : "",
                    'city_id'=>($userdata[0]->city) ? $userdata[0]->city : "",
                    'openning_time'=>($userdata[0]->openning_time) ? $userdata[0]->openning_time : "",
                    'closing_time'=> ($userdata[0]->closing_time) ? $userdata[0]->closing_time : "",
                    'subscription_id'=>($userdata[0]->subscription_id)? $userdata[0]->subscription_id : "",
                    'profile_percentage'=>($userdata[0]->profile_percentage)? $userdata[0]->profile_percentage : "",
                    'facebook'=>($userdata[0]->facebook)? $userdata[0]->facebook : "",
                    'twitter'=>($userdata[0]->twitter)? $userdata[0]->twitter : "",
                    'linkedin'=>($userdata[0]->linkedin)? $userdata[0]->linkedin : "",
                    'instagram'=>($userdata[0]->instagram)? $userdata[0]->instagram : "",
                    'web_link'=>($userdata[0]->web_link)? $userdata[0]->web_link : "",
                    'image' => ($userdata[0]->image)? asset('public/storage/upload/partnerimage/').'/'.$userdata[0]->image : "",
            ];
			 return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);


    }

	public function partnerdashboard()
    {
		$user= auth()->guard('vendor-api')->user();
		$data['partnerdetails'] = $user->only(['id','status_company','company_name','about_company ','image','imagepath']);
		$data['totalproducts'] = auth()->guard('vendor-api')->user()->getParternProducts->count();
		$data['totaljobs'] = auth()->guard('vendor-api')->user()->getJobs->count();
		$data['totalevents'] = auth()->guard('vendor-api')->user()->getEvents->count();
		$data['totalgallery'] = auth()->guard('vendor-api')->user()->getGallery->count();
		$data['totalservices'] = auth()->guard('vendor-api')->user()->getParternService->count();
		$data['totalcourses'] = auth()->guard('vendor-api')->user()->getCourse->count();
		$i=0;
		foreach(auth()->guard('vendor-api')->user()->getReviews as $keyreview=>$review)
		{
			$data['notification'][$i]['userimage']= $review->getUser->profile_image;
			$data['notification'][$i]['imagepath']=asset('public/storage/upload/userimage/');
			$data['notification'][$i]['name']=$review->name;
			$data['notification'][$i]['rating']=$review->rating;
			$data['notification'][$i]['message']=$review->message;
			$data['notification'][$i]['created_at']=$review->getOriginal('created_at');

			$i++;
		}

		/* $i=0;
		foreach(auth()->guard('vendor-api')->user()->getReviews as $keyreview=>$review)
		{
			$valk= $i+1;
			$data['ratingpercentage'][$i][$valk.'startotal']= auth()->guard('vendor-api')->user()->getReviews->where('rating',1)->count();
			$data['ratingpercentage'][$i][$valk.'startotalreviews']=auth()->guard('vendor-api')->user()->getReviews->count();
			$data['ratingpercentage'][$i][$valk.'startotalpercentage']=user()->getReviews->where('rating',$valk)->count() * 100)/auth()->guard('vendor-api')->user()->getReviews->count(),2);
			$i++;
		} */


		//$data['notification'] =  auth()->guard('vendor-api')->user()->getReviews()->with('getUser')->get();


		$data['ratingpercentage'][0]['total'] = auth()->guard('vendor-api')->user()->getReviews->where('rating',1)->count();
		$data['ratingpercentage'][0]['totalreviews'] = auth()->guard('vendor-api')->user()->getReviews->count();
		$data['ratingpercentage'][0]['percentage'] = (auth()->guard('vendor-api')->user()->getReviews->count() > 0 ? round((auth()->guard('vendor-api')->user()->getReviews->where('rating',1)->count() * 100)/auth()->guard('vendor-api')->user()->getReviews->count(),2) : 0);

		$data['ratingpercentage'][1]['total'] = auth()->guard('vendor-api')->user()->getReviews->where('rating',2)->count();
		$data['ratingpercentage'][1]['totalreviews'] = auth()->guard('vendor-api')->user()->getReviews->count();
		$data['ratingpercentage'][1]['percentage'] = (auth()->guard('vendor-api')->user()->getReviews->count() > 0 ? round((auth()->guard('vendor-api')->user()->getReviews->where('rating',2)->count() * 100)/auth()->guard('vendor-api')->user()->getReviews->count(),2): 0);

		$data['ratingpercentage'][2]['total'] = auth()->guard('vendor-api')->user()->getReviews->where('rating',3)->count();
		$data['ratingpercentage'][2]['totalreviews'] = auth()->guard('vendor-api')->user()->getReviews->count();
		$data['ratingpercentage'][2]['percentage'] = (auth()->guard('vendor-api')->user()->getReviews->count() > 0 ? round((auth()->guard('vendor-api')->user()->getReviews->where('rating',3)->count() * 100)/auth()->guard('vendor-api')->user()->getReviews->count(),2): 0);

		$data['ratingpercentage'][3]['total'] = auth()->guard('vendor-api')->user()->getReviews->where('rating',4)->count();
		$data['ratingpercentage'][3]['totalreviews'] = auth()->guard('vendor-api')->user()->getReviews->count();
		$data['ratingpercentage'][3]['percentage'] = (auth()->guard('vendor-api')->user()->getReviews->count() > 0 ? round((auth()->guard('vendor-api')->user()->getReviews->where('rating',4)->count() * 100)/auth()->guard('vendor-api')->user()->getReviews->count(),2): 0);

		$data['ratingpercentage'][4]['total'] = auth()->guard('vendor-api')->user()->getReviews->where('rating',5)->count();
		$data['ratingpercentage'][4]['totalreviews'] = auth()->guard('vendor-api')->user()->getReviews->count();
		$data['ratingpercentage'][4]['percentage'] = (auth()->guard('vendor-api')->user()->getReviews->count() > 0 ? round((auth()->guard('vendor-api')->user()->getReviews->where('rating',5)->count() * 100)/auth()->guard('vendor-api')->user()->getReviews->count(),2): 0);


        return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
    }


	public function partnerlogout() {
        $accessToken = auth()->guard('vendor-api')->user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);
        $accessToken->revoke();
        return response()->json(array('result' => 1,'message' => 'Successfully logged out'), 200);
    }

	public function changepassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|max:15|min:5',
            'old_password' => 'required'
        ]);
        if ($validator->fails())
            {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->old_password[0])){
                    $err = $errMsg->old_password[0];
                }else if(isset($errMsg->password[0])){
                    $err = $errMsg->password[0];
                }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }
		else
		{
            $user = Partner::find(auth()->guard('vendor-api')->user()->id);
            if($user->textpassword == $request->old_password){
                $user->password = bcrypt($request->password);
                $user->textpassword = $request->password;
                $user->save();
                return response()->json(array('result' => 1,'message' => 'Successfully Changed Password'), 200);
            }else{
                return response()->json(['result' => 0,'message' => "Old password is wrong",], 200);
            }
        }
	}



	public function ManageJobs(Request $request)
    {
		if($request->type=="add")
		{
			$validator = Validator::make($request->all(), [
			'name' => 'required',
			'description' => 'required',
			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}

			$jobs = new PartnerJob;
			$jobs->description = $request->description;
			$jobs->name = $request->name;
			$jobs->apply_link = $request->apply_link;
			$jobs->partner_id = auth()->guard('vendor-api')->user()->id;
			$jobs->save();

			$jobs = PartnerJob::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$jobs->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $jobs->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);




		}
		elseif($request->type=="edit")
		{
			$jobs=PartnerJob::find($request->jobid);
			$validator = Validator::make($request->all(), [
				'name' => 'required',
				'description' => 'required'
				]);
			if($validator->fails())
					return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);

			$jobs->description = $request->description;
			$jobs->name = $request->name;
			$jobs->apply_link = $request->apply_link;
			$jobs->partner_id = auth()->guard('vendor-api')->user()->id;
			$jobs->save();

			$jobs = PartnerJob::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$jobs->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $jobs->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}
		elseif($request->type=="active")
		{
			$jobs = PartnerJob::find($request->jobid);
			$jobs->status = 1;
			$jobs->save();
			$jobs = PartnerJob::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$jobs->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $jobs->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{
			$jobs = PartnerJob::find($request->jobid);
			$jobs->status = 0;
			$jobs->save();
			$jobs = PartnerJob::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$jobs->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $jobs->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}
		elseif($request->type=="delete")
		{
			$jobs = PartnerJob::find($request->jobid);
			$jobs->delete();

			$jobs = PartnerJob::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$jobs->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $jobs->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		else
		{
			$jobs = PartnerJob::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$jobs->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $jobs->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}


	}



	public function MangeFeedback(Request $request)
    {
		if($request->type=="add")
		{
			$validator = Validator::make($request->all(), [
			'description' => 'required',
			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}

			$gallery = new Testimonial;
			$gallery->description = $request->description;
			$gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			$gallery->title = auth()->guard('vendor-api')->user()->company_name;
			$gallery->image = auth()->guard('vendor-api')->user()->image;

			$gallery->save();

			$gallery = Testimonial::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);




		}
		elseif($request->type=="edit")
		{
			$gallery=Testimonial::find($request->feedbackid);
			$validator = Validator::make($request->all(), [
				'description' => 'required'
				]);
			if($validator->fails())
					return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);

			$gallery->description = $request->description;
			$gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			$gallery->title = auth()->guard('vendor-api')->user()->company_name;
			$gallery->image = auth()->guard('vendor-api')->user()->image;
			$gallery->save();

			$gallery = Testimonial::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}
		elseif($request->type=="active")
		{
			$gallery = Testimonial::find($request->feedbackid);
			$gallery->status = 1;
			$gallery->save();
			$gallery = Testimonial::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{
			$gallery = Testimonial::find($request->feedbackid);
			$gallery->status = 0;
			$gallery->save();
			$gallery = Testimonial::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}
		elseif($request->type=="delete")
		{
			$gallery = Testimonial::find($request->feedbackid);
			$gallery->delete();

			$gallery = Testimonial::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		else
		{
			$gallery = Testimonial::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}


	}


	public function MangeLeads(Request $request)
    {
		if($request->type=="active")
		{
			$gallery = PartnerLead::find($request->id);
			$gallery->status = 1;
			$gallery->save();
			$gallery = PartnerLead::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{

			$gallery = PartnerLead::find($request->id);
			$gallery->status = 0;
			$gallery->save();
			$gallery = PartnerLead::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}
		else
		{
			$gallery = PartnerLead::wherePartnerId(auth()->guard('vendor-api')->user()->id)->Orderby('id','DESC')->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}

	}

	public function MangeEventsBooked(Request $request)
    {
		$data=array();
		$customerdetails = PartnerEvent::whereHas('getBookedEvents', function($query)
		{
		})->with('getBookedEvents')->wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();

		$i=0;
		foreach($customerdetails as $kye=>$value)
		{
			foreach($value->getBookedEvents as $key2=>$value2)
			{
			  $userdetials= User::find($value2->user_id);
			  $data[$i]['event_name'] = $value->name;
			  $data[$i]['event_image'] = $value->image;
			  $data[$i]['imagepath'] = asset('public/storage/upload/partnerevents/');
			  $data[$i]['event_price'] = $value2->booking_price;
			  $data[$i]['name'] = $userdetials->name;
			  $data[$i]['email'] = $userdetials->email;
			  $data[$i]['mobile_number'] = $userdetials->mobile_number;
			  $i++;
			}
		}

		if(count($data)>0)
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $data
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

	}

	public function MangeReview(Request $request)
    {
		if($request->type=="active")
		{
			$gallery = CustomerPartnerReview::find($request->reviewid);
			$gallery->status = 1;
			$gallery->save();
			$gallery = CustomerPartnerReview::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{

			$gallery = CustomerPartnerReview::find($request->reviewid);
			$gallery->status = 0;
			$gallery->save();
			$gallery = CustomerPartnerReview::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}
		else
		{
			$gallery = CustomerPartnerReview::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
            if(!$gallery->isEmpty()){
                $total_rating = 0;
                $fivestar = 0;$fourstar=0;$threestar=0;$twostart=0;$onestar=0;
                $totalcount = count($gallery);
                foreach($gallery as $rat){
                    $total_rating = $total_rating+$rat->rating;
                    if($rat->rating ==5){ $fivestar=$fivestar+1; }
                    if($rat->rating ==4){ $fourstar=$fourstar+1; }
                    if($rat->rating ==3){ $threestar=$threestar+1; }
                    if($rat->rating ==2){ $twostart=$twostart+1;}
                    if($rat->rating ==1){ $onestar=$onestar+1;}
                    $data[] = [
                        'id' => $rat->id,
                        'user_id' => $rat->user_id,
                        'name' => $rat->name,
                        'rating' => $rat->rating,
                        'review' => $rat->message,
                        'date' => date('d M Y',strtotime($rat->created_at)),
                        'image' => ($rat->getUser->profile_image) ? asset('public/storage/upload/profile').'/'.$rat->getUser->profile_image: ""
                    ];
                }
                return response()->json([
                    'result' => 1,
                    'message' => "Success",
                    'average' => round($total_rating/$totalcount),
                    '5_star' => $fivestar,
                    '4_star' => $fourstar,
                    '3_star' => $threestar,
                    '2_star' => $twostart,
                    '1_star' => $onestar,
                    'data' => $data
                ],200);
			}else{
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);
            }

		}

	}




	public function MangeCourses(Request $request)
    {
		if($request->type=="add")
		{
			$validator = Validator::make($request->all(), [
            'name' => 'required',
			'description' => 'required',
			'image_string' => 'required',
			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}

			$image_string = $request->image_string;
			$gallery = new PartnerCourse;
			if($request->has('image_string'))
			{
				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partnercourse';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				$gallery->name = $request->name;
				$gallery->duration = $request->duration;
				$gallery->start_date =  date('Y-m-d',strtotime($request->start_date));
				$gallery->end_date =  date('Y-m-d',strtotime($request->end_date));
				$gallery->end_time = $request->end_time;
				$gallery->start_time = $request->start_time;
				$gallery->features = $request->features;
				$gallery->booking_seg = $request->booking_seg;
				$gallery->price_seg = $request->price_seg;
				$gallery->diffrent_seg = $request->diffrent_seg;
				$gallery->terms_cond = $request->terms_cond;
				$gallery->faq = $request->faq;
				$gallery->description = $request->description;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = PartnerCourse::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
			}
			else
				return response()->json(['result' => 0,'message' => "Something wrong",], 200);


		}
		elseif($request->type=="edit")
		{
			$gallery=PartnerCourse::find($request->courseid);
			$image_string = $request->image_string;

			if($request->has('image_string'))
			{
				$validator = Validator::make($request->all(), [
				'name' => 'required',
				'description' => 'required',
				'image_string' => 'required',
				]);

				if($validator->fails())
					return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);

				if($gallery->image && file_exists(public_path('storage/upload/partnercourse').'/'.$gallery->image))
				unlink(public_path('storage/upload/partnercourse').'/'.$gallery->image);

				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partnercourse';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				$gallery->name = $request->name;
				$gallery->duration = $request->duration;
				$gallery->start_date =  date('Y-m-d',strtotime($request->start_date));
				$gallery->end_date =  date('Y-m-d',strtotime($request->end_date));
				$gallery->end_time = $request->end_time;
				$gallery->start_time = $request->start_time;
				$gallery->features = $request->features;
				$gallery->booking_seg = $request->booking_seg;
				$gallery->price_seg = $request->price_seg;
				$gallery->diffrent_seg = $request->diffrent_seg;
				$gallery->terms_cond = $request->terms_cond;
				$gallery->faq = $request->faq;
				$gallery->description = $request->description;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = PartnerCourse::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

			}
			else
			{
				$validator = Validator::make($request->all(), [
				'name' => 'required',
				'description' => 'required',
				]);

				if($validator->fails())
					return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);

				$gallery->name = $request->name;
				$gallery->description = $request->description;
				$gallery->duration = $request->duration;
				$gallery->start_date =  date('Y-m-d',strtotime($request->start_date));
				$gallery->end_date =  date('Y-m-d',strtotime($request->end_date));
				$gallery->end_time = $request->end_time;
				$gallery->start_time = $request->start_time;
				$gallery->features = $request->features;
				$gallery->booking_seg = $request->booking_seg;
				$gallery->price_seg = $request->price_seg;
				$gallery->diffrent_seg = $request->diffrent_seg;
				$gallery->terms_cond = $request->terms_cond;
				$gallery->faq = $request->faq;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = PartnerCourse::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);



			}

		}
		elseif($request->type=="active")
		{
			$gallery = PartnerCourse::find($request->courseid);
			$gallery->status = 1;
			$gallery->save();
			$gallery = PartnerCourse::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{

			$gallery = PartnerCourse::find($request->courseid);
			$gallery->status = 0;
			$gallery->save();
			$gallery = PartnerCourse::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);



		}
		elseif($request->type=="delete")
		{
			$gallery = PartnerCourse::find($request->courseid);
			if($gallery ->image)
				unlink(public_path('storage/upload/partnercourse').'/'.$gallery ->image);
			$gallery->delete();

			$gallery = PartnerCourse::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}

		else
		{
			$gallery = PartnerCourse::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}


	}



	public function MangeEvents(Request $request)
    {
		if($request->type=="add")
		{

			$validator = Validator::make($request->all(), [
            'name' => 'required',
			'description' => 'required',
			'image_string' => 'required',
            'from_date' => 'required',
            'to_date' => 'required'

			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}

			$image_string = $request->image_string;
			$gallery = new PartnerEvent;
			if($request->has('image_string'))
			{
				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partneroffers';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				$gallery->name = $request->name;
				$gallery->participants = $request->participants;
				$gallery->duration = $request->duration;
				$gallery->start_date = date('Y-m-d',strtotime($request->start_date));
				$gallery->end_date = date('Y-m-d',strtotime($request->end_date));

				$gallery->start_time = $request->start_time;
				$gallery->end_time = $request->end_time;

				$gallery->address = $request->address;
				/* $gallery->booking_segregation = $request->booking_segregation;
				$gallery->prizes_segregation = $request->prizes_segregation;
				$gallery->different_prizes = $request->different_prizes; */
				$gallery->terms_cond = $request->terms_cond;
				$gallery->faq = $request->faq;
				$gallery->description = $request->description;
				/* $gallery->price = $request->price; */
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				foreach($price as $key=>$value)
				{
				  $eventprice = new PartnerEventPrice;
				  $eventprice->price = $value->price;
				  $eventprice->title = $value->title;
				  $eventprice->event_id  = $gallery->id;
				  $eventprice->price_description = $value->price_description;
				  $eventprice->save();

				}
				$gallery = PartnerEvent::with('getEventPrice')->wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
			}
			else
				return response()->json(['result' => 0,'message' => "Something wrong",], 200);


		}
		elseif($request->type=="edit")
		{
			$price = json_decode($request->price);
			$gallery=PartnerEvent::find($request->eventid);
			$image_string = $request->image_string;

			if($request->has('image_string'))
			{
				$validator = Validator::make($request->all(), [
				'name' => 'required',
				'description' => 'required',
				'price' => 'required',
				'image_string' => 'required',
				]);

				if($validator->fails())
					return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);

				if($gallery->image && file_exists(public_path('storage/upload/partnerevents').'/'.$gallery->image))
				unlink(public_path('storage/upload/partnerevents').'/'.$gallery->image);

				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partnerevents';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				$gallery->name = $request->name;
				$gallery->participants = $request->participants;
				$gallery->duration = $request->duration;
				$gallery->start_date = date('Y-m-d',strtotime($request->start_date));
				$gallery->end_date = date('Y-m-d',strtotime($request->end_date));
				$gallery->start_time = $request->start_time;
				$gallery->end_time = $request->end_time;
				$gallery->address = $request->address;
				/* $gallery->booking_segregation = $request->booking_segregation;
				$gallery->prizes_segregation = $request->prizes_segregation;
				$gallery->different_prizes = $request->different_prizes; */
				$gallery->terms_cond = $request->terms_cond;
				$gallery->faq = $request->faq;
				$gallery->description = $request->description;
				/* $gallery->price = $request->price; */
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$eventpricedelte = PartnerEventPrice::whereEventId($gallery->id)->delete();

				foreach($price as $key=>$value)
				{
				  $eventprice = new PartnerEventPrice;
				  $eventprice->price = $value->price;
				  $eventprice->title = $value->title;
				  $eventprice->event_id  = $gallery->id;
				  $eventprice->price_description = $value->price_description;
				  $eventprice->save();

				}

				$gallery = PartnerEvent::with('getEventPrice')->wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

			}
			else
			{
				$validator = Validator::make($request->all(), [
				'name' => 'required',
				'description' => 'required',
				'price' => 'required'
				]);

				if($validator->fails())
					return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);

				$gallery->name = $request->name;
				$gallery->participants = $request->participants;
				$gallery->duration = $request->duration;
				$gallery->start_date = date('Y-m-d',strtotime($request->start_date));
				$gallery->end_date = date('Y-m-d',strtotime($request->end_date));
				$gallery->start_time = $request->start_time;
				$gallery->end_time = $request->end_time;

				$gallery->address = $request->address;
				/* $gallery->booking_segregation = $request->booking_segregation;
				$gallery->prizes_segregation = $request->prizes_segregation;
				$gallery->different_prizes = $request->different_prizes; */
				$gallery->terms_cond = $request->terms_cond;
				$gallery->faq = $request->faq;
				$gallery->description = $request->description;
				//$gallery->price = $request->price;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$eventpricedelte = PartnerEventPrice::whereEventId($gallery->id)->delete();

				foreach($price as $key=>$value)
				{
				  $eventprice = new PartnerEventPrice;
				  $eventprice->price = $value->price;
				  $eventprice->title = $value->title;
				  $eventprice->event_id  = $gallery->id;
				  $eventprice->price_description = $value->price_description;
				  $eventprice->save();

				}


				$gallery = PartnerEvent::with('getEventPrice')->wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);



			}

		}
		elseif($request->type=="active")
		{
			$gallery = PartnerEvent::find($request->eventid);
			$gallery->status = 1;
			$gallery->save();
			$gallery = PartnerEvent::with('getEventPrice')->wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{

			$gallery = PartnerEvent::find($request->eventid);
			$gallery->status = 0;
			$gallery->save();
			$gallery = PartnerEvent::with('getEventPrice')->wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);



		}
		elseif($request->type=="delete")
		{
			$gallery = PartnerEvent::find($request->eventid);
			if($gallery ->image)
				unlink(public_path('storage/upload/partnerevents').'/'.$gallery ->image);
			$gallery->delete();

			$gallery = PartnerEvent::with('getEventPrice')->wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="bookedevents")
		{
			$data=array();
			$customerdetails = PartnerEvent::whereHas('getBookedEvents', function($query)
			{
			})->with('getBookedEvents')->wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();

			$i=0;
			foreach($customerdetails as $kye=>$value)
			{
				foreach($value->getBookedEvents as $key2=>$value2)
				{
				  $userdetials= User::find($value2->user_id);
				  $data[$i]['event_name'] = $value->name;
				  $data[$i]['event_image'] = asset('public/storage/upload/partnerevents/').'/'.$value->image;
				  $data[$i]['event_price'] = $value2->booking_price;
				  $data[$i]['name'] = $userdetials->name;
				  $data[$i]['email'] = $userdetials->email;
				  $data[$i]['mobile_number'] = $userdetials->mobile_number;
				  $i++;
				}

			}
			if($data)
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $data
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}

		else
		{

			$gallery = PartnerEvent::with('getEventPrice')->wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}


	}


	public function MangeKeyword(Request $request)
    {
		$keywords=PartnerKeyword::wherePartnerId(auth()->guard('vendor-api')->user()->id)->first();

		if($request->type=="addedit")
		{
			if($request->all())
			{
				if(!$keywords)
				{
					 $keywords = new PartnerKeyword;
					 $keywords->description = $request->description;
					 $keywords->partner_id = auth()->guard('vendor-api')->user()->id;
					 $keywords->save();
				}
				else
				{
					 $keywords->description = $request->description;
					 $keywords->save();
				}

				$keywords=PartnerKeyword::wherePartnerId(auth()->guard('vendor-api')->user()->id)->first();
				if($keywords)
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $keywords->toArray()
					],200);
					else
				return response()->json(['result' => 0,'message' => "Not Found",], 200);
			}
		}
		else
			if($keywords)
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $keywords->toArray()
					],200);
					else
				return response()->json(['result' => 0,'message' => "Not Found",], 200);

	}

	public function MangeOffers(Request $request)
    {
		if($request->type=="add")
		{
			$validator = Validator::make($request->all(), [
            'name' => 'required',
			'description' => 'required',
            'valid_from' => 'required',
            'valid_upto' => 'required',
			'image_string' => 'required',
			]);

			if ($validator->fails()) {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->name[0])){
                    $err = $errMsg->name[0];
                }elseif(isset($errMsg->description[0])){
                    $err = $errMsg->description[0];
                }else if(isset($errMsg->image_string[0])){
                     $err = $errMsg->image_string[0];
                }else if(isset($errMsg->valid_from[0])){
                     $err = $errMsg->valid_from[0];
                }else if(isset($errMsg->valid_upto[0])){
                    $err = $errMsg->valid_upto[0];
               }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }

			$image_string = $request->image_string;
			$gallery = new ManageOffer;
			if($request->has('image_string'))
			{
				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partneroffer';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				$gallery->name = $request->name;
				$gallery->description = $request->description;
                $gallery->valid_from = $request->valid_from;
                $gallery->valid_upto = $request->valid_upto;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = ManageOffer::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
			}
			else
				return response()->json(['result' => 0,'message' => "Something wrong",], 200);


		}
		elseif($request->type=="edit")
		{
			$gallery=ManageOffer::find($request->offerid);
			$image_string = $request->image_string;

			if($request->has('image_string'))
			{
				$validator = Validator::make($request->all(), [
				'name' => 'required',
				'description' => 'required',
                'valid_from' => 'required',
                'valid_upto' => 'required',
				'image_string' => 'required',
				]);

				if ($validator->fails()) {
                    $errMsg = json_decode($validator->messages());
                    if(isset($errMsg->name[0])){
                        $err = $errMsg->name[0];
                    }elseif(isset($errMsg->description[0])){
                        $err = $errMsg->description[0];
                    }else if(isset($errMsg->image_string[0])){
                         $err = $errMsg->image_string[0];
                    }else if(isset($errMsg->valid_from[0])){
                         $err = $errMsg->valid_from[0];
                    }else if(isset($errMsg->valid_upto[0])){
                        $err = $errMsg->valid_upto[0];
                   }else {
                        $err = "Please pass required parameters";
                    }
                    return response()->json(['result' => 0,'message' => $err], 200);
                }
				if($gallery->image && file_exists(public_path('storage/upload/partneroffer').'/'.$gallery->image))
				unlink(public_path('storage/upload/partneroffer').'/'.$gallery->image);

				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partneroffer';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				$gallery->name = $request->name;
				$gallery->description = $request->description;
                $gallery->valid_from = $request->valid_from;
                $gallery->valid_upto = $request->valid_upto;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = ManageOffer::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

			}
			else
			{
				$validator = Validator::make($request->all(), [
				'name' => 'required',
				'description' => 'required',
                'valid_from' => 'required',
                'valid_upto' => 'required'
				]);

				if ($validator->fails()) {
                    $errMsg = json_decode($validator->messages());
                    if(isset($errMsg->name[0])){
                        $err = $errMsg->name[0];
                    }elseif(isset($errMsg->description[0])){
                        $err = $errMsg->description[0];
                    }else if(isset($errMsg->valid_from[0])){
                         $err = $errMsg->valid_from[0];
                    }else if(isset($errMsg->valid_upto[0])){
                        $err = $errMsg->valid_upto[0];
                   }else {
                        $err = "Please pass required parameters";
                    }
                    return response()->json(['result' => 0,'message' => $err], 200);
                }
				$gallery->name = $request->name;
				$gallery->description = $request->description;
                $gallery->valid_from = $request->valid_from;
                $gallery->valid_upto = $request->valid_upto;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = ManageOffer::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);



			}

		}
		elseif($request->type=="active")
		{
			$gallery = ManageOffer::find($request->offerid);
			$gallery->status = 1;
			$gallery->save();
			$gallery = ManageOffer::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{
			$gallery = ManageOffer::find($request->offerid);
			$gallery->status = 0;
			$gallery->save();
			$gallery = ManageOffer::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);



		}
		elseif($request->type=="delete")
		{
			$gallery = ManageOffer::find($request->offerid);
			if($gallery ->image)
				unlink(public_path('storage/upload/partneroffer').'/'.$gallery ->image);
			$gallery->delete();

			$gallery = ManageOffer::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		else
		{
			$gallery = ManageOffer::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}


	}



    public function MangeProducts(Request $request)
    {
		if($request->type=="add")
		{
			$validator = Validator::make($request->all(), [
            'name' => 'required',
			'description' => 'required',
			'price' => 'required',
			'image_string' => 'required',
			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}

			$image_string = $request->image_string;
			$gallery = new PartnerProduct;
			if($request->has('image_string'))
			{
				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partnerproducts';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				$gallery->name = $request->name;
				$gallery->description = $request->description;
				$gallery->price = $request->price;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = PartnerProduct::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
			}
			else
				return response()->json(['result' => 0,'message' => "Something wrong",], 200);


		}
		elseif($request->type=="edit")
		{
			$gallery=PartnerProduct::find($request->productid);
			$image_string = $request->image_string;

			if($request->has('image_string'))
			{
				$validator = Validator::make($request->all(), [
				'name' => 'required',
				'description' => 'required',
				'price' => 'required',
				'image_string' => 'required',
				]);

				if($validator->fails())
					return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);

				if($gallery->image && file_exists(public_path('storage/upload/partnerproducts').'/'.$gallery->image))
				unlink(public_path('storage/upload/partnerproducts').'/'.$gallery->image);

				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partnerproducts';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				$gallery->name = $request->name;
				$gallery->description = $request->description;
				$gallery->price = $request->price;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = PartnerProduct::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

			}
			else
			{
				$validator = Validator::make($request->all(), [
				'name' => 'required',
				'description' => 'required',
				'price' => 'required'
				]);

				if($validator->fails())
					return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);

				$gallery->name = $request->name;
				$gallery->description = $request->description;
				$gallery->price = $request->price;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = PartnerProduct::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);



			}

		}
		elseif($request->type=="active")
		{
			$gallery = PartnerProduct::find($request->productid);
			$gallery->status = 1;
			$gallery->save();
			$gallery = PartnerProduct::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{
			$gallery = PartnerProduct::find($request->productid);
			$gallery->status = 0;
			$gallery->save();
			$gallery = PartnerProduct::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);



		}
		elseif($request->type=="delete")
		{
			$gallery = PartnerProduct::find($request->productid);
			if($gallery ->image)
				unlink(public_path('storage/upload/partnerproducts').'/'.$gallery ->image);
			$gallery->delete();

			$gallery = PartnerProduct::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		else
		{
			$gallery = PartnerProduct::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}


	}



	public function MangeAdvertisement(Request $request)
    {
		if($request->type=="add")
		{
			$validator = Validator::make($request->all(), [
            'position' => 'required',
			'image_string' => 'required',
			]);

			if ($validator->fails()) {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->position[0])){
                    $err = $errMsg->position[0];
                }else if(isset($errMsg->image_string[0])){
                     $err = $errMsg->image_string[0];
                }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }

			$image_string = $request->image_string;
            $gallery = PartnerAdvertisement::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
           // if(!$gallery->isEmpty())
            $user = auth()->guard('vendor-api')->user();
            $subscriptionid = ($user->subscription_id) ? $user->subscription_id : "";
            if($subscriptionid=="" || $subscriptionid==0){
                return response()->json(['result' => 0,'message' => 'You have not purchased any Plan'], 200);
            }
            $subscriptiondata = Subscription::find($subscriptionid);
            $totalAdvertise = count($gallery);
            $no_of_advertisement = $subscriptiondata->no_of_advertisement;
            $expire_on = $user->subscription_expire_on;
            if($totalAdvertise >= $no_of_advertisement){
                return response()->json(['result' => 0,'message' => 'Your limit has reached to post your advertisement'], 200);
            }
            if( strtotime($expire_on) < strtotime(Carbon:: today())){
                return response()->json(['result' => 0,'message' => 'Your subscribed plan has expired'], 200);
            }

			if($request->has('image_string'))
			{
				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partneradvertisement';
				$filetosave = $url.'/'.$filename;
                File::put($url.'/' . $filename, $imgdata1);
                $gallery1 = new PartnerAdvertisement;
				$gallery1->image = $filename;
				$gallery1->url = $request->url;
				$gallery1->position = $request->position;
			    $gallery1->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery1->save();

				$gallery = PartnerAdvertisement::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
			}
			else
				return response()->json(['result' => 0,'message' => "Something wrong",], 200);


		}
		elseif($request->type=="edit")
		{
			$gallery=PartnerAdvertisement::find($request->adverid);
			$image_string = $request->image_string;

			if($request->has('image_string'))
			{
				$validator = Validator::make($request->all(), [
				'position' => 'required',
				'image_string' => 'required',
				]);

				if ($validator->fails()) {
                    $errMsg = json_decode($validator->messages());
                    if(isset($errMsg->position[0])){
                        $err = $errMsg->position[0];
                    }else if(isset($errMsg->image_string[0])){
                         $err = $errMsg->image_string[0];
                    }else {
                        $err = "Please pass required parameters";
                    }
                    return response()->json(['result' => 0,'message' => $err], 200);
                }


				if($gallery->image && file_exists(public_path('storage/upload/partneradvertisement').'/'.$gallery->image))
				unlink(public_path('storage/upload/partneradvertisement').'/'.$gallery->image);

				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partneradvertisement';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				$gallery->url = $request->url;
				$gallery->position = $request->position;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = PartnerAdvertisement::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

			}
			else
			{
				$validator = Validator::make($request->all(), [
				'position' => 'required'
				]);

				if ($validator->fails()) {
                    $errMsg = json_decode($validator->messages());
                    if(isset($errMsg->position[0])){
                        $err = $errMsg->position[0];
                    }else {
                        $err = "Please pass required parameters";
                    }
                    return response()->json(['result' => 0,'message' => $err], 200);
                }
				$gallery->url = $request->url;
				$gallery->position = $request->position;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = PartnerAdvertisement::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);



			}

		}
		elseif($request->type=="active")
		{
			$gallery = PartnerAdvertisement::find($request->adverid);
			$gallery->status = 1;
			$gallery->save();
			$gallery = PartnerAdvertisement::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{
			$gallery = PartnerAdvertisement::find($request->adverid);
			$gallery->status = 0;
			$gallery->save();
			$gallery = PartnerAdvertisement::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);



		}
		elseif($request->type=="delete")
		{
			$gallery = PartnerAdvertisement::find($request->adverid);
			if($gallery ->image)
				unlink(public_path('storage/upload/partneradvertisement').'/'.$gallery ->image);
			$gallery->delete();

			$gallery = PartnerAdvertisement::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		else
		{
			$gallery = PartnerAdvertisement::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}


	}


	public function MangeMenuFoods(Request $request)
    {
		if($request->type=="add")
		{
			$validator = Validator::make($request->all(), [
            'name' => 'required',
			'image_string' => 'required',
			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}

			$image_string = $request->image_string;
			$menufood = new PartnerMenu;
			if($request->has('image_string'))
			{
				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partnermenu';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$menufood->image = $filename;
				$menufood->name = $request->name;
			    $menufood->partner_id = auth()->guard('vendor-api')->user()->id;
			    $menufood->save();

				$menufood = PartnerMenu::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$menufood->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $menufood->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);



			}


		}
		elseif($request->type=="edit")
		{
			$menufood=PartnerMenu::find($request->menuid);
			$image_string = $request->image_string;

			if($request->has('image_string'))
			{
				$validator = Validator::make($request->all(), [
				'name' => 'required',
				'image_string' => 'required',
				]);

				if($validator->fails())
					return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);

				if($menufood->image && file_exists(public_path('storage/upload/partnermenu').'/'.$menufood->image))
				unlink(public_path('storage/upload/partnermenu').'/'.$menufood->image);

				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partnermenu';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$menufood->image = $filename;
				$menufood->name = $request->name;
			    $menufood->partner_id = auth()->guard('vendor-api')->user()->id;
			    $menufood->save();

				$menufood = PartnerMenu::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$menufood->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $menufood->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

			}
			else
			{
				$validator = Validator::make($request->all(), [
				'name' => 'required',
				]);

				if($validator->fails())
					return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);


				$menufood->name = $request->name;
			    $menufood->partner_id = auth()->guard('vendor-api')->user()->id;
			    $menufood->save();

				$menufood = PartnerMenu::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$menufood->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $menufood->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
			}

		}
		elseif($request->type=="active")
		{
			$menufood = PartnerMenu::find($request->menuid);
			$menufood->status = 1;
			$menufood->save();
			$menufood = PartnerMenu::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$menufood->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $menufood->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{
			$menufood = PartnerMenu::find($request->menuid);
			$menufood->status = 0;
			$menufood->save();
			$menufood = PartnerMenu::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$menufood->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $menufood->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);



		}
		elseif($request->type=="delete")
		{
			$menufood = PartnerMenu::find($request->menuid);
			if($menufood ->image)
				unlink(public_path('storage/upload/partnermenu').'/'.$menufood ->image);
			$menufood->delete();

			$menufood = PartnerMenu::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$menufood->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $menufood->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		else
		{
			$menufood = PartnerMenu::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$menufood->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $menufood->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}


	}


	public function MangeGallary(Request $request)
    {
		if($request->type=="add")
		{
			$validator = Validator::make($request->all(), [
            'name' => 'required',
			'image_string' => 'required',
			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}

			$image_string = $request->image_string;
			$gallery = new PartnerGallery;
			if($request->has('image_string'))
			{
				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partnergallery';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				$gallery->name = $request->name;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = PartnerGallery::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);



			}


		}
		elseif($request->type=="active")
		{
			$gallery = PartnerGallery::find($request->galleryid);
			$gallery->status = 1;
			$gallery->save();
			$gallery = PartnerGallery::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="inactive")
		{
			$gallery = PartnerGallery::find($request->galleryid);
			$gallery->status = 0;
			$gallery->save();
			$gallery = PartnerGallery::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);



		}
		elseif($request->type=="delete")
		{
            $validator = Validator::make($request->all(), [
                'galleryid' => 'required'
                ]);

                if($validator->fails()){

                return response()->json(['result' => 0,'message' => 'Please pass all parameter'], 200);
                }
			$gallery = PartnerGallery::find($request->galleryid);
			if($gallery ->image)
				unlink(public_path('storage/upload/partnergallery').'/'.$gallery ->image);
			$gallery->delete();

			return response()->json([
				'result' => 1,
				'message' => "Success"
			],200);

		}
		else
		{
			$gallery = PartnerGallery::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}


	}


	public function subscription(Request $request)
    {
		if($request->all())
		{
			$validator = Validator::make($request->all(), [
            'subscription_id' => 'required',
			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}

			/* $this->validate($request, ['subscription_id' => 'required']); */

			$user = auth()->guard('vendor-api')->user();

			$user->subscription_id = $request->subscription_id;
			if($user->profile_percentage!=100)
            $user->profile_percentage =  100;
            $user->save();


			$data['user'] = Partner::whereId(auth()->guard('vendor-api')->user()->id)->get(array('subscription_id'));
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
		}
		else
		{
			$data['user'] = Partner::whereId(auth()->guard('vendor-api')->user()->id)->get(array('subscription_id'));
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
		}
	}



	public function bankdetails(Request $request)
    {
		if($request->all())
		{


			$validator = Validator::make($request->all(), [
				'bank_name' => 'required|max:50',
                'bank_branch' => 'required',
                'bank_account' => 'required',
                'bank_ifsc' => 'required'
			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}


			$user = auth()->guard('vendor-api')->user();

			$user->bank_name = $request->bank_name;
            $user->bank_branch = $request->bank_branch;
            $user->bank_account =  $request->bank_account;
            $user->bank_ifsc =  $request->bank_ifsc;
			if($user->profile_percentage!=100)
            $user->profile_percentage =  80;
            $user->save();


			$data['user'] = Partner::whereId(auth()->guard('vendor-api')->user()->id)->get(array('bank_name','bank_account','bank_branch','bank_ifsc'));
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);

		}
		else
		{
			$data['user'] = Partner::whereId(auth()->guard('vendor-api')->user()->id)->get(array('bank_name','bank_account','bank_branch','bank_ifsc'));
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
		}
	}
	public function partnerContactdetails(Request $request)
    {
		if($request->all())
		{

			$validator = Validator::make($request->all(), [
				'email' => 'required|email|'.Rule::unique('partners')->ignore(auth()->guard('vendor-api')->user()->id),
                'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"|'.Rule::unique('partners')->ignore(auth()->guard('vendor-api')->user()->id),
               "mobile_number.regex"=>"The mobile number must be start with 9 or 8 or 7 or 6"]);

               if ($validator->fails())
               {
                   $errMsg = json_decode($validator->messages());
                  if(isset($errMsg->mobile_number[0])){
                       $err = $errMsg->mobile_number[0];
                   }else if(isset($errMsg->email[0])){
                       $err = $errMsg->email[0];
                   }else {
                       $err = "Please pass required parameters";
                   }
                   return response()->json(['result' => 0,'message' => $err], 200);
               }


			$user = auth()->guard('vendor-api')->user();
			$user->email = $request->email;
            $user->address =  $request->address;
            $user->mobile_number =  $request->mobile_number;
            $user->whatsapp_number =  $request->whatsapp_number;
            $user->state = $request->state;
			$user->city = $request->city;
			$user->web_link = $request->web_link;
            $user->facebook =  $request->facebook;
            $user->instagram =  $request->instagram;
            $user->latitude = $request->latitude;
            $user->longitude = $request->longitude;
                if($user->profile_percentage!=100 && $user->profile_percentage == 15){
                    $user->profile_percentage = 55;
                }else if(($user->profile_percentage != 100) && ($user->profile_percentage == 50)){
                    $user->profile_percentage = 90;
                }else if($user->profile_percentage!=100 && ($user->profile_percentage > 15 && $user->profile_percentage == 25)){
                    $user->profile_percentage = 65;
                }else if($user->profile_percentage!=100 && ($user->profile_percentage > 15 && $user->profile_percentage == 55)){
                    $user->profile_percentage = 55;
                }else if($user->profile_percentage!=100 && ($user->profile_percentage > 25 && $user->profile_percentage == 90)){
                    $user->profile_percentage = 90;
                }else if($user->profile_percentage!=100 && ($user->profile_percentage > 25 && $user->profile_percentage == 60)){
                    $user->profile_percentage = 100;
                }
            $user->save();


			$partnerinfo = Partner::with(['getState'=> function($q)
			{
				// Query the is_active field in getStates table
				$q->select(array('state_id', 'state')); // '=' is optional
			},
			'getCity'=> function($q)
			{
				// Query the is_active field in getStates table
				$q->select(array('city_id', 'city_name')); // '=' is optional
			}])->whereId(auth()->guard('vendor-api')->user()->id)->get();
             $data = [
                'id'=> (string)$partnerinfo[0]->id,
                'address'=> ($partnerinfo[0]->address) ? $partnerinfo[0]->address:"",
                'state_id'=>($partnerinfo[0]->profile_percentage) ? $partnerinfo[0]->profile_percentage : "",
                'state_id'=>($partnerinfo[0]->state) ? (string)$partnerinfo[0]->state:"",
                'state_name'=>($partnerinfo[0]->state) ? $partnerinfo[0]->getState->state:"",
                'profile_percentage'=>($partnerinfo[0]->profile_percentage) ? (string)$partnerinfo[0]->profile_percentage : "",
                'city_id'  => ($partnerinfo[0]->city) ? (string)$partnerinfo[0]->city:"",
                'city_name' => ($partnerinfo[0]->city) ? $partnerinfo[0]->getCity->city_name : "",
                'email' => ($partnerinfo[0]->email) ? $partnerinfo[0]->email : "",
                'mobile_number' => ($partnerinfo[0]->mobile_number) ? (string)$partnerinfo[0]->mobile_number:"",
                'whatsapp_number' => ($partnerinfo[0]->whatsapp_number) ? (string)$partnerinfo[0]->whatsapp_number:"",
                'website' => ($partnerinfo[0]->web_link) ? $partnerinfo[0]->web_link : "",
                'facebook' => ($partnerinfo[0]->facebook) ? $partnerinfo[0]->facebook : "",
                'instagram' => ($partnerinfo[0]->instagram) ? $partnerinfo[0]->instagram:"",
                'latitude' => ($partnerinfo[0]->latitude) ? $partnerinfo[0]->latitude:"",
                'longitude' => ($partnerinfo[0]->longitude) ? $partnerinfo[0]->longitude:"",

            ];
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);

		}
		else
		{
			$partnerinfo = Partner::whereId(auth()->guard('vendor-api')->user()->id)->get();


            $data = [
                'id'=> $partnerinfo[0]->id,
                'address'=> ($partnerinfo[0]->address) ? (string)$partnerinfo[0]->address:"",
                'state_id'=>($partnerinfo[0]->profile_percentage) ? $partnerinfo[0]->profile_percentage : "",
                'state_id'=>($partnerinfo[0]->state) ? (string)$partnerinfo[0]->state:"",
                'state_name'=>($partnerinfo[0]->state) ? $partnerinfo[0]->getState->state:"",
                'profile_percentage'=>($partnerinfo[0]->profile_percentage) ? (string)$partnerinfo[0]->profile_percentage : "",
                'city_id'  => ($partnerinfo[0]->city) ? (string)$partnerinfo[0]->city:"",
                'city_name' => ($partnerinfo[0]->city) ? $partnerinfo[0]->getCity->city_name : "",
                'email' => ($partnerinfo[0]->email) ? $partnerinfo[0]->email : "",
                'mobile_number' => ($partnerinfo[0]->mobile_number) ? (string)$partnerinfo[0]->mobile_number:"",
                'whatsapp_number' => ($partnerinfo[0]->whatsapp_number) ? (string)$partnerinfo[0]->whatsapp_number:"",
                'website' => ($partnerinfo[0]->web_link) ? $partnerinfo[0]->web_link : "",
                'facebook' => ($partnerinfo[0]->facebook) ? $partnerinfo[0]->facebook : "",
                'instagram' => ($partnerinfo[0]->instagram) ? $partnerinfo[0]->instagram:"",
                'latitude' => ($partnerinfo[0]->latitude) ? $partnerinfo[0]->latitude:"",
                'longitude' => ($partnerinfo[0]->longitude) ? $partnerinfo[0]->longitude:"",

            ];
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
		}
	}
	public function partnerInfo(Request $request)
    {
		if($request->all())
		{
			$user = auth()->guard('vendor-api')->user();

			$user->company_name = $request->company_name;
			$user->category_id = $request->category_id;
            $user->sub_cat_id = $request->sub_cat_id;
            $user->description = $request->desc;
			$user->openning_time = ($request->openning_time !="") ? date("H:i",strtotime($request->openning_time)) : "00:00:00";
			$user->closing_time = ($request->closing_time !="" ) ? date("H:i",strtotime($request->closing_time)) : "00:00:00";
            if($user->profile_percentage!=100 && $user->profile_percentage == 15){
                $user->profile_percentage = 50;
            }else if($user->profile_percentage!=100 && ($user->profile_percentage > 15 && $user->profile_percentage == 25)){
                $user->profile_percentage = 60;
            }else if($user->profile_percentage!=100 && ($user->profile_percentage > 15 && $user->profile_percentage == 50)){
                $user->profile_percentage = 50;
            }else if($user->profile_percentage!=100 && ($user->profile_percentage > 15 && $user->profile_percentage == 55)){
                $user->profile_percentage = 90;
            }else if($user->profile_percentage!=100 && ($user->profile_percentage > 25 && $user->profile_percentage == 60)){
                $user->profile_percentage = 60;
            }else if($user->profile_percentage!=100 && ($user->profile_percentage > 25 && $user->profile_percentage < 65)){
                $user->profile_percentage = 55;
            }else if($user->profile_percentage!=100 && ($user->profile_percentage == 90)){
                $user->profile_percentage = 90;
            }else if($user->profile_percentage!=100 && ($user->profile_percentage > 65)){
                $user->profile_percentage = 100;
            }
			$user->save();

			$data['user'] = Partner::with(['getcategory'=> function($q)
			{
				// Query the is_active field in getStates table
				$q->select(array('id', 'category')); // '=' is optional
			},
			'getSubCategory'=> function($q)
			{
				// Query the is_active field in getStates table
				$q->select(array('id', 'category')); // '=' is optional
			}

			])->whereId(auth()->guard('vendor-api')->user()->id)->get();
            $gallery = PartnerGallery::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get(['id','image']);
			if(!$gallery->isEmpty()){
               foreach($gallery as $gal){
                   $gallerydata[]=[
                        'id' => (string)$gal->id,
                        'image'=>asset('public/storage/upload/partnergallery').'/'.$gal->image
                   ];
               }
            }else{
                $gallerydata=[];
            }

          // print_r($user);exit;
            $data = [
                'id'=>$user->id,
                'company_name'=>($user->company_name) ? $user->company_name : "",
                'profile_percentage'=>($user->profile_percentage) ? (string)$user->profile_percentage : "",
                'description' =>($user->description) ? (string)$user->description : "",
                'category_id'=>($user->category_id) ? (string)$user->category_id : "",
                'category_name'=>($user->category_id) ? $user->getcategory->category : "",
                'sub_cat_id'  => ($user->sub_cat_id) ? (string)$user->sub_cat_id : "",
                'sub_cat_name' => ($user->sub_cat_id) ? $user->getSubCategory->category:"",
                'openning_time' => ($user->openning_time !="00:00:00") ? date("h:i A",strtotime($user->openning_time)) : "",
                'closing_time' => ($user->closing_time !="00:00:00") ? date("h:i A",strtotime($user->closing_time)) : "",
                'profile_image' => ($user->image) ? asset('public/storage/upload/partnerimage').'/'.$user->image : "",
                'more_image'=>$gallerydata

            ];
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);

		}
		else
		{
			$partnerinfo = Partner::with(['getcategory'=> function($q)
			{
				// Query the is_active field in getStates table
				$q->select(array('id', 'category')); // '=' is optional
			},
			'getSubCategory'=> function($q)
			{
				// Query the is_active field in getStates table
				$q->select(array('id', 'category')); // '=' is optional
			}

			])->whereId(auth()->guard('vendor-api')->user()->id)->get();
            $gallery = PartnerGallery::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get(['id','image']);
			if(!$gallery->isEmpty()){
               foreach($gallery as $gal){
                   $gallerydata[]=[
                        'id' => (string)$gal->id,
                        'image'=>asset('public/storage/upload/partnergallery').'/'.$gal->image
                   ];
               }
            }else{
                $gallerydata=[];
            }


            $data = [
                'id'=>$partnerinfo[0]->id,
                'company_name'=>($partnerinfo[0]->company_name) ? $partnerinfo[0]->company_name : "",
                'profile_percentage'=>($partnerinfo[0]->profile_percentage) ? (string)$partnerinfo[0]->profile_percentage : "",
                'description' =>($partnerinfo[0]->description) ? (string)$partnerinfo[0]->description : "",
                'category_id'=>($partnerinfo[0]->category_id) ? (string)$partnerinfo[0]->category_id : "",
                'category_name'=>($partnerinfo[0]->category_id) ? $partnerinfo[0]->getcategory->category : "",
                'sub_cat_id'  => ($partnerinfo[0]->sub_cat_id) ? (string)$partnerinfo[0]->sub_cat_id : "",
                'sub_cat_name' => ($partnerinfo[0]->sub_cat_id) ? $partnerinfo[0]->getSubCategory->category:"",
                'openning_time' => ($partnerinfo[0]->openning_time !="00:00:00") ? date("h:i A",strtotime($partnerinfo[0]->openning_time)) : "",
                'closing_time' => ($partnerinfo[0]->closing_time !="00:00:00") ? date("h:i A",strtotime($partnerinfo[0]->closing_time)) : "",
                'profile_image' => ($partnerinfo[0]->image) ? asset('public/storage/upload/partnerimage').'/'.$partnerinfo[0]->image : "",
                'more_image'=>$gallerydata

            ];
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
		}

	}
	public function Profileimageupdate(Request $request)
    {
		if($request->all())
		{
			$user = auth()->guard('vendor-api')->user();
			$image_string = $request->image_string;
			if($request->has('image_string'))
			{

				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partnerimage';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$user->image = $filename;
				if($user->profile_percentage!=100 && $user->profile_percentage == 15){
                    $user->profile_percentage = 25;
                }else if($user->profile_percentage!=100 && ($user->profile_percentage > 15 && $user->profile_percentage == 50)){
                    $user->profile_percentage = 60;
                }else if($user->profile_percentage!=100 && ($user->profile_percentage > 50 && $user->profile_percentage == 55)){
                    $user->profile_percentage = 65;
                }else if($user->profile_percentage!=100 && ($user->profile_percentage > 50 && $user->profile_percentage == 90)){
                    $user->profile_percentage = 100;
                }
                $user->save();
				$userdata = Partner::whereId(auth()->guard('vendor-api')->user()->id)->get();
              // print_r($userdata);
                $data['user'] = [
                    'id'=>$userdata[0]->id,
                    'company_name'=>$userdata[0]->company_name,
                    'email'=>$userdata[0]->email,
                    'mobile_number'=>$userdata[0]->mobile_number,
                    'category_id'=>($userdata[0]->category_id) ? $userdata[0]->category_id : "",
                    'sub_cat_id'=>($userdata[0]->sub_cat_id) ? $userdata[0]->sub_cat_id :"",
                    'address'=>($userdata[0]->address) ? $userdata[0]->address : "",
                    'state_id'=>($userdata[0]->state) ? $userdata[0]->state : "",
                    'city_id'=>($userdata[0]->city) ? $userdata[0]->city : "",
                    'openning_time'=>($userdata[0]->openning_time) ? $userdata[0]->openning_time : "",
                    'closing_time'=> ($userdata[0]->closing_time) ? $userdata[0]->closing_time : "",
                    'subscription_id'=>($userdata[0]->subscription_id)? $userdata[0]->subscription_id : "",
                    'profile_percentage'=>($userdata[0]->profile_percentage)? $userdata[0]->profile_percentage : "",
                    'facebook'=>($userdata[0]->facebook)? $userdata[0]->facebook : "",
                    'twitter'=>($userdata[0]->twitter)? $userdata[0]->twitter : "",
                    'linkedin'=>($userdata[0]->linkedin)? $userdata[0]->linkedin : "",
                    'instagram'=>($userdata[0]->instagram)? $userdata[0]->instagram : "",
                    'web_link'=>($userdata[0]->web_link)? $userdata[0]->web_link : "",
                    'image' => ($userdata[0]->image)? asset('public/storage/upload/partnerimage/').'/'.$userdata[0]->image : "",
                ];
				return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
			}
			else
				return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}
		else
		{
			$userdata = Partner::whereId(auth()->guard('vendor-api')->user()->id)->get();
             $data['user'] = [
                    'id'=>$userdata[0]->id,
                    'company_name'=>$userdata[0]->company_name,
                    'email'=>$userdata[0]->email,
                    'mobile_number'=>$userdata[0]->mobile_number,
                    'category_id'=>($userdata[0]->category_id) ? $userdata[0]->category_id : "",
                    'sub_cat_id'=>($userdata[0]->sub_cat_id) ? $userdata[0]->sub_cat_id :"",
                    'address'=>($userdata[0]->address) ? $userdata[0]->address : "",
                    'state_id'=>($userdata[0]->state) ? $userdata[0]->state : "",
                    'city_id'=>($userdata[0]->city) ? $userdata[0]->city : "",
                    'openning_time'=>($userdata[0]->openning_time) ? $userdata[0]->openning_time : "",
                    'closing_time'=> ($userdata[0]->closing_time) ? $userdata[0]->closing_time : "",
                    'subscription_id'=>($userdata[0]->subscription_id)? $userdata[0]->subscription_id : "",
                    'profile_percentage'=>($userdata[0]->profile_percentage)? $userdata[0]->profile_percentage : "",
                    'facebook'=>($userdata[0]->facebook)? $userdata[0]->facebook : "",
                    'twitter'=>($userdata[0]->twitter)? $userdata[0]->twitter : "",
                    'linkedin'=>($userdata[0]->linkedin)? $userdata[0]->linkedin : "",
                    'instagram'=>($userdata[0]->instagram)? $userdata[0]->instagram : "",
                    'web_link'=>($userdata[0]->web_link)? $userdata[0]->web_link : "",
                    'image' => ($userdata[0]->image)? asset('public/storage/upload/partnerimage/').'/'.$userdata[0]->image : "",
                ];
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
		}
    }
    public function uploadMoreImage(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'image_string' => 'required',
			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}

			$image_string = $request->image_string;
			$gallery = new PartnerGallery;
			if($request->has('image_string'))
			{
				$order = uniqid	();
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/partnergallery';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$gallery->image = $filename;
				//$gallery->name = $request->name;
			    $gallery->partner_id = auth()->guard('vendor-api')->user()->id;
			    $gallery->save();

				$gallery = PartnerGallery::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

			}


    }

    public function purchase_subscription(Request $request){
        if(!$request->subscription_id)
		{
            return response()->json(['result' => 0,'message' => "Please pass all parameters",], 200);
        }else{
        	$subscriptiondetails = Subscription::find($request->subscription_id);
            $userdata = Partner::whereId(auth()->guard('vendor-api')->user()->id)->get();
            $subscription_price = $subscriptiondetails->price;
            $subscription_name = $subscriptiondetails->name;
            $subscription_duration = $subscriptiondetails->duration;
            $wallet_balance = $userdata[0]->wallet_balance;
            $user_id = $userdata[0]->id;
            $purchasedata = PurchaseSubscription::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();

                if($wallet_balance >= $subscription_price){
                    $result = Partner::where('id','=',$user_id)->update(['subscription_id' => $request->subscription_id,'subscription_expire_on'=>Carbon::today()->addMonths($subscription_duration)]);
                    if($result){
                        $result1 = Partner::where('id','=',$user_id)->decrement('wallet_balance',$subscription_price);
                        if($result1){
                            $wallet_trans = new WalletTransaction;
                            $wallet_trans->partner_id = $user_id;
                            $wallet_trans->amount = $subscription_price;
                            $wallet_trans->trans_type = 'db';
                            $wallet_trans->remark = 'Wallet debited for subscription purchased';
                            $wallet_trans->created_at = Carbon::now();
                            $wallet_trans->save();
                        }
                        $purchase_subs = new PurchaseSubscription;
                        $purchase_subs->partner_id = $user_id;
                        $purchase_subs->subscription_id = $request->subscription_id;
                        $purchase_subs->price_paid = $subscription_price;
                        $purchase_subs->created_at = Carbon::now();
                        $purchase_subs->expire_on = $purchase_subs->created_at->addMonths($subscription_duration);
                        $purchase_subs->save();

                    }
                    return response()->json(['result' => 1,'message' => "Congrats! You have successfully purchased ".$subscription_name." plan",], 200);
                }else{
                    return response()->json(['result' => 0,'message' => "You don't have sufficient wallet balance",], 200);
                }

        }

    }
    public function my_subscribed_plan(){
        $purchasedata = PurchaseSubscription::wherePartnerId(auth()->guard('vendor-api')->user()->id)->Orderby('id','DESC')->limit(1)->get();
        if(!$purchasedata->isEmpty()){
            $data = [
                'id'=>$purchasedata[0]->id,
                'subscription_id'=>$purchasedata[0]->subscription_id,
                'subscription_name'=>$purchasedata[0]->getSubscription->name,
                'image' => ($purchasedata[0]->getSubscription->image) ? asset('public/storage/upload/subscription').'/'.$purchasedata[0]->getSubscription->image : "",
                'active_from'=>date("d M, Y",strtotime($purchasedata[0]->created_at)),
                'expire_on' => date("d M, Y",strtotime($purchasedata[0]->expire_on)),

            ];
             return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
        }else{
             return response()->json(['result' => 0,'message' => "Not Found",], 200);
        }
    }

    public function partnerWallet(){
        $user = auth()->guard('vendor-api')->user();
        $wallet_balance = $user->wallet_balance;
        $transactiondata = WalletTransaction::wherePartnerId(auth()->guard('vendor-api')->user()->id)->Orderby('id','DESC')->get();
        if(!$transactiondata->isEmpty()){
           return response()->json(['result' => 1,'message' => 'Success','current_balance'=>$wallet_balance, 'data' =>$transactiondata->toArray()], 200);
        }else{
             return response()->json(['result' => 1,'message' => "Success",'current_balance'=>$wallet_balance,'data'=>[]], 200);
        }
    }

    public function bookAvailability(Request $request){
        if($request->type=="add")
		{
			$validator = Validator::make($request->all(), [
            'client_name' => 'required',
			'booking_date' => 'required',
			]);

			if ($validator->fails()) {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->client_name[0])){
                    $err = $errMsg->client_name[0];
                }else if(isset($errMsg->booking_date[0])){
                     $err = $errMsg->booking_date[0];
                }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }
            $bookeddata = BookAvailability::wherePartnerId(auth()->guard('vendor-api')->user()->id)->where('booking_date', '=', $request->booking_date)->get();
            if(!$bookeddata->isEmpty()){
                return response()->json(['result' => 0,'message' => 'Given date has already booked'], 200);
            }else{
                $booked = new BookAvailability;
                $booked->client_name = $request->client_name;
				$booked->booking_date = $request->booking_date;
				$booked->status = 1;
			    $booked->partner_id = auth()->guard('vendor-api')->user()->id;
			    $booked->save();

				$gallery = BookAvailability::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
				if(!$gallery->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $gallery->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
            }


		}
		elseif($request->type=="edit")
		{
			$bookeddata=BookAvailability::find($request->id);


            $validator = Validator::make($request->all(), [
                'client_name' => 'required',
                'booking_date' => 'required',
                ]);

                if ($validator->fails()) {
                    $errMsg = json_decode($validator->messages());
                    if(isset($errMsg->client_name[0])){
                        $err = $errMsg->client_name[0];
                    }else if(isset($errMsg->booking_date[0])){
                         $err = $errMsg->booking_date[0];
                    }else {
                        $err = "Please pass required parameters";
                    }
                    return response()->json(['result' => 0,'message' => $err], 200);
                }

                $bookeddatadup = BookAvailability::wherePartnerId(auth()->guard('vendor-api')->user()->id)->where('id', '!=' ,$request->id)->where('booking_date', '=', $request->booking_date)->get();
                if(!$bookeddatadup->isEmpty()){
                    return response()->json(['result' => 0,'message' => 'Given date has already booked'], 200);
                }else{

                    $bookeddata->client_name = $request->client_name;
                    $bookeddata->booking_date = $request->booking_date;
                    $bookeddata->status = 1;
                    $bookeddata->partner_id = auth()->guard('vendor-api')->user()->id;
                    $bookeddata->save();

                    $bookeddata = BookAvailability::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
                    if(!$bookeddata->isEmpty())
                    return response()->json([
                        'result' => 1,
                        'message' => "Success",
                        'data' => $bookeddata->toArray()
                    ],200);
                    else
                    return response()->json(['result' => 0,'message' => "Not Found",], 200);
            }



		}

		elseif($request->type=="delete")
		{
			$gallery = BookAvailability::find($request->id);
			$gallery->delete();

			$gallery = BookAvailability::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		else
		{
			$gallery = BookAvailability::wherePartnerId(auth()->guard('vendor-api')->user()->id)->get();
			if(!$gallery->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $gallery->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}

    }

    public function partnerStatus(){
        $partnerinfo = Partner::whereId(auth()->guard('vendor-api')->user()->id)->get();
        if(!$partnerinfo->isEmpty()){
            $data = [
                'profile_percentage' => (string)$partnerinfo[0]->profile_percentage,
                'status' => $partnerinfo[0]->status
            ];
            return response()->json(['result' => 1,'message' => 'Success', 'data'=> $data], 200);
        }else{
            return response()->json(['result' => 0,'message' => 'No data found'], 200);
        }
    }

    public function analytics_report(Request $request){

            $start_date = Carbon::today()->subDays(7);
            $end_date = Carbon::today()->subDays(1);
            $transactiondataSevenday = Analytic::wherePartnerId(auth()->guard('vendor-api')->user()->id)->whereBetween('created_at', [$start_date->format('Y-m-d')." 00:00:00", $end_date->format('Y-m-d')." 23:59:59"])->Orderby('id','DESC')->get();
            $transactiondata = Analytic::wherePartnerId(auth()->guard('vendor-api')->user()->id)->Orderby('id','DESC')->get();
            if(!$transactiondata->isEmpty()){
                $listData1 = 0;
                $listData2 = 0;
                $listData3 = 0;
                foreach($transactiondata as $trans){
                    if($trans->page=='Offer'){
                        $listData1 = $listData1+1;
                    }
                    if($trans->page=='Whatsapp'){
                        $listData2 = $listData2+1;
                    }
                    if($trans->page=='Partner Details'){
                        $listData3 = $listData3+1;
                    }
                }
                $monday = 0;
                $tuesday = 0;
                $wednesday = 0;
                $thursday = 0;
                $friday = 0;
                $saturday = 0;
                $sunday = 0;
                foreach($transactiondata as $trans){
                    if($trans->created_at->format('D')=='Mon'){
                        $monday = $monday+1;
                    }
                    if($trans->created_at->format('D')=='Tue'){
                        $tuesday = $tuesday+1;
                    }
                    if($trans->created_at->format('D')=='Wed'){
                        $wednesday = $wednesday+1;
                    }

                    if($trans->created_at->format('D')=='Thu'){
                        $thursday = $thursday+1;
                    }
                    if($trans->created_at->format('D')=='Fri'){
                        $friday = $friday+1;
                    }
                    if($trans->created_at->format('D')=='Sat'){
                        $saturday = $saturday+1;
                    }
                    if($trans->created_at->format('D')=='Sun'){
                        $sunday = $sunday+1;
                    }
                }
                return response()->json(['result' => 1,'message' => 'Success','Offer' =>$listData1,'Whatsapp'=>$listData2,'PartnerDetails'=>$listData3, 'Monday' =>$monday,'Tuesday'=>$tuesday,'Wednesday'=>$wednesday,'Thursday' =>$thursday,'Friday'=>$friday,'Saturday'=>$saturday,'Sunday'=>$sunday], 200);
            }else{
                return response()->json(['result' => 0,'message' => "Not Found",], 200);
            }
    }


}
