<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Mail\MailEmail;
use Illuminate\Support\Facades\Mail;

class PassportController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

	public function register(Request $request)
    {
		/* $test = $this->validate($request, [
            'name' => 'required|min:3|max:50',
			'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ],["mobile_number.regex"=>"The mobile number must be start with 9 or 8 or 7 or 6"]); */



		$validator = Validator::make($request->all(), [
            'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"|unique:users',
			 ],["mobile_number.regex"=>"The mobile number must be start with 9 or 8 or 7 or 6"]);

		if ($validator->fails())
		{
			$errMsg = json_decode($validator->messages());
            if(isset($errMsg->mobile_number[0])){
                $err = $errMsg->mobile_number[0];
            }else {
                $err = "Please pass required parameters";
            }
			return response()->json(['result' => 0,'message' => $err], 200);
		}


        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'mobile_number' => $request->mobile_number,
            'fcm_token' => $request->fcm_token,
            'password' => bcrypt($request->password)
        ]);

		$token = $user->createToken('Eartiauser')->accessToken;
		if($token)
		{
			$data['user'] = $user;
			$data['token']= $token;
            return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
		}
		else
			 return response()->json(['result' => 0,'message' => 'Something Error'], 200);

    }


	public function login(Request $request)
    {
        $credentials = [
            'mobile_number' => $request->mobile_number
        ];
        if($request->has('mobile_number') &&  $request->mobile_number != ""){
            $user=User::where('mobile_number',$request->mobile_number)->first();
            if ($user) {
                $usersave = Auth::loginUsingId($user->id);
                $token = auth()->user()->createToken('Eartiauser')->accessToken;
                $user_data['token']=$token;
                $data = auth()->user();
                $usersave->fcm_token = $request->fcm_token;
                $usersave->save();

                $user_data['user']=[
                            "id"=> $data->id,
                            "mobile_number"=>$data->mobile_number,
                            "user_type" => "customer"

                        ];
                 return response()->json(['result' => 1,'message' => 'Success', 'data' =>$user_data], 200);


            } else {
                $invtcode = rand(1000,9999);
                $user = User::create([
                    'mobile_number' => $request->mobile_number,
                    'password' => '',
                    'provier_id' =>($request->login_type=='facebook') ? $request->uid:"",
                    'gmail_provider_id' =>($request->login_type=='google') ? $request->uid:"",
                    'provider' => ($request->login_type=='facebook') ? 'facebook':"",
                    'gmail_provider' => ($request->login_type=='google') ? 'google':"",
                    'fcm_token' => ($request->fcm_token) ? $request->fcm_token : ""
                ]);
                Auth::loginUsingId($user->id);
                $token = auth()->user()->createToken('Eartiapartner')->accessToken;
                $user_data['token']=$token;
                $data['user'] = auth()->user();
                $image = $data['user']->profile_image;
                $user_data['user']=[
                            "id"=>$data['user']->id,
                            "mobile_number"=>$data['user']->mobile_number,
                            "user_type" => "customer"

                        ];
                return response()->json(['result' => 1,'message' => 'Success', 'data' =>$user_data], 200);
            }
        }else{
            return response()->json(['result' => 0,'message' => 'Mobile number is required'], 200);
        }


    }







}
