<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use App\User;
use App\CustomerEventBook;
use App\PartnerEvent;
use App\CustomerMenuTran;
use App\PartnerEventPrice;
use App\PartnerMenu;
use App\Partner;
use App\Chat;
use Illuminate\Http\Request;
use Arr;
use Event;
use File;
use Illuminate\Validation\Rule;

class CustomerController extends Controller
{
    public function customerdetails()
    {
        $userdata = auth()->user();
        $data['id'] = $userdata->id;
        $data['name'] = ($userdata->name) ? $userdata->name : "";
        $data['email'] = ($userdata->email) ? $userdata->email : "";
        $data['mobile'] = ($userdata->mobile_number) ? $userdata->mobile_number : "";
        $data['address'] = ($userdata->address) ? $userdata->address : "";
        $data['image'] = ($userdata->profile_image) ? asset('public/storage/upload/profile').'/'.$userdata->profile_image : "";
        return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
    }
    public function update_profile_image(Request $request){
        //print_r($request->all());exit;
        if($request->all())
		{
            $validator = Validator::make($request->all(), [
				'image_string' => 'required']);

                if ($validator->fails()) {
                    $errMsg = json_decode($validator->messages());
                    if(isset($errMsg->image_string[0])){
                        $err = $errMsg->image_string[0];
                    }else {
                        $err = "Please pass required parameters";
                    }
                    return response()->json(['result' => 0,'message' => $err], 200);
                }
			$userdata = auth()->user();
			$image_string = $request->image_string;
			if($request->has('image_string'))
			{
				$order = uniqid	();
				if (strpos($image_string, 'data:image/jpeg;base64,') !== false) {
					$check = 1;
				}else if(strpos($image_string, 'data:image/png;base64,') !== false){
					$check = 2;
				}else{
					$check = 0;
				}
				if($check ==1){
					$image_string = str_replace('data:image/jpeg;base64,', '', $image_string);
				}else if($check==2){
					$image_string = str_replace('data:image/png;base64,', '', $image_string);
				}
				$imgdata = str_replace(' ', '+', $image_string);
				$imgdata1 = base64_decode($imgdata);
				$filename = $order.'.PNG';
				$url = public_path(). '/storage/upload/profile';
				$filetosave = $url.'/'.$filename;
				File::put($url.'/' . $filename, $imgdata1);
				$userdata->profile_image = $filename;
                $userdata->save();

                $data['id'] = $userdata->id;
                $data['name'] = ($userdata->name) ? $userdata->name : "";
                $data['email'] = ($userdata->email) ? $userdata->email : "";
                $data['mobile'] = ($userdata->mobile_number) ? $userdata->mobile_number : "";
                $data['address'] = ($userdata->address) ? $userdata->address : "";
                $data['image'] = ($userdata->profile_image) ? asset('public/storage/upload/profile').'/'.$userdata->profile_image : "";
				return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
			}
			else
				return response()->json(['result' => 0,'message' => "Please pass image string",], 200);
		}
		else
		{
			$userdata = auth()->user();
            $data['id'] = $userdata->id;
            $data['name'] = ($userdata->name) ? $userdata->name : "";
            $data['email'] = ($userdata->email) ? $userdata->email : "";
            $data['mobile'] = ($userdata->mobile_number) ? $userdata->mobile_number : "";
            $data['address'] = ($userdata->address) ? $userdata->address : "";
            $data['image'] = ($userdata->profile_image) ? asset('public/storage/upload/profile').'/'.$userdata->profile_image : "";
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
		}

    }
    public function update_profile(Request $request){
        if($request->all())
		{
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:50',
                'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"|'.Rule::unique('users')->ignore(auth()->user()->id),
                'email' => 'required|email|'.Rule::unique('users')->ignore(auth()->user()->id)
            ],["mobile_number.regex"=>"The mobile number must be start with 9 or 8 or 7 or 6"]);

            if ($validator->fails())
            {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->name[0])){
                    $err = $errMsg->name[0];
                }else if(isset($errMsg->mobile_number[0])){
                    $err = $errMsg->mobile_number[0];
                }else if(isset($errMsg->email[0])){
                    $err = $errMsg->email[0];
                }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }
            $userdata = auth()->user();
			$userdata->email = $request->email;
            $userdata->address =  $request->address;
            $userdata->mobile_number =  $request->mobile_number;
            $userdata->name =  $request->name;

            $userdata->save();


                $data['id'] = $userdata->id;
                $data['name'] = ($userdata->name) ? $userdata->name : "";
                $data['email'] = ($userdata->email) ? $userdata->email : "";
                $data['mobile'] = ($userdata->mobile_number) ? $userdata->mobile_number : "";
                $data['address'] = ($userdata->address) ? $userdata->address : "";
                $data['image'] = ($userdata->profile_image) ? asset('public/storage/upload/profile').'/'.$userdata->profile_image : "";
				return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);

		}
		else
		{
			$userdata = auth()->user();
            $data['id'] = $userdata->id;
            $data['name'] = ($userdata->name) ? $userdata->name : "";
            $data['email'] = ($userdata->email) ? $userdata->email : "";
            $data['mobile'] = ($userdata->mobile_number) ? $userdata->mobile_number : "";
            $data['address'] = ($userdata->address) ? $userdata->address : "";
            $data['image'] = ($userdata->profile_image) ? asset('public/storage/upload/profile').'/'.$userdata->profile_image : "";
			return response()->json(['result' => 1,'message' => 'Success', 'data' =>$data], 200);
		}

    }

	public function customerlogout() {
        $accessToken = auth()->user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);
        $accessToken->revoke();
        return response()->json(array('result' => 1,'message' => 'Successfully logged out'), 200);
    }


	public function FoodBooking(Request $request)
    {
		$details = CustomerMenuTran::with('getMenu')->whereUserId(auth()->user()->id)->orderBy('created_at','desc')->get();
		if($request->all())
		{
			$eventdetails = PartnerMenu::find($request->menu_id);
			$parnterdetails = Partner::find($request->partner_id);

			$book = new CustomerMenuTran;
			$book->user_id  = auth()->user()->id;
			$book->menu_id  =$request->menu_id;
			$book->date  = $request->date;
			$book->time  = $request->time;
			$book->first_name  =$request->first_name;
			$book->last_name   = $request->last_name;
			$book->email  = $request->email;
			$book->mobile_no = $request->mobile_no;
			$book->noofguest  = $request->noofguest;
			$book->save();

			Event::dispatch('event.menucontact',array($request->all(),$parnterdetails->email,$parnterdetails->company_name,$eventdetails->name));

			$details = CustomerMenuTran::with('getMenu')->whereUserId(auth()->user()->id)->orderBy('created_at','desc')->get();

				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);


		}
		else
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				return response()->json(['result' => 0,'message' => "Not Found",], 200);


	}

	public function ReviewList(Request $request)
    {
		$details = \App\CustomerPartnerReview::with(array('getUser:id,profile_image'))->wherePartnerId($request->partner_id)->orderBy('created_at','desc')->get();
        if(!$details->isEmpty()){
                foreach($details as $detail){
                    $data[] = [
                        'id'=> (string)$detail->id,
                        'rating'=> ($detail->rating) ? $detail->rating:"",
                        'name'=> ($detail->name) ? $detail->name:"",
                        'partner_id'=> ($detail->partner_id) ? $detail->partner_id:"",
                        'email'=>($detail->email) ? $detail->email : "",
                        'message'=>($detail->message) ? (string)$detail->message:"",
                        'created_at'=>($detail->created_at) ? $detail->created_at:"",
                        'updated_at'  => ($detail->updated_at) ? (string)$detail->updated_at:"",
                       // 'imagepath' => ($detail->partner_id) ? $detail->getCity->city_name : "",
                        'partner_image' => ($detail->partner_id)? asset('public/storage/upload/profile/').'/'.$detail->getUser->profile_image : "",

                    ];
                }
				return response()->json([
					'result' => 1,
                    'message' => "Success",
                    'data' => $data
					//'data' => $details->makeHidden(['partner_id','user_id','status','admin_status'])->toArray()
				],200);
        }else{
                return response()->json(['result' => 0,'message' => "Not Found",], 200);
        }
	}

	public function ReviewRating(Request $request)
    {
		if($request->all())
		{
		$review = new \App\CustomerPartnerReview;
		$review->partner_id  = $request->partner_id;
		$review->user_id = auth()->user()->id ;
		$review->email = auth()->user()->email ;
		$review->name = auth()->user()->name ;
		$review->message = $request->message;
		$review->rating = $request->rating;
		$review->save();

		$details = \App\CustomerPartnerReview::wherePartnerId($request->partner_id)->orderBy('created_at','desc')->get();
		if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}
		else
			return response()->json(['result' => 0,'message' => "Not Found",], 200);

	}
	public function EventBooking(Request $request)
    {
		$details = CustomerEventBook::with(['getEvent'])->whereUserId(auth()->user()->id)->orderBy('created_at','desc')->get();
		if($request->event_id >0 )
		{
			$eventpricedetials = PartnerEventPrice::find($request->event_price_id);
			$eventdetails = PartnerEvent::find($request->event_id);

			if($eventdetails->end_date >=date('Y-m-d') && $eventdetails->participants >0 && $eventdetails->participants >= $request->noofguest  && auth()->user()->id >0  && auth()->user()->wallet_balance >= $eventdetails->price *$request->noofguest)
			{
				for($i=0;$i<$request->noofguest;$i++)
				{
					$book = new CustomerEventBook;
					$book->user_id  = auth()->user()->id;
					$book->event_id  = $request->event_id;
					$book->event_price_id  = $request->event_price_id;
					$book->booking_price  = $eventpricedetials->price;
					$book->start_date  = $eventdetails->start_date;
					$book->end_date  = $eventdetails->end_date;
					$book->user_name = auth()->user()->name;
					$book->user_email = auth()->user()->email;
					$book->user_mobile = auth()->user()->mobile_number;
					$book->save();
					PartnerEvent::find($request->event_id)->decrement('participants');
					User::find(auth()->user()->id)->decrement('wallet_balance',$eventdetails->price);

					Event::dispatch('event.eventcustomer',$book);
					Event::dispatch('event.eventvendor',$book);
				}


				$details = CustomerEventBook::with(['getEvent'])->whereUserId(auth()->user()->id)->orderBy('created_at','desc')->get();

				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->makeHidden(['user_id'])->toArray()
				],200);

			}
			else
				return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}
		else
		{
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->makeHidden(['user_id'])->toArray()
				],200);
			else
				return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}

    }

    public function manageChat(Request $request){
        if($request->type=="add")
		{
			$validator = Validator::make($request->all(), [
            'message' => 'required'
			]);

			if($validator->fails()){

			return response()->json(['result' => 0,'message' => 'Message is required'], 200);
			}
            $chat = new Chat;


				$chat->message = $request->message;
			    $chat->user_id = auth()->user()->id;
			    $chat->save();

				$Chat = Chat::whereUserId(auth()->user()->id)->get();
				if(!$Chat->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $Chat->toArray()
				],200);
				else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}

		else
		{
			$chat = Chat::whereUserId( auth()->user()->id)->get();
			if(!$chat->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $chat->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
    }

    public function updateChatStatus(Request $request){
        $Chat = Chat::whereUserId(auth()->user()->id)->whereIsRead('0')->whereSenderType('Admin')->update(['is_read' => 1]);

        $chatdata= Chat::whereUserId( auth()->user()->id)->get();
		if(!$chatdata->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $chatdata->toArray()
			],200);
			else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

    }




}
