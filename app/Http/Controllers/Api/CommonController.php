<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use DB;
use App\Country;
use App\State;
use App\City;
use App\Area;
use App\Page;
use App\SubCategory;
use App\PartnerEvent;
use App\PartnerLead;
use App\PartnerCourse;
use App\PartnerProduct;
use App\PartnerGallery;
use App\PartnerAdvertisement;
use App\ManageOffer;
use App\PartnerMenu;
use App\Partner;
use App\Category;
use App\Tag;
use App\Organizaton;
use App\Subscription;
use App\BookAvailability;
use App\Analytic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Event;
use Carbon\Carbon;
class CommonController extends Controller
{
	public function getcity()
    {
			$details = City::whereIsActive(1)->orderBy('city_name','asc')->get();
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
	}
	public function getcountry()
    {
			$details = Country::whereStatus(1)->orderBy('name','asc')->get();
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
	}

	public function getorganization()
    {
			$details = Organizaton::whereStatus(1)->orderBy('organizaton','asc')->get();
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
	}

	public function getstate()
    {
			$details = State::whereIsActive(1)->orderBy('state','asc')->get();
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
	}

	public function getarea()
    {
			$details = Area::whereStatus(1)->orderBy('locality','asc')->get();
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
	}

	public function getcategory()
    {
			$details = Category::whereStatus(1)->orderBy('display_order','asc')->get();
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
	}

	public function getsubcategory()
    {
			$details = SubCategory::whereStatus(1)->orderBy('display_order','asc')->get();
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
	}

	public function gettags()
    {
			$details = Tag::whereStatus(1)->orderBy('tags','asc')->get();
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
	}

	public function getsubscriiption()
    {
			$details = Subscription::whereStatus(1)->orderBy('name','asc')->get();
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);
	}

	public function categorysubcategorylist($category_id= null)
    {
		//For all Country List
		if(!$category_id)
		{
			$details = Category::whereStatus(1)->orderBy('display_order','asc')->get(['id','category','image','icon']);
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}
		//For all subcategory list as per category id
		else
		{
			$details = SubCategory::whereStatus(1)->whereMainCategory($category_id)->orderBy('display_order','asc')->get(['id','category','image','icon']);
            if(!$details->isEmpty()){
                $category = Category::find($category_id);
				return response()->json([
					'result' => 1,
                    'message' => "Success",
                    'category_name' => $category->category,
					'data' => $details->toArray()
				],200);
            }else{
                 return response()->json(['result' => 0,'message' => "Not Found",], 200);
            }
		}


	}

	public function CountrySateCityArealist($state_id= null)
    {
		//For all Country List
        $country_id =1;
		if(!$state_id)
		{
			$details = State::whereIsActive(1)->whereCountryId($country_id)->orderBy('state','asc')->get(['state_id','state']);
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}

		//For all city list as per country id and state id
		else if($state_id )
		{

			$details = City::whereIsActive(1)->whereStateId($state_id)->orderBy('city_name','asc')->get(['city_id','city_name']);
			if(!$details->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $details->toArray()
				],200);
			else
				 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}

		else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);


	}


	public function PartnerMenuFoodList(Request $request)
    {
		$newslist = PartnerMenu::whereStatus(1)->wherePartnerId($request->partner_id)->orderBy('created_at', 'DESC')->get();
		if(!$newslist->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $newslist->toArray()
				],200);
		else
			return response()->json(['result' => 0,'message' => "Not Found",], 200);

	}

	public function PartnerJobsList(Request $request)
    {
		$newslist = \App\PartnerJob::whereStatus(1)->wherePartnerId($request->partner_id)->orderBy('created_at', 'DESC')->get();
		if(!$newslist->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $newslist->toArray()
				],200);
		else
			return response()->json(['result' => 0,'message' => "Not Found",], 200);

	}

	public function PartnerPhotoList(Request $request)
    {
		$newslist = \App\PartnerGallery::whereStatus(1)->wherePartnerId($request->partner_id)->orderBy('created_at', 'DESC')->get();
		if(!$newslist->isEmpty())
				return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $newslist->toArray()
				],200);
		else
			return response()->json(['result' => 0,'message' => "Not Found",], 200);

	}


	public function EventList(Request $request,$id=0)
    {

		if($request->type=="details")
		{

			$newslist = PartnerEvent::whereStatus(1)->whereAdminStatus(1)->whereId($request->event_id)->orderBy('created_at', 'DESC')->get();


			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);


		}
		elseif($request->type=="list")
		{
			$newslist = PartnerEvent::whereStatus(1)->whereAdminStatus(1)->orderBy('created_at', 'DESC')->get();


			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}
		else
		{
			$newslist = PartnerEvent::whereHas('getPartner', function($query) use($id)
			{
					return $query->where('category_id', '=',$id);

			})->whereStatus(1)->whereAdminStatus(1)->orderBy('created_at', 'DESC')->get();


			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}


	}


	public function CommonPages($page_id= null)
    {
		if($page_id)
			$pages=Page::whereId($page_id)->get(array('id','title','content'));
		else
			$pages=Page::whereStatus(1)->orderBy('title', 'asc')->get(array('id','title','content'));


		if(!$pages->isEmpty())
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $pages->toArray()
			],200);
		else
			 return response()->json(['result' => 0,'message' => "Not Found",], 200);

    }


	public function Contactus(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
			'email' => 'required|email',
            'subject' => 'required|min:3|max:50',
            'message' => 'required|min:10',
            'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"',
        ]);

		if ($validator->fails())
            {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->name[0])){
                    $err = $errMsg->name[0];
                }else if(isset($errMsg->mobile_number[0])){
                    $err = $errMsg->mobile_number[0];
                }else if(isset($errMsg->email[0])){
                    $err = $errMsg->email[0];
                }else if(isset($errMsg->subject[0])){
                    $err = $errMsg->subject[0];
                }else if(isset($errMsg->message[0])){
                    $err = $errMsg->message[0];
                }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }
		else
		{
			  //Event::dispatch('event.contactus',array($request->all()));
			  return response()->json(['result' => 1,'message' => 'Message Sent Successfully'], 200);

		}
	}

	public function VendorLeadgeneration(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'email' => 'required|email',
            'mobile_number' => 'required|numeric|digits:10|regex:"^[9876]\d{9}"',
			'partner_id' => 'required',
			'message' => 'required|min:10',
        ]);

		if ($validator->fails())
            {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->name[0])){
                    $err = $errMsg->name[0];
                }else if(isset($errMsg->mobile_number[0])){
                    $err = $errMsg->mobile_number[0];
                }else if(isset($errMsg->email[0])){
                    $err = $errMsg->email[0];
                }else if(isset($errMsg->message[0])){
                    $err = $errMsg->message[0];
                }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }
		else
		{
			$review = new PartnerLead;
			$review->partner_id  = $request->partner_id;
			$review->email = $request->email;
			$review->name = $request->name;
			$review->mobile_number = $request->mobile_number;
			$review->message = $request->message;
			$review->save();
            return response()->json(['result' => 1,'message' => 'Message Sent Successfully'], 200);
		}
	}


	public function NewsList(Request $request)
    {
		if($request->type=="details")
		{
			$newslist = \App\News::whereStatus(1)->whereId($request->news_id)->get();
			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}
		else
		{
			$newslist= \App\News::whereStatus(1)->orderBy('created_at', 'DESC')->get();
			if(!$newslist->isEmpty())
				return response()->json([
							'result' => 1,
							'message' => "Success",
							'data' => $newslist->toArray()
						],200);
				else
						 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}

	}

	public function PositionList()
    {
        $positiontype123 = array(
            'Home' => 'Home',
            'Category' => 'Category',
            'List' => 'List'
        );
		return response()->json([
					'result' => 1,
					'message' => "Success",
					'data' => $positiontype123
					],200);
	}

	public function TestimonialList(Request $request)
    {
		$newslist= \App\Testimonial::whereStatus(1)->orderBy('created_at', 'DESC')->get(array('id','image','description','title'));
		if(!$newslist->isEmpty())
			return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
			else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);
	}

	public function getCategoryDetails(Request $request)
    {
		$data=array();
		if($request->type=="category")
		{
			$locaitondetails['city'] = $request->city;
			$locaitondetails['regionName'] = $request->regionName;
			$categorydetails= Category::whereStatus(1)
			->whereId($request->category_id)
			->get();

		$query = Partner::whereStatus(1)->whereShowInListing(1)->whereCategoryId($request->category_id);
		if($locaitondetails) {
			$query->where(function ($subquery) use ($locaitondetails){
						$city = City::Where('city_name', 'LIKE', '%' . $locaitondetails['city'] . '%')->first();
						$state = State::Where('state', 'LIKE', '%' . $locaitondetails['regionName'] . '%')->first();
						$subquery->where('city', '=',$city->city_id)->where('state', '=',$state->state_id);
					});
            }
		$topvendors = $query->get(array('id','company_name','image'));


		$topbrands = SubCategory::whereStatus(1)->whereShowInListing(1)->whereMainCategory($request->category_id)->get(array('id','main_category','category','image'));
		$menu = SubCategory::whereStatus(1)->whereShowInListing(0)->whereMainCategory($request->category_id)->get(array('id','main_category','category','image'));

		$topcities = Area::whereHas('getCity', function($query) use($locaitondetails)
		{
			if($locaitondetails['city'])
				return $query->where('is_active', '=',1)->where('city_name', 'LIKE', '%' . $locaitondetails['city'] . '%');

		})->whereStatus(1)->orderBy('locality', 'DESC')->get(array('id','city_id','locality','image'));


		$id= $request->category_id;
		$vendoradvetisement = \App\PartnerAdvertisement::whereHas('getPartner', function($query) use($id)
		{
				return $query->where('category_id', '=',$id);

		})->whereStatus(1)->whereAdminStatus(1)->wherePosition('Category')->where('todate', '>=', date('Y-m-d'))->get();


		$topratedlocation = City::whereIsActive(1)->whereShowInListing(1)->orderBy('city_name', 'asc')->get(array('city_id','city_name','state_id','image','country_id'));

		$data['category']=$categorydetails->toArray();
		$data['topvendors']=$topvendors->toArray();
		$data['topbrands']=$topbrands->makeHidden(['getTags'])->toArray();
		$data['menu']=$menu->makeHidden(['getTags'])->toArray();
		$data['topcities']=$topcities->toArray();
		$data['topratedlocation']=$topratedlocation->toArray();
		$data['vendoradvetisement']=$vendoradvetisement->toArray();

		if($data)
			return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $data
					],200);
			else
			return response()->json(['result' => 0,'message' => "Not Found",], 200);



		}
		elseif($request->type=="sub-category")
		{
			$id= $request->subcategory_id;
			$categorydetails= SubCategory::with(['getTags'=> function($q)
			{
				// Query the is_active field in getStates table
				$q->where('status', '=',1); // '=' is optional
			}])
			->whereStatus(1)
			->whereId($id)
			->get();


			$vendoradvetisement = \App\PartnerAdvertisement::whereHas('getPartner', function($query) use($id)
			{
					return $query->where('sub_cat_id', '=',$id);

			})->whereStatus(1)->whereAdminStatus(1)->wherePosition('Category')->where('todate', '>=', date('Y-m-d'))->get();


			$topratedlocation = City::whereIsActive(1)->whereShowInListing(1)->orderBy('city_name', 'asc')->get();

			$data['subcategory']=$categorydetails->toArray();
		    $data['vendoradvetisement']=$vendoradvetisement->toArray();
			$data['topratedlocation']=$topratedlocation->toArray();

			if($data)
			return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $data
					],200);
			else
			return response()->json(['result' => 0,'message' => "Not Found",], 200);




		}
	}
	public function Locationcity(Request $request)
    {

		$countrylist= Country::with(['getCity'=> function($q)
		{
			// Query the is_active field in getStates table
			$q->where('is_active', '=',1); // '=' is optional
		}])
		->whereStatus(1)
		->inRandomOrder()->limit(1)
		->get();
		if(!$countrylist->isEmpty())
			return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $countrylist->toArray()
					],200);
			else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);
    }

	public function BlogList(Request $request)
    {
		if($request->type=="details")
		{
			$newslist = \App\Blog::whereStatus(1)->whereId($request->blog_id)->get();
			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}
		else
		{
			$newslist= \App\Blog::whereStatus(1)->orderBy('created_at', 'DESC')->get();
			if(!$newslist->isEmpty())
				return response()->json([
							'result' => 1,
							'message' => "Success",
							'data' => $newslist->toArray()
						],200);
				else
						 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}

	}

	public function OccasionList(Request $request)
    {
		if($request->type=="details")
		{
			$newslist = \App\Occasion::whereStatus(1)->whereId($request->occation_id)->get();
			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}
		else
		{
			$newslist= \App\Occasion::whereStatus(1)->orderBy('created_at', 'DESC')->get();
			if(!$newslist->isEmpty())
				return response()->json([
							'result' => 1,
							'message' => "Success",
							'data' => $newslist->toArray()
						],200);
				else
						 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}

	}




	public function PartnerList(Request $request)
    {

		if($request->type=="details")
		{
			$data=array();

			$partnerdetails= Partner::with(['getEvents'=> function($q)
			{
				$q->where('status', '=',1)->where('admin_status', '=',1);
			},
			'getGallery'=> function($q)
			{
				$q->where('status', '=',1);
			},

			'getReviews'=> function($q)
			{
				$q->where('status', '=',1)->where('admin_status', '=',1);
			},
			'getReviews.getUser'=> function($q)
			{
				$q->where('status', '=',1)->select('id','profile_image');
			}

			])
			->find($request->partner_id);

			if($partnerdetails)
			{

				$data['vendordetails'] = $partnerdetails->toArray();
				$relatedvendors=Partner::whereStatus(1)->where('id','<>',$request->partner_id)->whereCategoryId($partnerdetails->category_id)->get();

				$nearbyvendors=Partner::whereStatus(1)->where('id','<>',$request->partner_id)->whereArea($partnerdetails->area)->get();

				$sponsoredads=\App\PartnerAdvertisement::where('partner_id','=',$request->partner_id)->where('position','=', 'Details')->get();

				if(!$relatedvendors->isEmpty())
				{
				$data['relatedvendors'] = $relatedvendors->toArray();
			    $data['nearbyvendors'] = $nearbyvendors->toArray();
			    $data['sponsoredads'] = $sponsoredads->toArray();
				}

					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $data
					],200);
			}
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}
		else
		{
			$s=array();
			if($request->category_id)
				$s['category'] =json_decode($request->category_id);

			$vendoradvetisement = \App\PartnerAdvertisement::whereStatus(1)->whereAdminStatus(1)->wherePosition('List')->where('todate', '>=', date('Y-m-d'))->get(array('id','partner_id','position','name','image'));


			$vendorlist = $this->searchapi($s);
			$vendorlist = $vendorlist->get(array('id','company_name','image','address'));

			$data['vendoradvetisement']=$vendoradvetisement->toArray();
			$data['vendorlist']=$vendorlist->toArray();
			if(!$vendorlist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $data
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);

		}


	}

	public function PartnerCourseList(Request $request)
    {
		if($request->type=="details")
		{
			$newslist = PartnerCourse::whereStatus(1)->wherePartnerId($request->partner_id)->whereId($request->course_id)->get();
			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}
		elseif($request->type=="book")
		{
			$validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
			'email' => 'required|email',
			'message' => 'required|min:10',
			]);

			if($validator->fails())
			{
				return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}
			else
			{

				$id2=$request->course_id;
				$partnerdetails= Partner::with(['getCourse'=> function($q) use ($id2)
				{
					$q->where('id', '=',$id2); // '=' is optional
				}
				])
				->find($request->partner_id);

				Event::dispatch('event.coursecontact',array($request->all(),$partnerdetails->email,$partnerdetails->company_name,$partnerdetails->getCourse[0]->name));

				 return response()->json(['result' => 1,'message' => 'Success'], 200);
			}
		}
		else
		{
			$newslist = PartnerCourse::whereStatus(1)->wherePartnerId($request->partner_id)->orderBy('created_at', 'DESC')->get();
			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}
	}

	public function PartnerProductList(Request $request)
    {
		if($request->type=="details")
		{
			$newslist = PartnerProduct::whereStatus(1)->wherePartnerId($request->partner_id)->whereId($request->product_id)->get();
			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}
		elseif($request->type=="book")
		{

			$validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
			'email' => 'required|email',
			'message' => 'required|min:10',
			]);

			if($validator->fails())
			{
				return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}
			else
			{

				$id2=$request->product_id;
				$partnerdetails= Partner::with(['getProducts'=> function($q) use ($id2)
				{
					$q->where('id', '=',$id2); // '=' is optional
				}
				])
				->find($request->partner_id);

				Event::dispatch('event.productscontact',array($request->all(),$partnerdetails->email,$partnerdetails->company_name,$partnerdetails->getProducts[0]->name));

				 return response()->json(['result' => 1,'message' => 'Success'], 200);
			}

		}
		else
		{
			$newslist = PartnerProduct::whereStatus(1)->wherePartnerId($request->partner_id)->orderBy('created_at', 'DESC')->get();
			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}

	}

	public function PartnerOfferList(Request $request)
    {
		if($request->type=="details")
		{
			$newslist = ManageOffer::whereStatus(1)->wherePartnerId($request->partner_id)->whereId($request->offer_id)->get();
			if(!$newslist->isEmpty())
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $newslist->toArray()
					],200);
				else
					 return response()->json(['result' => 0,'message' => "Not Found",], 200);
		}
		elseif($request->type=="book")
		{

			$validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
			'email' => 'required|email',
			'message' => 'required|min:10',
			]);

			if($validator->fails())
			{
				return response()->json(['result' => 0,'message' => 'Failed', 'data' =>$validator->messages()], 200);
			}
			else
			{

				$id2=$request->service_id;
				$partnerdetails= Partner::with(['getServices'=> function($q) use ($id2)
				{
					$q->where('id', '=',$id2); // '=' is optional
				}
				])
				->find($request->partner_id);

				Event::dispatch('event.servicescontact',array($request->all(),$partnerdetails->email,$partnerdetails->company_name,$partnerdetails->getServices[0]->name));

				 return response()->json(['result' => 1,'message' => 'Success'], 200);
			}

		}
		else
		{
            $validator = Validator::make($request->all(), [
                'partner_id' => 'required'
            ]);

            if ($validator->fails())
                {
                    $errMsg = json_decode($validator->messages());
                    if(isset($errMsg->partner_id[0])){
                        $err = $errMsg->partner_id[0];
                    }else {
                        $err = "Please pass required parameters";
                    }
                    return response()->json(['result' => 0,'message' => $err], 200);
                }
            else
            {
                $newslist = ManageOffer::whereStatus(1)->wherePartnerId($request->partner_id)->where('valid_upto', '>=', Carbon::today())->orderBy('created_at', 'DESC')->get();
                if(!$newslist->isEmpty())
                    {
                        $analytic = new Analytic;
                        $analytic->partner_id  = $request->partner_id;
                        $analytic->page = 'Offer';
                        $analytic->created_at = Carbon::now();
                        $analytic->save();
                        return response()->json([
                            'result' => 1,
                            'message' => "Success",
                            'data' => $newslist->toArray()
                        ],200);
                    }else{
                        return response()->json(['result' => 0,'message' => "Not Found",], 200);
                    }
            }
		}

	}


    public function searchapi($params)
	{
		 /* print_r($params);
		 exit; */
		 if(empty($params))
		 {
            $vendorlist = Partner::frntOpenProducts()->frnPriorityOrder('');
		 }
		 else
		 {

			$query = Partner::frntOpenProducts();

			if (!empty($params['tagslist'])) {
			$tags = $params['tagslist'];
               $query->where(function ($subquery) use ($tags) {
						foreach ($tags as $categoryid) {
						$subquery->orWhereRaw('FIND_IN_SET("' . $categoryid . '",tags) ');
						}
					});

            }

			if (!empty($params['category'])) {
				$categories = $params['category'];
               $query->where(function ($subquery) use ($categories) {
						foreach ($categories as $categoryid) {
						$subquery->orWhereRaw('FIND_IN_SET("' . $categoryid . '",category_id) ');
						}
					});

            }

			if (!empty($params['area'])) {
				$areas = $params['area'];
               $query->where(function ($subquery) use ($areas) {
						foreach ($areas as $areasid) {
						$subquery->orWhereRaw('FIND_IN_SET("' .$areasid . '",area) ');
						}
					});

            }

			if (!empty($params['city'])) {
				$citys = $params['city'];
               $query->where(function ($subquery) use ($citys) {
						foreach ($citys as $cityid) {
						$subquery->orWhereRaw('FIND_IN_SET("' .$cityid . '",city) ');
						}
					});

            }



			if (!empty($params['subcategory'])) {
			$categories = $params['subcategory'];
               $query->where(function ($subquery) use ($categories) {
						foreach ($categories as $categoryid) {
						$subquery->orWhereRaw('FIND_IN_SET("' . $categoryid . '",sub_cat_id) ');
						}
					});

            }



			if (!array_key_exists('sort_by', $params))
                $params['sort_by'] = '';
            $vendorlist = $query->frnPriorityOrder($params['sort_by']);


				//echo $query->frnPriorityOrder($params['sort_by'])->toSql();


		 }

		 return $vendorlist;


	}
    public function getreference(){
        $referencelist = ['Self' => 'Self','Socail_Media'=>'Socail Media','Reference ID'=>'Reference ID'];
        return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $referencelist
					],200);
    }
    public function getOTP(Request $request){
        if($request->mobile_number){
            $otp = rand(1000,9999);
            $message="Phone2bazar OTP : ".$otp."\n";
			getSMMMobile1($request->mobile_number,$message);
            return response()->json(['result' => 1,'message' => "Success", 'OTP' => $otp], 200);
        }else{
            return response()->json(['result' => 0,'message' => "Mobile number not found"], 200);
        }
    }
	public function vendorlistbysubcategory(Request $request){
		$validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required',
            'subcategory_id' => 'required'
        ]);
        if ($validator->fails())
            {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->search_string[0])){
                    $err = $errMsg->search_string[0];
                }else if(isset($errMsg->latitude[0])){
                    $err = $errMsg->latitude[0];
                }else if(isset($errMsg->longitude[0])){
                    $err = $errMsg->longitude[0];
                }else if(isset($errMsg->subcategory_id[0])){
                    $err = $errMsg->subcategory_id[0];
                }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }
		else
		{
            $subcategoryid = $request->subcategory_id;
            $details = Partner::whereStatus(1)->whereSubCatId($subcategoryid)->orderBy('searchdistance','asc')->get(['id','company_name','address','profile_percentage','city','state','email','mobile_number','whatsapp_number','web_link','facebook','instagram','image',DB::raw('distance('.$request->latitude.','.$request->longitude.',latitude,longitude) as searchdistance')]);
            if(!$details->isEmpty()){
                foreach($details as $detail){
                    $vendor_id = $detail->id;
                    $ratingsdetails = \App\CustomerPartnerReview::wherePartnerId($vendor_id)->orderBy('created_at','desc')->get();
                    $total_rating = 0;
                    if(!$ratingsdetails->isEmpty()){
                        foreach($ratingsdetails as $detailrat){
                            $total_rating = $total_rating +  $detailrat->rating;

                        }
                    }else{
                        $ratingdata = [];
                    }
                    $avg_rating = ($total_rating > 0) ? round($total_rating/count($ratingsdetails)):0;
                    $data[] = [
                        'id'=> (string)$detail->id,
                        'company_name'=> ($detail->company_name) ? $detail->company_name:"",
                        'address'=> ($detail->address) ? $detail->address:"",
                        'state_id'=>($detail->profile_percentage) ? $detail->profile_percentage : "",
                        'state_id'=>($detail->state) ? (string)$detail->state:"",
                        'state_name'=>($detail->state) ? $detail->getState->state:"",
                        'city_id'  => ($detail->city) ? (string)$detail->city:"",
                        'city_name' => ($detail->city) ? $detail->getCity->city_name : "",
                        'email' => ($detail->email) ? $detail->email : "",
                        'mobile_number' => ($detail->mobile_number) ? (string)$detail->mobile_number:"",
                        'whatsapp_number' => ($detail->whatsapp_number) ? (string)$detail->whatsapp_number:"",
                        'website' => ($detail->web_link) ?  $this->website($detail->web_link): "",
                        'facebook' => ($detail->facebook) ?  $this->website($detail->facebook) : "",
                        'instagram' => ($detail->instagram) ?  $this->website($detail->instagram):"",
                        'image' => ($detail->image)? asset('public/storage/upload/partnerimage/').'/'.$detail->image : "",
                        'avg_rating' =>$avg_rating,
                        'distance' => $detail->searchdistance,

                    ];
                }
                $subcategorydata = SubCategory::find($subcategoryid);
                return response()->json([
                    'result' => 1,
                    'message' => "Success",
                    'subcategory_name' =>$subcategorydata->category,
                    'data' => $data
                ],200);
            }else{
                return response()->json(['result' => 0,'message' => "Not Found",], 200);
            }
        }
    }

    public function vendordetails($vendor_id=NULL){
        $details = Partner::whereStatus(1)->whereId($vendor_id)->orderBy('company_name','asc')->get();
        $ratingsdetails = \App\CustomerPartnerReview::with(array('getUser:id,profile_image'))->wherePartnerId($vendor_id)->orderBy('created_at','desc')->get();
        $total_rating = 0;
        if(!$ratingsdetails->isEmpty()){
                foreach($ratingsdetails as $detail){
                    $total_rating = $total_rating +  $detail->rating;
                    $ratingdata[] = [
                        'id'=> (string)$detail->id,
                        'rating'=> ($detail->rating) ? $detail->rating:"",
                        'name'=> ($detail->name) ? $detail->name:"",
                        'email'=>($detail->email) ? $detail->email : "",
                        'message'=>($detail->message) ? (string)$detail->message:"",
                        'created_at'=>($detail->created_at) ? date("d M, Y h:i A", strtotime($detail->created_at)):"",
                        'updated_at'  => ($detail->updated_at) ? (string)$detail->updated_at:"",
                       // 'imagepath' => ($detail->partner_id) ? $detail->getCity->city_name : "",
                        'partner_image' => ($detail->getUser->profile_image)? asset('public/storage/upload/profile/').'/'.$detail->getUser->profile_image : "",

                    ];
                }
        }else{
            $ratingdata = [];
        }
        $avg_rating = ($total_rating > 0) ? round($total_rating/count($ratingsdetails)):0;
        if(!$details->isEmpty()){
                $analytic = new Analytic;
                $analytic->partner_id  = $vendor_id;
                $analytic->page = 'Partner Details';
                $analytic->created_at = Carbon::now();
                $analytic->save();
            foreach($details as $detail){
                $data = [
                    'id'=> (string)$detail->id,
                    'company_name'=> ($detail->company_name) ? $detail->company_name:"",
                    'description'=> ($detail->description) ? $detail->description:"",
                    'address'=> ($detail->address) ? $detail->address:"",
                    'state_id'=>($detail->profile_percentage) ? $detail->profile_percentage : "",
                    'state_id'=>($detail->state) ? (string)$detail->state:"",
                    'state_name'=>($detail->state) ? $detail->getState->state:"",
                    'city_id'  => ($detail->city) ? (string)$detail->city:"",
                    'city_name' => ($detail->city) ? $detail->getCity->city_name : "",
                    'email' => ($detail->email) ? $detail->email : "",
                    'mobile_number' => ($detail->mobile_number) ? (string)$detail->mobile_number:"",
                    'whatsapp_number' => ($detail->whatsapp_number) ? (string)$detail->whatsapp_number:"",
                    'website' => ($detail->web_link) ? $this->website($detail->web_link): "",
                    'facebook' => ($detail->facebook) ?  $this->website($detail->facebook) : "",
                    'instagram' => ($detail->instagram) ?  $this->website($detail->instagram):"",
                    'opening_time' => ($detail->openning_time != "00:00:00" && $detail->openning_time !="") ? date("h:i A",strtotime($detail->openning_time)) : "",
                    'closing_time' => ($detail->closing_time != "00:00:00" && $detail->closing_time != "") ? date("h:i A",strtotime($detail->closing_time)):"",
                    'image' => ($detail->image)? asset('public/storage/upload/partnerimage/').'/'.$detail->image : "",
                    'map_link'=> "https://www.google.com/maps/search/?api=1&query=".$detail->latitude.",".$detail->longitude,
                    'rating_review' => $ratingdata,
                    'avg_rating' => $avg_rating,
                    'latitude' => ($detail->latitude) ? $detail->latitude : "",
                    'longitude' => ($detail->longitude) ? $detail->longitude : "",

                ];
            }
			return response()->json([
				'result' => 1,
				'message' => "Success",
				'data' => $data
			],200);
        }else{
            return response()->json(['result' => 0,'message' => "Not Found",], 200);
        }
    }
    public function vendorGallery($vendor_id=NULL){
        $gallery = PartnerGallery::wherePartnerId($vendor_id)->get(['id','image']);
        if(!$gallery->isEmpty()){
            foreach($gallery as $gal){
               $gallerydata[]=[
                    'id' => (string)$gal->id,
                    'image'=>asset('public/storage/upload/partnergallery').'/'.$gal->image
               ];
            }
            return response()->json([
                'result' => 1,
                'message' => "Success",
                'data' => $gallerydata
            ],200);
        }else{
            return response()->json(['result' => 0,'message' => "Not Found",], 200);
        }
    }

    public function vendorBookedAvl($vendor_id=NULL){
        $gallery = BookAvailability::wherePartnerId($vendor_id)->get(['id','booking_date']);
        if(!$gallery->isEmpty()){
            foreach($gallery as $gal){
               $gallerydata[]=[
                    'id' => (string)$gal->id,
                    'booking_date'=>$gal->booking_date
               ];
            }
            return response()->json([
                'result' => 1,
                'message' => "Success",
                'data' => $gallerydata
            ],200);
        }else{
            return response()->json(['result' => 0,'message' => "Not Found",], 200);
        }
    }

    public function whatsappCallClick(Request $request){
        $validator = Validator::make($request->all(), [
            'partner_id' => 'required'
        ]);

		if ($validator->fails())
            {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->partner_id[0])){
                    $err = $errMsg->partner_id[0];
                }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }
		else
		{
            $analytic = new Analytic;
            $analytic->partner_id  = $request->partner_id;
            $analytic->page = 'Whatsapp';
            $analytic->created_at = Carbon::now();
            $analytic->save();
            return response()->json(['result' => 1,'message' => "Success",], 200);
        }
    }

    public function Advertisement(Request $request){
        $validator = Validator::make($request->all(), [
            'position' => 'required'
        ]);

		if ($validator->fails())
            {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->position[0])){
                    $err = $errMsg->position[0];
                }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }
		else
		{
            $details = PartnerAdvertisement::whereStatus(1)->whereAdminStatus(1)->wherePosition($request->position)->orderBy('id','DESC')->get();
            if(!$details->isEmpty()){
                $gallerydata = [];
                foreach($details as $gal){
                    $partnerdetails = Partner::find($gal->partner_id);
                    $expire_date = $partnerdetails->subscription_expire_on;
                    if($expire_date >= Carbon::now()){
                        $gallerydata[]=[
                            'id' => (string)$gal->id,
                            'partner_id' => $gal->partner_id,
                            'image'=>asset('public/storage/upload/partneradvertisement').'/'.$gal->image,
                            'url' =>  ($gal->url) ?  $this->website($gal->url) : "",
                            'expire' => $expire_date
                        ];
                    }

                }
                if(!empty($gallerydata)){
					return response()->json([
						'result' => 1,
						'message' => "Success",
						'data' => $gallerydata
                    ],200);
                }else{
                    return response()->json(['result' => 0,'message' => "Not Found",], 200);
                }
            }else{
                     return response()->json(['result' => 0,'message' => "Not Found",], 200);
            }

        }
    }
    public function searchVendor(Request $request){
        $validator = Validator::make($request->all(), [
            'search_string' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
        if ($validator->fails())
            {
                $errMsg = json_decode($validator->messages());
                if(isset($errMsg->search_string[0])){
                    $err = $errMsg->search_string[0];
                }else if(isset($errMsg->latitude[0])){
                    $err = $errMsg->latitude[0];
                }else if(isset($errMsg->longitude[0])){
                    $err = $errMsg->longitude[0];
                }else {
                    $err = "Please pass required parameters";
                }
                return response()->json(['result' => 0,'message' => $err], 200);
            }
		else
		{
            $search_string = $request->search_string;
            $query = Partner::whereStatus(1)->orderBy('searchdistance');
                $query->whereHas('getcategory', function($subquery) use($search_string)
                {
                 $subquery->where('status', '=',1)->where('category', 'LIKE', '%'.$search_string.'%');
                });
           // echo $result = $query->toSql();
            $categorydetails = $query->get(['id','company_name','address','profile_percentage','city','state','email','mobile_number','whatsapp_number','web_link','facebook','instagram','image',DB::raw('distance('.$request->latitude.','.$request->longitude.',latitude,longitude) as searchdistance')]);
            $query1 = Partner::whereStatus(1)->orderBy('searchdistance');
                $query1->whereHas('getSubCategory', function($subquery1) use($search_string)
                {
                 $subquery1->where('status', '=',1)->where('category', 'LIKE', '%'.$search_string.'%');
                });
           // echo $result = $query->toSql();
            $subcategorydetails = $query1->get(['id','company_name','address','profile_percentage','city','state','email','mobile_number','whatsapp_number','web_link','facebook','instagram','image',DB::raw('distance('.$request->latitude.','.$request->longitude.',latitude,longitude) as searchdistance')]);
            $partnerdetails = Partner::whereStatus(1)->where('company_name', 'LIKE', "%$request->search_string%" )->orderBy('searchdistance')->get(['id','company_name','address','profile_percentage','city','state','email','mobile_number','whatsapp_number','web_link','facebook','instagram','image', DB::raw('distance('.$request->latitude.','.$request->longitude.',latitude,longitude) as searchdistance')]);
			//$partnerdetails = DB::table('partners')->select('id', 'company_name',DB::raw('distance('.$request->latitude.','.$request->longitude.',latitude,longitude) as searchdistance'))->where('status', 1)->where('company_name', 'LIKE', '%'.$request->search_string.'%' )->orderBy('searchdistance')->get();
			//print_r($subcategorydetails);die;
            if(!$partnerdetails->isEmpty()){
                foreach($partnerdetails as $detail){
                    $data[] = [
                        'id'=> (string)$detail->id,
                        'company_name'=> ($detail->company_name) ? $detail->company_name:"",
                        'address'=> ($detail->address) ? $detail->address:"",
                        'profile_percentage'=>($detail->profile_percentage) ? $detail->profile_percentage : "",
                        'state_id'=>($detail->state) ? (string)$detail->state:"",
                        'state_name'=>($detail->state) ? $detail->getState->state:"",
                        'city_id'  => ($detail->city) ? (string)$detail->city:"",
                        'city_name' => ($detail->city) ? $detail->getCity->city_name : "",
                        'email' => ($detail->email) ? $detail->email : "",
                        'mobile_number' => ($detail->mobile_number) ? (string)$detail->mobile_number:"",
                        'whatsapp_number' => ($detail->whatsapp_number) ? (string)$detail->whatsapp_number:"",
                        'website' => ($detail->web_link) ?  $this->website($detail->web_link) : "",
                        'facebook' => ($detail->facebook) ?  $this->website($detail->facebook) : "",
                        'instagram' => ($detail->instagram) ?  $this->website($detail->instagram):"",
						'image' => ($detail->image)? asset('public/storage/upload/partnerimage/').'/'.$detail->image : "",
						'distance' => ($detail->searchdistance) ? $detail->searchdistance : 0,
                    ];
                }
                return response()->json([
                    'result' => 1,
                    'message' => "Success",
                    'data' => $data
                ],200);

            }else if(!$categorydetails->isEmpty()){
                foreach($categorydetails as $detail){
                    $data[] = [
                        'id'=> (string)$detail->id,
                    'company_name'=> ($detail->company_name) ? $detail->company_name:"",
                    'address'=> ($detail->address) ? $detail->address:"",
                    'profile_percentage'=>($detail->profile_percentage) ? $detail->profile_percentage : "",
                    'state_id'=>($detail->state) ? (string)$detail->state:"",
                    'state_name'=>($detail->state) ? $detail->getState->state:"",
                    'city_id'  => ($detail->city) ? (string)$detail->city:"",
                    'city_name' => ($detail->city) ? $detail->getCity->city_name : "",
                    'email' => ($detail->email) ? $detail->email : "",
                    'mobile_number' => ($detail->mobile_number) ? (string)$detail->mobile_number:"",
                    'whatsapp_number' => ($detail->whatsapp_number) ? (string)$detail->whatsapp_number:"",
                    'website' => ($detail->web_link) ? $detail->web_link : "",
                    'facebook' => ($detail->facebook) ? $detail->facebook : "",
                    'instagram' => ($detail->instagram) ? $detail->instagram:"",
					'image' => ($detail->image)? asset('public/storage/upload/partnerimage/').'/'.$detail->image : "",
					'distance' => ($detail->searchdistance) ? $detail->searchdistance : 0,
                    ];
                }
                return response()->json([
                    'result' => 1,
                    'message' => "Success",
                    'data' => $data
                ],200);

            }else if(!$subcategorydetails->isEmpty()){
                foreach($subcategorydetails as $subcatdata){
                    $data[] = [
                        'id'=> (string)$subcatdata->id,
                        'company_name'=> ($subcatdata->company_name) ? $subcatdata->company_name:"",
                        'address'=> ($subcatdata->address) ? $subcatdata->address:"",
                        'profile_percentage'=>($subcatdata->profile_percentage) ? $subcatdata->profile_percentage : "",
                        'state_id'=>($subcatdata->state) ? (string)$subcatdata->state:"",
                        'state_name'=>($subcatdata->state) ? $subcatdata->getState->state:"",
                        'city_id'  => ($subcatdata->city) ? (string)$subcatdata->city:"",
                        'city_name' => ($subcatdata->city) ? $subcatdata->getCity->city_name : "",
                        'email' => ($subcatdata->email) ? $subcatdata->email : "",
                        'mobile_number' => ($subcatdata->mobile_number) ? (string)$subcatdata->mobile_number:"",
                        'whatsapp_number' => ($subcatdata->whatsapp_number) ? (string)$subcatdata->whatsapp_number:"",
                        'website' => ($subcatdata->web_link) ? $subcatdata->web_link : "",
                        'facebook' => ($subcatdata->facebook) ? $subcatdata->facebook : "",
                        'instagram' => ($subcatdata->instagram) ? $subcatdata->instagram:"",
						'image' => ($subcatdata->image)? asset('public/storage/upload/partnerimage/').'/'.$subcatdata->image : "",
						'distance' => ($subcatdata->searchdistance) ? $subcatdata->searchdistance : 0,
                    ];
                }
                return response()->json([
                    'result' => 1,
                    'message' => "Success",
                    'data' => $data
                ],200);

            }else{
                return response()->json(['result' => 0,'message' => "No data found"], 200);
            }
        }
    }

    function website($url=NULL){
        $word = "https://";
        $word1 = "http://";
        if(strpos($url, $word) !== false){
            return $http_referer = str_replace($word, "", $url);
        }elseif(strpos($url, $word1) !== false){
            return $http_referer = str_replace($word1, "", $url);
        }else{
            return $url;
        }

    }



}
