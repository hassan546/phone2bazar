<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;
use App\Country;
use App\Area;

class State extends Model
{
    public $primaryKey='state_id';
	//protected $appends = array('imagepath');

	public function getImagePathAttribute()
    {
        return asset('public/storage/upload/state/');  
    }
	
    public function getCity()
    {
    	return $this->hasMany(City::class,'state_id','state_id');
    }
	
	public function getCountry()
    {
		return $this->belongsTo(Country::class,'country_id');
		
    	
    }
	
 
}
