<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseSubscription extends Model
{
    //
    public function getSubscription()
    {
    	return $this->belongsTo(Subscription::class,'subscription_id');
    }
    public function getPartner()
    {
    	return $this->belongsTo(Partner::class,'partner_id');
    }

}
