<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            '*',
            'App\Http\ViewComposers\HelperComposer'
        );



        // Using Closure based composers...
        view()->composer('*', function ($view) {
            //
            $status = array(
                '' => 'Select Status',
                '0' => 'Inactive',
                '1' => 'Active'
            );

			$ratings = array(
                '' => 'Select Plan',
                'Basic' => 'Basic Plan',
                'Regular' => 'Regular Plan',
				'Premium' => 'Premium Plan'

            );

			$occationtype = array(
                '1' => 'General',
				'2' => 'Personalities',
                '3' => 'Companies',
                '4' => 'Occations / Festival',
				'5' => 'Places'
            );

			$placetype = array(
                'State' => 'State',
				'City' => 'City',
                'Location' => 'Location',
            );


			$dashboardaccessview = array(
				'' => 'Select Type',
                '1' => 'Products',
				'2' => 'Services',
                '3' => 'Courses',
                '4' => 'Menu (Food)',
				'5' => 'Products &  Services',
				'6' => 'Products, Services and Courses',
				'7' => 'Products, Services, Courses and  Menu'
            );

			$viewtimeings = array(
                '00:00:00' => 'Select Time',
                '01:00:00' => '1:00 AM',
				'02:00:00' => '2:00 AM',
                '03:00:00' => '3:00 AM',
                '04:00:00' => '4:00 AM',
				'05:00:00' => '5:00 AM',
				'06:00:00' => '6:00 AM',
				'07:00:00' => '7:00 AM',
				'08:00:00' => '8:00 AM',
				'09:00:00' => '9:00 AM',
				'10:00:00' => '10:00 AM',
				'11:00:00' => '11:00 AM',
				'12:00:00' => '12:00 AM',
				'13:00:00' => '1:00 PM',
				'14:00:00' => '2:00 PM',
				'15:00:00' => '3:00 PM',
				'16:00:00' => '4:00 PM',
				'17:00:00' => '5:00 PM',
				'18:00:00' => '6:00 PM',
				'19:00:00' => '7:00 PM',
				'20:00:00' => '8:00 PM',
				'21:00:00' => '9:00 PM',
				'22:00:00' => '10:00 PM',
				'23:00:00' => '11:00 PM',
				'24:00:00' => '12:00 PM'
            );





			$positiontype = array(
                '' => 'Select Position',
				'Home' => 'Home',
                'Category' => 'Category',
				'List' => 'List'
            );


			$optionstype = array(
                '1' => 'Select',
                '2' => 'Checkbox',
				 '3' => 'Radio',
            );
			$optionstype = array(
                '1' => 'Select',
                '2' => 'Checkbox',
				 '3' => 'Radio',
            );
			$gender = array(
                'Male' => 'Male',
                'Female' => 'Female',
            );

            $view->with(array(
                'status'=>$status,
				 'optionstype'=>$optionstype,
				 'ratings'=>$ratings,
				 'occationtype'=>$occationtype,
				 'dashboardaccessview'=>$dashboardaccessview,
				 'positiontype'=>$positiontype,
				  'viewtimeings'=>$viewtimeings,
				  'placetype'=>$placetype,
				  'gender'=>$gender,

            ));

        });

    }
}
