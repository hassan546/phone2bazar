<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Mail\MailEmail;
use App\Hotelier;
use App\User;
use Auth;
use Illuminate\Support\Facades\Mail;
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('event.contactus', function(array $contact) 
		{
			$message="";
			$message.="Dear Admin,<br>";
			$message.="A new query has been made from ".config('mail.from.name').", please check below:<br><br>";
			$message.="Name : ".$contact['first_name']."<br>";
			$message.="Email : ".$contact['email']."<br>";
			$message.="Subject : ".$contact['subject']."<br>";
			$message.="Message : ".$contact['message']."<br>";
		
			$response = new \stdClass();
			$response->mailtemplate = "mails.common";
			$response->subject = "New Query from  : ".config('mail.from.name');
			$response->mailcontent = $message;
			
			if(!\App::environment('local'))
			  Mail::to(config('mail.from.address'))->send(new MailEmail($response));
	  
		});
		
		Event::listen('event.coursecontact', function(array $contact,$email,$name,$coursename) 
		{
			
			
			$message="";
			$message.="Dear ".$name.",<br>";
			$message.="A new course query has been made from ".$contact['name'].", please check below:<br><br>";
			
			$message.="Course Name : ".$coursename."<br>";
			$message.="Name : ".$contact['name']."<br>";
			$message.="Email : ".$contact['email']."<br>";
			$message.="Message : ".$contact['message']."<br>";
		
			$response = new \stdClass();
			$response->mailtemplate = "mails.common";
			$response->subject = "New Course Query from  : ".config('mail.from.name');
			$response->mailcontent = $message;
			
			if(!\App::environment('local'))
			  Mail::to($email)->send(new MailEmail($response));
	  
		});
		
		Event::listen('event.productscontact', function(array $contact,$email,$name,$coursename) 
		{
			
			
			$message="";
			$message.="Dear ".$name.",<br>";
			$message.="A new product booking has been made from ".$contact['name'].", please check below:<br><br>";
			
			$message.="Product Name : ".$coursename."<br>";
			$message.="Name : ".$contact['name']."<br>";
			$message.="Email : ".$contact['email']."<br>";
			$message.="Message : ".$contact['message']."<br>";
		
			$response = new \stdClass();
			$response->mailtemplate = "mails.common";
			$response->subject = "New Product Booking from  : ".config('mail.from.name');
			$response->mailcontent = $message;
			
			if(!\App::environment('local'))
			  Mail::to($email)->send(new MailEmail($response));
	  
		});
		
		Event::listen('event.servicescontact', function(array $contact,$email,$name,$coursename) 
		{
			
			$message="";
			$message.="Dear ".$name.",<br>";
			$message.="A new service query has been made from ".$contact['name'].", please check below:<br><br>";
			
			$message.="Service Name : ".$coursename."<br>";
			$message.="Name : ".$contact['name']."<br>";
			$message.="Email : ".$contact['email']."<br>";
			$message.="Message : ".$contact['message']."<br>";
		
			$response = new \stdClass();
			$response->mailtemplate = "mails.common";
			$response->subject = "New Service Query from  : ".config('mail.from.name');
			$response->mailcontent = $message;
			
			if(!\App::environment('local'))
			  Mail::to($email)->send(new MailEmail($response));
	  
		});
		
		
		
		Event::listen('event.eventcustomer', function($eventbookd) 
		{
			$message="";
			$message.="Dear ".$eventbookd->user_name.",<br>";
			$message.="Thanks for you booking:<br><br>";
			
			$message.="Event Name : ".$eventbookd->getEvent->name."<br>";
			$message.="Event Start Date : ".$eventbookd->start_date."<br>";
			$message.="Event End Date : ".$eventbookd->end_date."<br>";
			$message.="Event Price : ".$eventbookd->booking_price."<br>";

		
			$response = new \stdClass();
			$response->mailtemplate = "mails.common";
			$response->subject = "Event booking :".$eventbookd->id.$eventbookd->created_at->format('Ymd')." from : ".config('mail.from.name');
			$response->mailcontent = $message;
			
			if(!\App::environment('local'))
			  Mail::to($eventbookd->user_email)->send(new MailEmail($response));
	  
		});
		
		
		Event::listen('event.eventvendor', function($eventbookd) 
		{
			$message="";
			$message.="Dear ".$eventbookd->getEvent->getPartner->company_name.",<br>";
			
			$message.="A new booking has been made from ".$eventbookd->user_name.", please check below:<br><br>";
			
			$message.="Event Name : ".$eventbookd->getEvent->name."<br>";
			$message.="Event Start Date : ".$eventbookd->start_date."<br>";
			$message.="Event End Date : ".$eventbookd->end_date."<br>";
			$message.="Event Price : ".$eventbookd->booking_price."<br><br>";
			
			$message.="Customer Name : ".$eventbookd->user_name."<br>";
			$message.="Customer Email : ".$eventbookd->user_email."<br>";
			$message.="Customer Mobile No : ".$eventbookd->user_mobile."<br>";

		
			$response = new \stdClass();
			$response->mailtemplate = "mails.common";
			$response->subject = "Event booking :".$eventbookd->id.$eventbookd->created_at->format('Ymd')." from : ".config('mail.from.name');
			$response->mailcontent = $message;
			
			if(!\App::environment('local'))
			  Mail::to($eventbookd->getEvent->getPartner->email)->send(new MailEmail($response));
	  
		});
		
		
		
		
		Event::listen('event.menucontact', function(array $contact,$email,$name,$coursename) 
		{
			
			$message="";
			$message.="Dear ".$name.",<br>";
			$message.="A new menu booking has been made from ".$contact['first_name'].", please check below:<br><br>";
			
			$message.="Menu Name : ".$coursename."<br>";
			$message.="First Name : ".$contact['first_name']."<br>";
			$message.="Last Name : ".$contact['last_name']."<br>";
			$message.="Email : ".$contact['email']."<br>";
			$message.="Phone/Mobile Number : ".$contact['mobile_no']."<br>";
			$message.="Date : ".$contact['date']."<br>";
			$message.="Time : ".$contact['time']."<br>";
			$message.="Number Of Guest : ".$contact['noofguest']."<br>";
		
			$response = new \stdClass();
			$response->mailtemplate = "mails.common";
			$response->subject = "New Menu booking from  : ".config('mail.from.name');
			$response->mailcontent = $message;
			
			if(!\App::environment('local'))
			  Mail::to($email)->send(new MailEmail($response));
	  
		});
		
		
		
		 
		
		
		

		
		
		
		
		
		
    }
}
