<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable=[];
	protected $appends = array('imagepath');

    public function getImagePathAttribute()
    {
        return asset('public/storage/upload/category/');  
    }
	
	public function getCategorySubcatgoryInCategoryAttribute($value) 
	{
		$category = \App\Category::where('status','=',1)->where('id','=',$this->parent_id)->first();
		if($category)
			return $category->category.' > '.$this->category;
		else
			return $this->category;
	}
	
	public function getSubCategory()
    {
        return $this->hasMany(SubCategory::class,"main_category","id");
    }
	
	public function getSubCategoryfornon()
    {
        return $this->hasMany(SubCategory::class,"main_category","id");
    }
	
	public function getVendors()
    {
        return $this->hasMany(Partner::class,"category_id","id");
    }
	
	
	public function getTags()
    {
        return $this->hasMany(Tag::class,"main_category","id");
    }
	
	
}
	
	
