<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\PartnerResetPasswordNotification;
use Illuminate\Database\Eloquent\Model;
use Str;
use App\Category;
use Laravel\Passport\HasApiTokens;
class Partner extends Authenticatable
{
	 use Notifiable,HasApiTokens;
    protected $guard = 'partner';
    /**
     * The attributes that are mass assignable.
     *
 * @var array
     */
    protected $fillable = [
        'company_name', 'unique_id', 'email', 'password','mobile_number','address','status','textpassword','facebook','twitter','linkedin','profile_percentage','reference_id','reference_mobile','openning_time','closing_time','state_id,','city_id'

    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */




	protected $appends = array('imagepath');

    public function getImagePathAttribute()
    {
        return asset('public/storage/upload/partnerimage/');
    }



	public function getcategory()
    {
    	return $this->belongsTo(Category::class,'category_id')->withDefault();
    }


	public function getSubscription()
    {
    	return $this->belongsTo(Subscription::class,'subscription_id');
    }

	public function getSubCategory()
    {
    	return $this->belongsTo(SubCategory::class,'sub_cat_id')->withDefault();
    }

	 public function getState()
    {
    	return $this->belongsTo(State::class,'state');
    }


    public function getCity()
    {
    	return $this->belongsTo(City::class,'city');
    }






	protected $hidden = [
        'password', 'remember_token',
    ];

	protected $casts = [
        'email_verified_at' => 'datetime',
    ];

	 //Send password reset notification
	  public function sendPasswordResetNotification($token)
	  {
		  $this->notify(new PartnerResetPasswordNotification($token));
	  }


}
