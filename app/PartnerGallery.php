<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\User;

class PartnerGallery extends Model
{
    protected $fillable=[];
	protected $appends = array('imagepath');
	public function getImagePathAttribute()
    {
        return asset('public/storage/upload/partnergallery/');
    }
   
}