<?php
use App\User;
use App\Hotelier;
use App\RoomDetail;
use App\BookHotel;
use App\Setting;
if (!function_exists('getSMMMobile')) {
    function getSMMMobile($mobile,$message)
    {
        $msg = urlencode ($message );
        $datetime = "2017-11-08%2013:36:59";
        $url = file_get_contents ("https://mobile.btmsindia.com/api/api_http.php?username=SHAAPP&password=SHAAPP@2017&senderid=EFISTY&to=$mobile&text=$msg&route=Informative&type=text&datetime=$datetime");

		//$url = file_get_contents ("https://api.msg91.com/api/sendhttp.php?mobiles=$mobile&authkey=290048Ap7K5ioM5d599dfe&route=4&sender=EFISTY&message=$msg&country=91");

        $jsondecode = explode ( ' [', $url );
        //print_r($jsondecode);exit;
        $response = $jsondecode [0];
		return $response;
    }
}
if (!function_exists('getSMMMobile1')) {
    function getSMMMobile1($mobile,$message)
    {
        $username = urlencode("u44487");
        $msg_token = urlencode("xyniyA");
        $sender_id = urlencode("PSEBZR"); // optional (compulsory in transactional sms)
        $message = urlencode($message);
        $mobile = urlencode($mobile);
        $api = "http://manage.sarvsms.com/api/send_transactional_sms.php?username=".$username."&msg_token=".$msg_token."&sender_id=".$sender_id."&message=".$message."&mobile=".$mobile."";
        $response = file_get_contents($api);
        return $response;
    }
}
if (!function_exists('getSettingsValue'))
{
    function getSettingsValue($hours)
    {
		$settings=Setting::find(1);
		$settingsview = unserialize($settings->settings);

		return $settingsview['settings']['Hour'][$hours];
	}
}

if (!function_exists('getUserLocationdetails'))
{
    function getUserLocationdetails()
    {
		$ip = ''.$_SERVER['REMOTE_ADDR'].''; // your ip address here
		$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
		return $query;
	}
}

if (!function_exists('getBookingStatusforAdmin')) {
    function getBookingStatusforAdmin()
    {
       $data=array();
	   $noofhoteltodaysbooked = DB::table('book_hotels')->select(DB::raw('*'))
                  ->whereRaw('Date(created_at) = CURDATE()')->groupBy('hotel_no')->get()->count();


		$noofcheckintoday = DB::table('book_hotels')->select(DB::raw('num_of_person'))
                  ->whereRaw('Date(check_in_date) = CURDATE()')->sum("num_of_person");


		$noofcheckinfuture = DB::table('book_hotels')->select(DB::raw('num_of_person'))
                  ->whereRaw('Date(check_in_date) > CURDATE()')->sum("num_of_person");

		$booked_rooms=BookHotel::with('getRoomNumber')->where("check_out_flag",1)->orderBy('check_in_date', 'desc')->groupBy("order_id")->get();
		$booked_rooms_count=$booked_rooms->count();

		$data['noofhoteltodaysbooked']=$noofhoteltodaysbooked;
		$data['noofcheckintoday']=$noofcheckintoday;
		$data['noofcheckinfuture']=$noofcheckinfuture;
		$data['booked_rooms_count']=$booked_rooms_count;

		return $data;


    }
}
if (!function_exists('GetMetaTitle'))
{
	function GetMetaTitle($model = '', $id = '')
	{
		$metatitle = "";
		switch ($model) {
			case "Hotelier":
				$modeldata = \App\Hotelier::findOrFail($id);
				if ($modeldata->getOriginal("category_id"))
				{
					$location_records = \App\Category::whereIn('id', explode(",", $modeldata->getOriginal("category_id")))->get();
					foreach ($location_records as $value) {
						$metatitle.=$value->category . ", ";
					}
					$metatitle = substr_replace($metatitle, '', -2);
				}
				break;
			default:
				break;
		}
		return $metatitle;
	}
}
if (!function_exists('GetMetaDescription'))
{
	function GetMetaDescription($model = '', $id = '')
	{
		$metades = "";
		switch ($model) {
			case "Hotelier":
				$modeldata = \App\Hotelier::findOrFail($id);
				if ($modeldata->description) {
					$desc = strip_tags($modeldata->description);
					if (strlen($desc) <= 160)
						$metades = rtrim($desc);
					else {
						$metades = rtrim(substr($desc, 0, 160));
					}
				}
				break;
			default:
				break;
		}
		return $metades;
	}
}

if (!function_exists('getRoomNumber')) {
    function getRoomNumber($order_id)
    {
		$data=array();
		$bookings= App\BookHotel::where('order_id','=',$order_id)->get();
		$num_of_person=0;
		$kid=0;
		foreach($bookings as $key2=>$value2)
		{
			$num_of_person = $num_of_person + $value2->num_of_person;
			$kid=$kid + $value2->kid;
			$data['room_number'][]=$value2->getRoomNumber->room_no;
			$data['num_of_person']=$num_of_person;
			$data['kid']=$kid;
		}

		return $data;

    }
}

if (!function_exists('getPushNotificaiton')) {
    function getPushNotificaiton($fcm_fields=array())
    {
		$path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
        $fields = $fcm_fields;
        $headers = array (
            'Authorization:key= AIzaSyDS45wtUqpC6ah7dDn9aBJgpeNrXQwKBDM',
            'Content-Type:application/json'
        );
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $path_to_firebase_cm );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt ( $ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
        $output = curl_exec ( $ch );

        //print_r($output);die;

        curl_close ( $ch );

		return $output;
    }
}


?>
