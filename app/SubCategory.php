<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable=[];
	protected $appends = array('imagepath');

    public function getImagePathAttribute()
    {
        return asset('public/storage/upload/subcategory/');  
    }
	
	public function getCategory()
    {
    	return $this->belongsTo(Category::class,'main_category');
    }

}
	
	
